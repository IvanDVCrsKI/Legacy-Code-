// COMtestAlfaDoc.cpp : implementation of the CCOMtestAlfaDoc class
//

#include "stdafx.h"
#include "COMtestAlfa.h"

#include "COMtestAlfaDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CCOMtestAlfaDoc

IMPLEMENT_DYNCREATE(CCOMtestAlfaDoc, CDocument)

BEGIN_MESSAGE_MAP(CCOMtestAlfaDoc, CDocument)
END_MESSAGE_MAP()


// CCOMtestAlfaDoc construction/destruction

CCOMtestAlfaDoc::CCOMtestAlfaDoc()
{
	// TODO: add one-time construction code here

}

CCOMtestAlfaDoc::~CCOMtestAlfaDoc()
{
}

BOOL CCOMtestAlfaDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CCOMtestAlfaDoc serialization

void CCOMtestAlfaDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}


// CCOMtestAlfaDoc diagnostics

#ifdef _DEBUG
void CCOMtestAlfaDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CCOMtestAlfaDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CCOMtestAlfaDoc commands
