// KreatorTrajektorijaView.cpp : implementation of the CKreatorTrajektorijaView class
//

#include "stdafx.h"
#include "KreatorTrajektorija.h"

#include "KreatorTrajektorijaDoc.h"
#include "KreatorTrajektorijaView.h"



#include "Math.h"

//DIJALOZI
#include "AutorDV.h"
#include "Uputstvo.h"
#include "Parametri.h"
#include "Mreza.h"
#include "Korak.h"
#include "DGrafik.h"
#include "Tabla.h"
#include "Zum.h"
#include "Atmosfera.h"

#include "FODT.h"
#include "Tablab.h"





#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaView

IMPLEMENT_DYNCREATE(CKreatorTrajektorijaView, CView)

BEGIN_MESSAGE_MAP(CKreatorTrajektorijaView, CView)
	//{{AFX_MSG_MAP(CKreatorTrajektorijaView)
	ON_COMMAND(IDM_UPUTSTVO, OnUputstvo)
	ON_COMMAND(IDM_ZUM, OnZum)
	ON_COMMAND(IDM_PARAMETRI, OnParametri)
	ON_COMMAND(IDM_MREZA, OnMreza)
	ON_COMMAND(IDM_KORAK, OnKorak)
	ON_COMMAND(IDM_DGRAFIK, OnDgrafik)
	ON_COMMAND(IDM_TABLA, OnTabla)
	ON_COMMAND(IDM_AUTOR, OnAutor)
	ON_COMMAND(IDM_ATMOSFERA, OnAtmosfera)
	ON_WM_KEYDOWN()
	ON_COMMAND(IDM_FODT, OnFodt)
	ON_COMMAND(IDM_TABLA_B, OnTablaB)
	ON_WM_KEYUP()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaView construction/destruction

CKreatorTrajektorijaView::CKreatorTrajektorijaView()
{
	// TODO: add construction code here
	m_Brzina_f1 = 10; //0
	m_Brzina_f2 = 0;
	m_Brzina_f3= 0;
	m_Ugao_f1= 45; //0
    m_Ugao_f2= 0;
	m_Ugao_f3= 0;
    m_OtporF2 = 0.005;



	m_Mreza = 0;
	m_DGrafik = 0;
	m_Zumx= 100000;
	m_Zumy = 100000;
	m_Korak = 15;
	m_IDev = 80000;

    m_StepenP = 1000;


	m_GMreza = 5000;
	m_ITabla = 0;
	m_PI = 3.14159;
	m_Grav = 9.81;
	m_Mf1 = 1; // 0
	m_Mf2 = 0;
	m_Mf3 = 0;
	m_PAG = 0;
	m_TAG = 0;




	m_tropos = 11000;
	m_troposp = 12000;
	m_stratos = 55000;
	m_mezos = 80000;

	m_PTG =0;
	m_PadT = 6,5;
	m_To = 25;
	m_Orgfx =90;
	m_Orgfy =620;


	m_FodT =  "";
	m_InfoTabla = ("");

    m_dlgFODT = NULL;
	m_dlgINFO = NULL;

}

CKreatorTrajektorijaView::~CKreatorTrajektorijaView()
{
}

BOOL CKreatorTrajektorijaView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}


/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaView drawing

void CKreatorTrajektorijaView::OnDraw(CDC* pDC)
{
	//CKreatorTrajektorijaDoc* pDoc = GetDocument();
	//ASSERT_VALID(pDoc);
//////////////////////////////////////////////////////
//CENTAR I GRAFICKI PRIKAZ
//////////////////////////////////////////////////////
	


	CClientDC;
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetViewportOrg(m_Orgfx, m_Orgfy); //90 -620
	pDC->SetWindowExt(m_Zumx,m_Zumy); //SetWindowExtent(100,100)  SetViewportExtent(200,200)  100 jedinica = 200 pixela
	                                                                                             //1 m = 2 pixela
	
	pDC->SetViewportExt(1000,-1000);// Orijentacija osa
	//pDC->SetWindowOrg(m_Orgfx,m_Orgfy);
	

    




//////////////////////////////////////////////////////
//ATMOSFERA PRIKAZ
//////////////////////////////////////////////////////


CString stringA;


CPen PenBlue1(PS_SOLID, 1, RGB(20,20,20));


pDC->SelectObject(PenBlue1);

int tropos = m_tropos;
	
	
int troposp = m_troposp;
int stratos = m_stratos;
int mezos = m_mezos;

switch (m_PAG)
	{
	case 0:
		
	      
	break;



	case 1:
		pDC->MoveTo(0, tropos);
pDC->LineTo(150000, tropos);
//troposfera
	CBrush brush2 (RGB (150, 150, 255));
	pDC->SelectObject(brush2);
	pDC->Rectangle(0,0,150000,tropos);

	

//tropopauza

pDC->MoveTo(0, troposp);
pDC->LineTo(150000, troposp);
    CBrush brush3 (RGB (250, 250, 255));
	pDC->SelectObject(brush3);
	pDC->Rectangle(0,tropos,150000,troposp);
	
	
	
          


//stratosfera
pDC->MoveTo(0, stratos);
pDC->LineTo(150000, stratos);
    CBrush brush4 (RGB (250, 250, 255));
	pDC->SelectObject(brush4);
	pDC->Rectangle(0,troposp,150000,stratos); 
	
	
	      
//mezosfera
	
    CBrush brush5 (RGB (252, 252, 255));
	pDC->SelectObject(brush5);
	pDC->Rectangle(0,55000,150000,80000);
	
		break;
	}

switch (m_TAG)
	{
	case 0:
		
	      
	break;



	case 1:
		pDC->SetTextColor(RGB(0, 0, 255));
    stringA.Format (("Troposfera od %d, do %d m "), 0, tropos);

    pDC->TextOut (90000,tropos/2 , stringA);
	
	pDC->SetTextColor(RGB(0, 0, 255));
    stringA.Format (("Tropopauza od %d, do %d m "), tropos, troposp);

    pDC->TextOut (90000,17800 , stringA);
 

    pDC->SetTextColor(RGB(0, 0, 255));
    stringA.Format (("Stratosfera od %d, do %d m "), troposp, stratos);
    pDC->TextOut (90000,(stratos+troposp)/2 , stringA);

    pDC->SetTextColor(RGB(0, 0, 255));
    stringA.Format (("Mezosfera od %d, do %d m "), stratos, mezos);

    pDC->TextOut (90000,stratos+2000 , stringA);
		break;
	}



//merac sa tekstom

////////////////////////////////////////////////////////
/////TEMPERATURA
////////////////////////////////////////////////////////

CPen PenBlackT(PS_SOLID, 10, RGB(0,0,0));


pDC->SelectObject(PenBlackT);


switch (m_PTG)
	
	{
	case 0:
    
	

	break;

	case 1:
		
    
        
    
	
    //m_To = 25;
    //m_PadT = 6,5;

    double dT=m_To;
	CString string;
	pDC->SetTextColor(RGB(255, 0, 0));

	for(int iT = 1000; iT<m_tropos;iT +=1000)
	{		
		dT-=m_PadT;
		

		  string.Format ("T: ");
		  pDC->TextOut (84000,1500+2*iT , string);
		  char ct1[10];		 
		  sprintf(ct1,"%2f",dT);
		  pDC->TextOut (87000,1500+2*iT ,ct1);
		  stringA.Format (("na %d m "), iT);
          pDC->TextOut (95000,1500+2*iT , stringA);

		
	
	}
		break;
	}

///////////////////////////////////////////////////////

//	
//////////////////////////////////////////////////////	
//KONSTANTE
//////////////////////////////////////////////////////	
double PI = m_PI;
double g = m_Grav;
//////////////////////////////////////////////////////		



//////////////////////////////////////////////////////
//MREZA
//////////////////////////////////////////////////////	
	CPen PenBlack4(PS_SOLID, 1, RGB(0, 0, 0));
	pDC->SelectObject(PenBlack4);
	
	switch (m_Mreza)
	
	{
	case 0:
    
	

	break;

	case 1:
		for(int kooxx = 0; kooxx < 1000000; kooxx += m_GMreza)
    {
        pDC->MoveTo(kooxx, 0);
        pDC->LineTo(kooxx, 1500000);
    }
	

	for(int kooyy = 0; kooyy < 1000000; kooyy += m_GMreza)
    {
        pDC->MoveTo(0, kooyy);
        pDC->LineTo(1500000, kooyy);
    }
		break;
	}
//////////////////////////////////////////////////////	
//PROMENE BRZINE I UGLA
//////////////////////////////////////////////////////	
int alfa1 = m_Ugao_f1;
int alfa2 = m_Ugao_f2;
int alfa3 = m_Ugao_f3;

double v1 = m_Brzina_f1;
double v2 = m_Brzina_f2;
int v3 = m_Brzina_f3;

///////////////////////////////////////////////////////
//OTPOR
///////////////////////////////////////////////////////

double c2 = m_OtporF2;

///////////////////////////////////////////////////////
//PRIKAZ TABLE
///////////////////////////////////////////////////////
 switch (m_ITabla)
	
	{
	case 0:
	
    
	

	break;

	case 1:
		CRect Proz;
		GetClientRect(&Proz);
		 
		int pozTx = 1000;
		int pozTy = 25000;

          //61-80
		CString string;
//////////////////////////////////////////////////////////////////////////////
//Tekst parametri za funkciju F1 CRVENA
//////////////////////////////////////////////////////////////////////////////
		  pDC->SetTextColor(RGB(255, 0, 0));
          string.Format (("Ugao: %d;"),alfa1);
          pDC->TextOut (60000+pozTx,35000+pozTy , string);//pDC->TextOut (60000+pozTx,35000+pozTy , string);

		  string.Format ("Brzina: ");
		  pDC->TextOut (60000+pozTx,33000+pozTy , string);
		  char cv1[10];		 
		  sprintf(cv1,"%2f",v1);
		  pDC->TextOut (69000+pozTx,33000+pozTy ,cv1);

		  string.Format ("Vreme Leta: ");
		  pDC->TextOut (60000+pozTx,31000+pozTy , string);
          char ct1[10];
		  double resH1 = sin (alfa1*PI/180);
		  double VREME1 = (2*v1*resH1)/(m_Grav);
		  sprintf(ct1, "%2f",VREME1);
		  pDC->TextOut (69000+pozTx,31000+pozTy ,ct1);

		  string.Format ("Domet:");
		  pDC->TextOut (60000+pozTx,29000+pozTy , string);
		  double VISINA1 = ((v1*v1*resH1*resH1)/(2*m_Grav));
		  char cd1[10];
		  double resD1;
          resD1 = sin (2*alfa1*PI/180);
          double DOMET1 = (v1*v1*resD1)/m_Grav;
		  sprintf(cd1, "%2f",DOMET1);
		  pDC->TextOut (69000+pozTx,29000+pozTy ,cd1);

		  string.Format ("Visina:");
		  pDC->TextOut (60000+pozTx,27000+pozTy , string);
		  char ch1[10];
		  sprintf(ch1,"%2f",VISINA1);
		  pDC->TextOut (69000+pozTx,27000+pozTy ,ch1);

//////////////////////////////////////////////////////////////////////////////
//Tekst parametri za funkciju F2 PLAVA
//////////////////////////////////////////////////////////////////////////////

		  pDC->SetTextColor(RGB(0, 0, 255));
          string.Format (("Ugao: %d;"),alfa2);
          pDC->TextOut (60000+pozTx,22000+pozTy , string);

		  string.Format ("Brzina: ");
		  pDC->TextOut (60000+pozTx,20000+pozTy , string);
		  char cv2[10];		 
		  sprintf(cv2,"%2f",v2);
		  pDC->TextOut (69000+pozTx,20000+pozTy ,cv2);

		  string.Format ("Vreme Leta: ");
		  pDC->TextOut (60000+pozTx,18000+pozTy , string);
          char ct2[10];
		  double resH2 = sin (alfa2*PI/180);
		  double VREME2 = (2*v2*resH1)/(m_Grav);
		  sprintf(ct2, "%2f",VREME2);
		  pDC->TextOut (69000+pozTx,18000+pozTy ,ct2);

		  string.Format ("Domet:");
		  pDC->TextOut (60000+pozTx,16000+pozTy , string);
		  double VISINA2 = ((v2*v2*resH1*resH2)/(2*m_Grav));
		  char cd2[10];
		  double resD2;
          resD2 = sin (2*alfa2*PI/180);  // operator... sin (2alfa)
          double DOMET2 = (v2*v2*resD2)/m_Grav;
		  sprintf(cd2, "%2f",DOMET2);
		  pDC->TextOut (69000+pozTx,16000+pozTy ,cd2);

		  string.Format ("Visina:");
		  pDC->TextOut (60000+pozTx,14000+pozTy , string);
		  char ch2[10];
		  sprintf(ch2,"%2f",VISINA2);
		  pDC->TextOut (69000+pozTx,14000+pozTy ,ch2);




/*
*/




		  
		  
		  //
		  //
		  //

          //pDC->SetTextColor(RGB(0, 0, 255));
          //string.Format (("-Funkcija f2 Ugao %d, Brzina %d m/s "), alfa2, v2);
          //pDC->TextOut (60000,33000 ,str);

          //pDC->SetTextColor(RGB(0, 255, 0));
          //string.Format (("-Funkcija f3 Ugao %d, Brzina %d m/s "), alfa3, v3);
          //pDC->TextOut (60000,31000 , string);
		  
	
		break;
	}    

CFont font;
font.CreatePointFont (720, _T ("Times New Roman"));







//////////////////////////////////////////////////////
// OSE
//////////////////////////////////////////////////////	
	pDC->MoveTo(-1000,     0);
	pDC->LineTo( 20000000,     0);
	pDC->MoveTo(   0, -1000);
	pDC->LineTo(   0,  20000000);
//////////////////////////////////////////////////////	
//POZICIJA OZNAKA
//////////////////////////////////////////////////////		
	
	//Koordinate 
	//////////////////////////////////////////////////////	
	pDC->SetTextColor(RGB(100, 100, 0));

    pDC->TextOut(56000, 2000, 'X');
	pDC->TextOut(500, 35000, 'Y');
   //////////////////////////////////////////////////////	
   
	
	
	//Lenjiri po x i y osi
	//////////////////////////////////////////////////////	

    CPen PenBlack3(PS_SOLID, 2, RGB(0, 0, 0));
	pDC->SelectObject(PenBlack3);
    
	for(int koox = 0; koox < 1000000; koox += 1000)
    {
        pDC->MoveTo(koox, 0);
        pDC->LineTo(koox, -1000);
		
    }

	for(int kooy = 0; kooy < 1000000; kooy += 1000)
    {
        pDC->MoveTo(0, kooy);
        pDC->LineTo(-1000, kooy);
		
    }


    //MERNE JEDINICE NA OSAMA

     //CFont font;
    //font.CreatePointFont (720, _T ("Comic Sans MS"));




	pDC->SetTextColor(RGB(0, 0, 0));
	for (int kt1= 0; kt1<1000000; kt1+=10000) 
	{
		

        
        CString string;
        string.Format ((" %d km "), (kt1/1000 ) );
        pDC->TextOut (kt1, -3000, string);
    }

	pDC->SetTextColor(RGB(0, 0, 0));
	for (int kt2= 1000; kt2<1000000; kt2+=10000) 
	{
        
        CString string;
        string.Format ((" %d km "), (((kt2-1000)/1000) ) );
        pDC->TextOut (-8000, kt2, string);///////////**********************************NACI FONT SIZE
		
		
    }


    //MERNE JEDINICE NA 5000m I 10000m NA X I Y
	pDC->SelectObject(PenBlack3);
    for(int koox2 = 0; koox2 < 1000000; koox2 += 5000)
    {
        pDC->MoveTo(koox2, 0);
        pDC->LineTo(koox2, -2000);
	

    }
	for(int kooy2 = 0; kooy2 < 1000000; kooy2 += 5000)
    {
        pDC->MoveTo(0, kooy2);
        pDC->LineTo(-2000, kooy2);
	
    }

	for(int koox3 = 0; koox3 < 1000000; koox3 += 10000)
    {
        pDC->MoveTo(koox3, 0);
        pDC->LineTo(koox3, -3000);
	
    }
	for(int kooy3 = 0; kooy3 < 1000000; kooy3 += 10000)
    {
        pDC->MoveTo(0, kooy3);
        pDC->LineTo(-3000, kooy3);
	
    }

///OPCIONE MERE NA LENJIRU
	for(int koox4 = 0; koox4 < 1000000; koox4 += 100)
    {
        pDC->MoveTo(koox4, 0);
        pDC->LineTo(koox4, -30);
	
    }
	for(int kooy4 = 0; kooy4 < 1000000; kooy4 += 100)
    {
        pDC->MoveTo(0, kooy4);
        pDC->LineTo(-30, kooy4);
	
    }
	for(int koox5 = 0; koox5 < 1000000; koox5 += 500)
    {
        pDC->MoveTo(koox5, 0);
        pDC->LineTo(koox5, -60);
	
    }
	for(int kooy5 = 0; kooy5 < 1000000; kooy5 += 500)
    {
        pDC->MoveTo(0, kooy5);
        pDC->LineTo(-60, kooy5);

    }











	




//////////////////////////////////////////////////////	
//FUNKCIJE
//////////////////////////////////////////////////////	


//KORAK I MAKSIMUM
//////////////////////////////////////////////////////	

double kor = m_Korak;

double max = m_IDev; //oko milion



//////////////////////////////////////////////////////	



pDC->SelectObject(PenBlack4);


////////////////////////////////////////////////////////////////////////////////////////////////////////////
double d1;

////////////////////////////////////////////////////////////////////////////////////////////////////////////		
//Funkcija f1 CRVENA
////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	switch (m_Mf1)
	{
	case 0:
		
	      
	break;



	case 1:
		for(double i = 1; i < max; i +=kor)// i = X   // f = Y   // korak i += 15  max = 150000   150000/15 = broj tacaka = 10.000
	{ 
        for(double c1 = 1; c1 < 11000000; c1 +=10000000)
		
		{
			
			
	


	    double f =(i*tan((PI*alfa1)/180))-((g*i*i)/(2*(v1*((i+c1)/(i+log10(i)))-(c2*i))*(v1*((i+c1)/(i+log10(i)))-(c2*i))*cos((PI*alfa1)/180)*cos((PI*alfa1)/180)));
			//
		    // double f =1*i*tan(100*i)*sin(i);
        
		
    
	    pDC->SetPixel(i, f, RGB(255, 0, 0));
		if (f >= 0)
		{
			
			d1 = i;
			char cdometf1[10];		 
            sprintf(cdometf1,"Domet:    %2f",d1);
            pDC->TextOut (69000,20000 ,cdometf1);

			char ch1f1[10];
			sprintf(ch1f1,"Visina:    %2f",(i*tan((PI*alfa1)/180))-((g*i*i)/(2*(v1*((i+c1)/(i+log10(i)))-(c2*i))*(v1*((i+c1)/(i+log10(i)))-(c2*i))*cos((PI*alfa1)/180)*cos((PI*alfa1)/180))));
			//);
            pDC->TextOut (69000,18000 ,ch1f1);


			break;
			
			
			
		}
		
		
	
	
		}
		
		
	}
		break;
	}


/////////////////////////////////////////////////////////////
	switch (m_Mf2)
	{
	case 0:
		
	     
	break;



	case 1:
////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//Funkcija f2 PLAVA
///////////////////////////////////////////////

	
		for(double i2 = 1; i2 < max; i2 +=kor)
		{
	  
		
		
	    double f2 = (i2*tan((PI*alfa2)/180))-((g*i2*i2)/(2*(v2-(c2*i2))*(v2-(c2*i2))*cos((PI*alfa2)/180)*cos((PI*alfa2)/180)));
			//tan(i2)*i2;
	    pDC->SetPixel(i2, f2, RGB(0, 0, 255));
	
			
			
		
		}
	

	
	
		break;
	}




	
////////////////////////////////////////////////////////////////////////////////////////////////////////////		
//Funkcija f3 ZELENA
////////////////////////////////////////////////////////////////////////////////////////////////////////////	
    switch (m_Mf3)
	{
	case 0:
		
	      
	break;



	case 1:	

		for(double t = 0; t < 300; t +=5)
	{
		for(double i3 = 1; i3 < max; i3 +=kor)
		
		{
		int c3 = 0;
	    double f3 = (i3*tan((PI*alfa3+t)/180))-((g*i3*i3)/(2*(v3)*(v3)*cos((PI*alfa3+t)/180)*cos((PI*alfa3+t)/180)));
	    pDC->SetPixel(i3, f3, RGB(0, 200, 0));
		}   
    }
		break;
	}
	
	
	
	/*

	
	*/
//////////////////////////////////////////////////////////////////////////////
//GRAFIK DONJI PRIKAZ    
//////////////////////////////////////////////////////////////////////////////	
	 
	CBrush brush (RGB (0, 255, 255));
	pDC->SelectObject(brush);
    	CPen PenBela(PS_SOLID, 200, RGB(255, 255, 255));

	switch (m_DGrafik)
	{
	case 0:
	
	    //pDC->SelectObject(PenBlack3);
		pDC->FillSolidRect(-1000000,-5000,2000000,-10000000,RGB (255, 255, 255));
		//pDC->Rectangle(-1000000,-5000,2000000,-10000000);
	
	      
	break;



	case 1:
		
		
		  
		break;
	}
/////////////////////////////////////////////////////////////////////////////


	
	





ReleaseDC(pDC);
}



/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaView diagnostics

#ifdef _DEBUG
void CKreatorTrajektorijaView::AssertValid() const
{
	CView::AssertValid();
}

void CKreatorTrajektorijaView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CKreatorTrajektorijaDoc* CKreatorTrajektorijaView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CKreatorTrajektorijaDoc)));
	return (CKreatorTrajektorijaDoc*)m_pDocument;
}
#endif //_DEBUG


/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaDVView message handlers

/////////////////////////////////////////////////
                         //////////
                           
                          ////////

                            //                       


void CKreatorTrajektorijaView::OnUputstvo() 
{
	CUputstvo dlg;
	dlg.DoModal();
	
}

void CKreatorTrajektorijaView::OnZum() 
{
	CZum dlg;
	dlg.m_Zumx = m_Zumx;
	dlg.m_Zumy = m_Zumy;
	
	if (dlg.DoModal () == IDOK)
	{
		m_Zumx= dlg.m_Zumx;
		m_Zumy= dlg.m_Zumy;
		Invalidate ();
	}
	
}

void CKreatorTrajektorijaView::OnParametri() 
{
	CParametri dlg;

    dlg.m_Brzina_f1 =m_Brzina_f1;
	dlg.m_Brzina_f2 =m_Brzina_f2;
	dlg.m_Brzina_f3 =m_Brzina_f3;

	dlg.m_OtporF2 = m_OtporF2;

	dlg.m_Ugao_f1 =m_Ugao_f1;
	dlg.m_Ugao_f2 =m_Ugao_f2;
	dlg.m_Ugao_f3 =m_Ugao_f3;

	dlg.m_Grav= m_Grav;
	dlg.m_PI= m_PI;

	dlg.m_Mf1 = m_Mf1;
	dlg.m_Mf2 = m_Mf2;
	dlg.m_Mf3 = m_Mf3;


    if (dlg.DoModal () == IDOK)
	{
        m_Brzina_f1 = dlg.m_Brzina_f1;
		m_Brzina_f2 = dlg.m_Brzina_f2;
		m_Brzina_f3 = dlg.m_Brzina_f3;

        m_OtporF2 = dlg.m_OtporF2;

		m_Ugao_f1 = dlg.m_Ugao_f1;
		m_Ugao_f2 = dlg.m_Ugao_f2;
		m_Ugao_f3 = dlg.m_Ugao_f3;

		m_Grav = dlg.m_Grav;
		m_PI = dlg.m_PI;

		m_Mf1=dlg.m_Mf1;
		m_Mf2=dlg.m_Mf2;
		m_Mf3=dlg.m_Mf3;



             
        Invalidate ();
    }    
	
}

void CKreatorTrajektorijaView::OnMreza() 
{
	CMreza dlg;
	dlg.m_Mreza = m_Mreza;
	dlg.m_GMreza = m_GMreza;
	
	if (dlg.DoModal () == IDOK)
	{
		m_Mreza= dlg.m_Mreza;
		m_GMreza= dlg.m_GMreza;
		Invalidate ();
	}
	
}

void CKreatorTrajektorijaView::OnKorak() 
{
	CKorak dlg;
	dlg.m_Korak = m_Korak;
	dlg.m_IDev = m_IDev;
	dlg.m_StepenP = m_StepenP;

	if(dlg.DoModal() == IDOK)
	{
	m_Korak = dlg.m_Korak;
	m_IDev = dlg.m_IDev;
	m_StepenP = dlg.m_StepenP;
	Invalidate();
	}
	
}

void CKreatorTrajektorijaView::OnDgrafik() 
{
	CDGrafik dlg;
	dlg.m_DGrafik = m_DGrafik;
	
	if (dlg.DoModal () == IDOK)
	{
		m_DGrafik= dlg.m_DGrafik;
		Invalidate ();
	}
	
}

void CKreatorTrajektorijaView::OnTabla() 
{
	CTabla dlg;
	dlg.m_ITabla = m_ITabla;

	if(dlg.DoModal()==IDOK)
	{
		m_ITabla = dlg.m_ITabla;
		Invalidate();
	}
	
}

void CKreatorTrajektorijaView::OnAutor() 
{
	CAutorDV dlg;
	dlg.DoModal();
	
}

void CKreatorTrajektorijaView::OnAtmosfera() 
{
	CAtmosfera dlg;
	dlg.m_tropos = m_tropos;
	dlg.m_troposp = m_troposp;
	dlg.m_mezos = m_mezos;
	dlg.m_stratos = m_stratos;
	dlg.m_PAG = m_PAG;
	dlg.m_TAG = m_TAG;
	dlg.m_PTG = m_PTG;
	dlg.m_PadT = m_PadT;
	dlg.m_To = m_To;


	
	if (dlg.DoModal () == IDOK)
	{
		m_tropos = dlg.m_tropos;
		m_troposp = dlg.m_troposp;
	    m_mezos = dlg.m_mezos;
	    m_stratos = dlg.m_stratos;
	    m_PAG = dlg.m_PAG;
	    m_TAG =dlg.m_TAG;
		m_PTG = dlg.m_PTG;
	    m_PadT = dlg.m_PadT;
	    m_To = dlg.m_To;

		Invalidate ();
	}
	
}

void CKreatorTrajektorijaView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	
switch (nChar)
 {
	case VK_RIGHT :
			m_Orgfx-=50;
			m_Mf1=0;
	
			Invalidate();
         break;

	case VK_LEFT :
		m_Orgfx+=50;
		m_Mf1=0;
	
			Invalidate();
		break;

	case VK_UP :
		m_Orgfy+=50;
		m_Mf1=0;
	
			Invalidate();
		break;

    case VK_DOWN:
		m_Orgfy-=50;
		m_Mf1=0;
		Invalidate();
		break;
    
	case VK_HOME:
		m_Orgfx = 90;
		m_Orgfy = 620;
		m_Zumx= 100000;
        m_Zumy = 100000;
		Invalidate();
		break;

	case VK_F1:
		m_Zumx +=1000;
		m_Zumy +=1000;
		m_Mf1 = 0;
		Invalidate();
		break;

	case VK_F2:
		m_Zumx -=1000;
		m_Zumy -=1000;
		m_Mf1 = 0;
		if (m_Zumx < 1000)
		{
			m_Zumx =1000;
			m_Zumy =1000;
		}
		Invalidate();
		break;

	case VK_F3:

		m_Korak -=1;
		Invalidate();
		if (m_Korak <=0)
		{
			m_Korak = 1;
		}
		
		
		break;

	case VK_F4:
		m_Korak +=1;
		Invalidate();
		
		break;

	case VK_F9:
		m_IDev +=m_StepenP;
		Invalidate();
		break;


	case VK_SPACE:
		m_IDev -=m_StepenP;
		Invalidate();
		break;

	case 0x31: //1 ----1 on 0 off
	  	m_Mf1 = 1;
		Invalidate();
		break;
			 
			
		

	case 0x32://2
		m_Mf1 = 0;
		Invalidate();
		break;




}
       

   
	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}



void CKreatorTrajektorijaView::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	switch (nChar)
	{
	case VK_UP:
		m_Mf1 = 1;
		Invalidate();
		break;
	case VK_LEFT:
		m_Mf1 = 1;
		Invalidate();
		break;
	case VK_RIGHT:
		m_Mf1 = 1;
		Invalidate();
		break;
	case VK_DOWN:
		m_Mf1 = 1;
		Invalidate();
		break;
	case VK_F1:
		m_Mf1 = 1;
		Invalidate();
		break;
	case VK_F2:
		m_Mf1 = 1;
		Invalidate();
		break;


	}
 
	








	CView::OnKeyUp(nChar, nRepCnt, nFlags);
}

void CKreatorTrajektorijaView::OnFodt() 
{
	

	//m_FodT 
	//char ch2[10];
	//int t = 30;
	//for(int iT = 10; iT<20;iT +=1)
	//{		
		 
	//}
	//int v1 = 100;
	//char cv1[10];		 
	//sprintf(cv1,"%2f",v1)
   //=("ura \r\n   hec) ;
	// sprintf(ch2,"%2f",VISINA2);
	//itoa SetDlgItemText
/*	
CTty3Dlg dlg;
m_pMainWnd = &dlg;
char textbuffer[40];
double t1= 10.0, t2=20.0;
sprintf(textbuffer, "%f\n",t1);
dlg.m_MSG = textbuffer;
sprintf(textbuffer, " %f", t2);
dlg.m_MSG = dlg.m_MSG + textbuffer;
int nResponse = dlg.DoModal();
	*/
	//CString strLine;
   // add CR/LF to text
   //strLine.Format(_T("\r\n%s"), pszText);
   //AppendTextToEditCtrl(edit, strLine);



		
    m_dlgFODT = new CFODT;
    double t3;
	t3 = 10;
	
    char c1[40];
	double t1= 10.0, t2=20.0;
	sprintf(c1, "%f\r\n",t3);
    m_dlgFODT->m_FodT = c1;
	sprintf(c1, "%f",t2);
	m_dlgFODT->m_FodT = m_dlgFODT->m_FodT + c1;
	
    

	m_dlgFODT->Create();
    m_dlgFODT->ShowWindow (SW_SHOW);
	
}

void CKreatorTrajektorijaView::OnTablaB() 
{
	m_dlgINFO = new CTablab;

	double vf1;
	double vf2;
	
	double uf1;
	double uf2;
	
	char cv1[40];
	char cv2[40];
	char cu1[40];
	char cu2[40];

    char nazivf1[40] ="PARAMETRI FUNKCIJE F1 [CRVENA]\r\n" ;
    

    m_dlgINFO->m_InfoTabla = nazivf1;
	
	
	
	vf1 = m_Brzina_f1;
	sprintf(cv1, "Brzina:        %f m/s\r\n",vf1);
     

	uf1 = m_Ugao_f1;
	sprintf(cu1, "Ugao:         %f stepeni\r\n",uf1);

    //m_dlgINFO->m_InfoTabla = m_dlgINFO->m_InfoTabla  + cu1;

	char ctd1[50];
    double d_resH1 = sin (uf1*m_PI/180);
    double d_VREME1 = (2*vf1*d_resH1)/(m_Grav);
	sprintf(ctd1, "Vreme leta: %f s\r\n",d_VREME1);

	
	char cdh1[50];
    double d_VISINA1 = ((vf1*vf1*d_resH1*d_resH1)/(2*m_Grav));
    sprintf(cdh1, "Visina:        %f m\r\n",d_VISINA1);


	char cdd1[50];
	double d_resD1;
    d_resD1 = sin (2*uf1*m_PI/180);
    double d_DOMET1 = (vf1*vf1*d_resD1)/m_Grav;
    sprintf(cdd1, "Domet:       %f m\r\n",d_DOMET1);

	

	m_dlgINFO->m_InfoTabla = m_dlgINFO->m_InfoTabla +cv1 + cu1 + ctd1 + cdh1 + cdd1 ;

   	m_dlgINFO->Create();
    m_dlgINFO->ShowWindow (SW_SHOW);
	
}




