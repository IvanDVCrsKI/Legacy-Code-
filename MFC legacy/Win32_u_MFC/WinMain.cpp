#include <afxwin.h> // afxwin.h sadrzi definicije MFC klasa


/* 
------------------------------------------------------------------------------------------------------------------
Za pravljenje programa koriste se 2 MFC klase :

  -application class
  -window class

------------------------------------------------------------------------------------------------------------------
WinMain() funkcija je time ukljucena i ne treba biti deklarisana
------------------------------------------------------------------------------------------------------------------

*/

class CExerciseApp : public CWinApp // CWinApp je application class (C u imenu je za MFC class)
{
public:
	virtual BOOL InitInstance();// Osnovna funkcija u WinApp klasi (virtualna funkcija u baznoj klasi vrednosti vuce iz Win32 API)
};                              // NAPOMENA afxwin.h mora biti ukljucen da bi se koristila data funkcija

class CMainFrame : public CFrameWnd //CFrameWnd je window class  (FRAME WINDOW) sadrzi sve u okviru prozora
{
public:
	CMainFrame();//konstruktor
};

CMainFrame::CMainFrame()
{
	Create(NULL, "MFC Fundamentals");
}

/*
ili na drugi nacin preko konstruktora klase
*/


BOOL CExerciseApp::InitInstance()
{
	m_pMainWnd = new CMainFrame; // cuvamo adresu aplikacije
	m_pMainWnd->ShowWindow(m_nCmdShow);// prikazi dat prozor SW_NORMAL NULL ZA HIDE

	return TRUE;
}

CExerciseApp APLIKACIJA;// definicija objekta aplikacije