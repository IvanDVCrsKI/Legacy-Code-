// UputstvoDLG.cpp : implementation file
//

#include "stdafx.h"
#include "KreatorTrajektorijaMDI.h"
#include "UputstvoDLG.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUputstvoDLG dialog


CUputstvoDLG::CUputstvoDLG(CWnd* pParent /*=NULL*/)
	: CDialog(CUputstvoDLG::IDD, pParent)
{
	//{{AFX_DATA_INIT(CUputstvoDLG)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CUputstvoDLG::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUputstvoDLG)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CUputstvoDLG, CDialog)
	//{{AFX_MSG_MAP(CUputstvoDLG)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUputstvoDLG message handlers
