#if !defined(AFX_PARAMETRIDLG_H__D22DCAE2_086D_4697_9745_6B9ADBF51FF3__INCLUDED_)
#define AFX_PARAMETRIDLG_H__D22DCAE2_086D_4697_9745_6B9ADBF51FF3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ParametriDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CParametriDlg dialog

class CParametriDlg : public CDialog
{
// Construction
public:
	CParametriDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CParametriDlg)
	enum { IDD = IDD_PARAMETRI };
	int		m_Brzina_f1;
	int		m_Brzina_f2;
	int		m_Brzina_f3;
	int		m_Ugao_f1;
	int		m_Ugao_f2;
	int		m_Ugao_f3;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CParametriDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CParametriDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PARAMETRIDLG_H__D22DCAE2_086D_4697_9745_6B9ADBF51FF3__INCLUDED_)
