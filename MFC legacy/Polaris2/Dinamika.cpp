#include "stdafx.h"
#include "Polaris.h"

#include "ChildFrm.h"
/*
ako se zanemari masa ruke


  sum Mo = Mog+Mob+M
  sum Mo = Rgo X F + Rbo X F + M
  sum Mo = Rgo X (-mg j) + Rbo X (-mg j) + M kwwwwwwwww


  Rgo = duzina*(projekcija na x ili projekcija na y)
  Rgo = duzg2*(cos(alfa1)*i))



  Rgo X (-mg j) = (duzg2/2*cos(alfa1)*i)) X (-mg j) 
  
	(duzg2/2)*cos(alfa1)*(-mg)*(i X j) = (duzg2/2)*cos(alfa1)*(-mg)*k

  Rbo X (-mg j) = (duzg2*cos(alfa1)*i)) X (-mg j) 


  sum Mo = (duzg2/2)*cos(alfa1)*(-mg)*k   +    (duzg2)*cos(alfa1)*(-mg)*k  +  M*k

  sum Mo = -((3*duzg2)/2)*cos(alfa1)*mg*k + M*k

  sum Mo = 0

  M = -((3*duzg2)/2)*cos(alfa1)*mg*k
------------------------
Vektori
 
  i X j = k
  j X j = 0

  

-82
  i X j = k
  j X k = i
  k X i = j

  j X i = -k
  

  i X i = 0


  i*i=1
  i*j=0


              | i  j  k |
  A X B = det | Ax Ay Az|  = i(Ay*Bz-By*Az)-j(Ax*Bz-Bx*Az)+k(Ax*By-Bx*Ay)
              | Bx By Bz|

  po intenzitetu
  A X B = |A|*|B|*sin(A,B) * n   n normalno na A i normalno na B  n = i ili j ili k

  
  A X A = 0

  A X (B + C) =A X B + A X C

  A X (B X C) = (A*C)*B - (A*B)*C


  A*B=|A|*|B|*cos(A,B) 

  A*(B X C) = A*B X C ili B X C*A

  A*A = A^2

  A*B = C

  ako je A normalno na B A*B=0

  A~=A*i
  w~=w*k


  w~ X r~ =w*k X r*i = w*r*(i X k) = w*r*j


  w~ X r~  = w*k X L*j = w*L*(k*j) = w*L*(-i)

  w~ X r~ = w*k X (L*i + L*j) = w*k X L*i + w*k X L*j = w*L*(k X i) + w*L*(k X j) = w*L*(j)+w*L*(-i) = w*L*(j-i)

orijentacija osa i usmerenje jedinicnih vektora BITNO za projekcije !!! 53 teorija str 671 primer

projekcije 

  standardne
  rx =r*cos(-15)
  ry =r*sin(+15)

  alternativno u zavisnosti od orijentacije ugla
  r~ = r*i+r*j
  r~ = rx*i + ry*j


intenzitet
    koren kvadrata projekcija

  |r~| = sqrt(rx^2+ry^2)


prikaz u vidu vektora

  jedinicni vektor = r~/|r~| = (30i +40j)/50  = 0.6i + 0.8j 


  w~ X r~ = w*k X (?)  r~= R(cos*i -+sin*j)

--------------------------------BITNOOOOOOOOO!!!!!!
  w X (w X R) = (w*R)*w-(w*w)*R

  w normalno na R => 0

  -w^2*R
--------------------------------
                            +j(Fx*rz-rx*Fz)
  M = r X F = i(ry*Fz-Fy*rz)-j(rx*Fz-Fx*rz)+k(rx*Fy-Fx*ry)
  693 

*/

/*
POLARNE KOORDINATE


  R~ = R*er
  V = R~/dt = (R*er)/dt = R/dt+er/dt  
  ako se R tj Duzina L nemenja dR/dt=0 => 0*er+R*v_er  
  
  V = R*v_er

  er  vektor - prati tacku koja rotira 
  efi vektor - 90 stepeni + od vektora er -prati ugao


  er = cos(fi)*i+sin(fi)*j - projekcija vektora er na i i j jedinicne vektore

  efi = cos(fi+90)*i+sin(fi+90)*j

  efi = -sin(fi)*i+cos(fi)*j


///////////////////////////

 er = cos(fi)*i+sin(fi)*j 
 efi = -sin(fi)*i+cos(fi)*j

///////////////////////////
  
	
	  
Brzine

  v_er = d(cos(fi)*i+sin(fi)*j)/dt

 d(cos(fi)*i/dt = -sin(fi)*v_fi*i
 d(sin(fi)*j)/dt= cos(fi)*v_fi*j
   
	 v_er =-sin(fi)*v_fi*i+cos(fi)*v_fi*j = v_fi*(-sin(fi)*i+cos(fi)*j)= v_fi*efi !!!

  v_efi = (-sin(fi)*i+cos(fi)*j)/dt

  d(-sin(fi)*i)/dt = -cos(fi)*v_fi*i
  d(cos(fi)*j)/dt = -sin(fi)*v_fi*j

  v_efi = -(cos(fi)*v_fi*i+sin(fi)*v_fi*j) = -v_fi*(cos(fi)*i+sin(fi)*j) = -v_fi*er !!!

///////////////////////////
er =  v_fi*efi
efi = -v_fi*er
///////////////////////////
*/

/*
sile

sum F = Fn + Ft

F = Fx*i+Fy*j+Fz*k

sum F = Rx*i+Ry*j-M*g*j-m*g*j =Rx*i+(Ry-m*g-M*g)*j /*i

sum Fi = Rx

sum F = Rx*i+Ry*j-M*g*j-m*g*j =Rx*i+(Ry-m*g-M*g)*j /*j

sum Fj =(Ry-m*g-M*g)

a = aug+aub

aug = 0+ L/2*[(w^2*-sin(alfa1)*i+w^2*cos(alfa1)*j)]
aub = 0+ L*[(w^2*-sin(alfa1)*i+w^2*cos(alfa1)*j)]

a = L/2*[(w^2*-sin(alfa1)*i+w^2*cos(alfa1)*j)]+L*[(w^2*-sin(alfa1)*i+w^2*cos(alfa1)*j)]

Rx = m*L/2*[(w^2*-sin(alfa1)]+M*L*[(w^2*-sin(alfa1)*i]


(Ry-m*g-M*g) = m*L/2*[(w^2*cos(alfa1)]+M*L*[(w^2*cos(alfa1)]



Ry = m*g+M*g+m*L/2*[(w^2*cos(alfa1)]+M*L*[(w^2*cos(alfa1)]

*/

/*
snaga


  P = M*v_alfa1
  v_alfa1 = 60 opm
  v_alfa1 = 0.017rad/s
  v_alfa1 = 1 stepen/s


  P = 10000Nm*60opm

  P = 600000Nm*2*PI/60
*/