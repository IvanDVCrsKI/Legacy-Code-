// cKORView.h : interface of the CCKORView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CKORVIEW_H__35ECF3F0_199A_46F7_B326_C783AC14303A__INCLUDED_)
#define AFX_CKORVIEW_H__35ECF3F0_199A_46F7_B326_C783AC14303A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CCMtablica;

class CCKORView : public CView
{
protected: // create from serialization only
	CCKORView();
	DECLARE_DYNCREATE(CCKORView)

// Attributes
public:
	CCKORDoc* GetDocument();
	CCMtablica*m_dlgINFO;//klasa dijaloga Info Tabla b

private:
CPoint pXY;
// Operations
public:
	

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCKORView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CCKORView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	int m_Zumx;
	int m_Zumy;
	double alfa;
	double beta;
	double PTX;
	double PTY;
	double  m_DKR;
	double	m_alfa;
	double	m_beta;
	double	m_DDI;
	double	m_DKO;
	double	m_RKR;

	double m_Rsp;
	double m_Run;
	double m_Lduz;
	double m_ROg;
	double m_Umasa;
	double m_CenapoKg;
	double m_CenaUkupno;
	double m_naponZ;
	double m_Pmax;


    double m_rsp ;
	double m_run ;
	double m_debljina;
	double m_odnosr;

	double m_ntri;
	double m_nrri;
	double m_nlri;
	double m_pPu;

	double m_ena;

	double m_srednjinn;
	double m_taumax;
	double m_Zatezna;

	double m_zateznaT;
	int m_kvalitetS;
	double m_PuD;
	double m_Dun;
	double m_Dun2;
	double m_Dsp1;
	double m_Dsp2;
	double m_PuD2;
	double m_DD1;
	double m_DD2;
	double m_DP1;
	double m_DP2;
	double m_PP1;
	double m_PP2;

	double	m_rsp2;
	double	m_run2;
	double	m_debljina2;
	double	m_pritisakPi;
	double	m_hoop;
	double	m_long;
	double	m_radi;
	double	m_zatezna2;
	double	m_ena2;
	double	m_odnosDsR;
	double	m_gnn2;
	double	m_taumax2;

	double	m_dex;
	double	m_dey;
	double	m_dez;
	double	m_zapreminaVo;
	double	m_dVo;
	double	m_radnaT;
	double	m_defT;

	double	m_poason;
	double	m_modulE;

	double t_radi;
	double t_long;
	double t_hoop;
	double t_poason;
	double t_modulE;

	double	m_Po;
	double	m_Pi;
	double	m_kapa;
	double	m_Pamb;
	double	m_To;
	double	m_Rgg;















	//{{AFX_MSG(CCKORView)
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnPar();
	afx_msg void OnMasacena();
	afx_msg void OnAutor();
	afx_msg void OnMtablica();
	afx_msg void On1napon();
	afx_msg void On3napon();
	afx_msg void OnTankozidne();
	afx_msg void OnTankozid();
	afx_msg void OnRmo();
	afx_msg void OnTest5();
	afx_msg void OnTestSave();
	afx_msg void OnTestOpen();
	afx_msg void OnExe1();
	afx_msg void OnKt();
	afx_msg void OnPolaris();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in cKORView.cpp
inline CCKORDoc* CCKORView::GetDocument()
   { return (CCKORDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CKORVIEW_H__35ECF3F0_199A_46F7_B326_C783AC14303A__INCLUDED_)
