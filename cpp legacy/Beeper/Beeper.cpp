// This is a simple filter application.  It will spawn
// the application on  command line. But before spawning
// the application, it will create a pipe that will direct the
// spawned application's stdout to the filter.  The filter
// will remove ASCII 7 (beep) characters.

// Beeper.Cpp

/* Compile options needed: None */
#include <stdio.h>
#include <string.h>

int main()
{
   int   i;
   
   for(i=0;i<100;++i)
   {     
         printf("\nThis is speaker beep number %d... \n\7", i+1);
      }
   return 0;
}