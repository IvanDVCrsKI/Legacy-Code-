// Pirol.h : main header file for the PIROL application
//

#if !defined(AFX_PIROL_H__93FBB138_03FD_46A8_A3EA_0D47B95626A1__INCLUDED_)
#define AFX_PIROL_H__93FBB138_03FD_46A8_A3EA_0D47B95626A1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CPirolApp:
// See Pirol.cpp for the implementation of this class
//

class CPirolApp : public CWinApp
{
public:
	CPirolApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPirolApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CPirolApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PIROL_H__93FBB138_03FD_46A8_A3EA_0D47B95626A1__INCLUDED_)
