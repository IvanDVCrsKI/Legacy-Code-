#include "stdafx.h"

#include <windows.h>
#include <stdio.h>
#include <io.h>
#include <conio.h>
#include <stdlib.h>
#include <commctrl.h>

#define maxBytes 256//128  //256 5 sekundi //interesantno za 8   //procitaj char do 256
#define MAX 512//115200//4096//2048//1024//utice znatno na brzinu npr 8  //max broj kolona u fajlu da procita
          
int main()
{
	HANDLE hSerial;
	DCB dcb;
	COMMTIMEOUTS timeouts;
	DWORD dwBytesWrite = 0;
	int i;
	char szBuff[maxBytes];
	char stemp[MAX];
	FILE *fp;


	//opening the serial port
	hSerial = CreateFile(
		"COM2",// Pointer to the name of the port
		GENERIC_WRITE,//GENERIC_READ | GENERIC_WRITE,  // Access (read/write) mode
		0,           // Share mode
		NULL,// Pointer to the security attribute
		OPEN_EXISTING,   // How to open the serial port
	   	0,//FILE_ATTRIBUTE_NORMAL,
		NULL); //0

	if(hSerial==INVALID_HANDLE_VALUE)
	{
		if(GetLastError()==ERROR_FILE_NOT_FOUND)
		{
			printf("Serial port does not exist\n");
		}
		printf("Other errors\n");
	}

	//setting parameters
	dcb.DCBlength = sizeof (dcb);

	//GetCommState is to retrieves the current control settings for a specific communications device.
	if (!GetCommState(hSerial, &dcb))
	{
		printf("Not GetCommState, not able to retrieves the current control.\n");
	}
   
/*
iz MTTTY programa
char * szBaud[] = {
	    "110", "300", "600", "1200", "2400", 
	    "4800", "9600", "14400", "19200",
	    "38400", "56000", "57600", "115200","120000", 
	    "128000","230400","240000","300000","921600","1000000", "2457600", "3000000","4000000","6000000"
	};

DWORD   BaudTable[] =  {
	    CBR_110, CBR_300, CBR_600, CBR_1200, CBR_2400,
	    CBR_4800, CBR_9600, CBR_14400, CBR_19200, CBR_38400,
	    CBR_56000, CBR_57600, CBR_115200,120000, CBR_128000,230400,240000,300000, 921600,1000000,2457600,3000000, 4000000,6000000
	} ;


 //pri brzini od 6.000.000 braud slanje je trajalo 48 sekundi COMMANDERU2 bez tredova  //brzia je oko 153000 baud
  //pri brzini od 6.000.000 braud slanje je trajalo 7 sekundi u MTTTY sa tredovima 

*/	
	
	
	dcb.BaudRate =4000000;//4000000;  //2457600 9s za 2000 redova;//CBR_128000;//CBR_256000;//6000000;//CBR_9600;//CBR_256000;//CBR_115200;
	dcb.ByteSize = 8;
	dcb.StopBits = ONESTOPBIT;
	dcb.Parity = NOPARITY;
	dcb.fBinary = TRUE;// Binary mode; no EOF check
	dcb.fParity = TRUE; // Enable parity checking
    dcb.fDtrControl     = DTR_CONTROL_ENABLE;// DTR flow control type
    dcb.fRtsControl     = RTS_CONTROL_ENABLE;// RTS flow control
    dcb.fOutxCtsFlow    = FALSE;// No CTS output flow control
    dcb.fOutxDsrFlow    = FALSE;// No DSR output flow control
    dcb.fDsrSensitivity = FALSE;// DSR sensitivity
    dcb.fOutX           = FALSE;//No XON/XOFF out flow control
    dcb.fInX            = FALSE;//No XON/XOFF in flow control
    dcb.fTXContinueOnXoff = TRUE;// XOFF continues Tx
	dcb.fNull = FALSE; // Disable null stripping.
	dcb.fErrorChar = FALSE; // Disable error replacement.
    dcb.fAbortOnError = FALSE; // Do not abort reads/writes on error
  

	//SetCommState configures a communications device according to the specifications
	//in DCB. The function reinitializes all hardware control settings but it does not
	//empty output or input queues
	if (!SetCommState(hSerial, &dcb))
	{
		printf("Not SetCommState, cannot configures serial port according to DCB specifications set.\n");
	}
    GetCommTimeouts(hSerial, &timeouts);
	//setting timeouts
	timeouts.ReadIntervalTimeout = MAXDWORD;
	timeouts.ReadTotalTimeoutConstant = 0;
	timeouts.ReadTotalTimeoutMultiplier = 0;
	timeouts.WriteTotalTimeoutConstant = 10;
	timeouts.WriteTotalTimeoutMultiplier = 1000;

	//SetCommTimeouts set the time out parameters for all reand and write operation
	if (!SetCommTimeouts(hSerial, &timeouts))
	{
		printf("Not SetCommTimeouts, cannot set the timeout parameters to serial port.\n");
	//	return EC_TIMEOUT_SET;
	}

	//Writting data
	//WriteFile write data from the specified file or i/o devices.
	printf("The content inside the file: \n\n");
	fp = fopen("c:\\1.txt", "r");

OVERLAPPED osWrite = {0};
osWrite.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
//if (!WriteFile(hFile, lpBuf, dwBufLen, &dwWritten, NULL))
	while ((fgets(stemp, MAX, fp)) != NULL)
	{
		i = sprintf(szBuff, "%s", stemp);
		if (!WriteFile(
			hSerial, // Port handle  
			szBuff,                                                  // u mtty unsigned long 
			maxBytes,  // Number of bytes to write                   //mttty char *lpbuf
			&dwBytesWrite, // Pointer to the number of bytes written // u mtty unsigned long 
			&osWrite))// Must be NULL for Windows CE                     //OVERLAPPED osWrite = {0}; tj &osWrite
		{


  
			printf("Serial port cannot write file.\n");
		}
		else
		{
			//bez printf 45 sekundi pri 6.000.000 braud

				printf("%s", szBuff);
				LocalFree(szBuff);
		}
	}
	fclose(fp);
	CloseHandle(hSerial);
	 CloseHandle(osWrite.hEvent);
}

/*
	while ((fgets(stemp, MAX, fp)) != NULL)
	{
		i = sprintf(szBuff, "%s", stemp);
		if (!WriteFile(
			hSerial, // Port handle  
			szBuff, 
			maxBytes,  // Number of bytes to write 
			&dwBytesWrite, // Pointer to the number of bytes written
			NULL))// Must be NULL for Windows CE
		{


  
			printf("Serial port cannot write file.\n");
		}
		else
		{
			//bez printf 45 sekundi pri 6.000.000 braud

				printf("%s", szBuff);
				LocalFree(szBuff);
		}
	}
--------------------------------------------------------------------------------------------
oid WriterGeneric(char * lpBuf, DWORD dwToWrite)
{
    OVERLAPPED osWrite = {0};
    HANDLE hArray[2];
    DWORD dwWritten;
    DWORD dwRes;

    //
    // If no writing is allowed, then just return
    //
    if (NOWRITING(TTYInfo))
        return ;

    //
    // create this writes overlapped structure hEvent
    //
    osWrite.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (osWrite.hEvent == NULL)
        ErrorInComm("CreateEvent (overlapped write hEvent)");

    hArray[0] = osWrite.hEvent;
    hArray[1] = ghThreadExitEvent;
    
    //
    // issue write
    //
    if (!WriteFile(
	COMDEV(TTYInfo),  // Port handle  
	lpBuf,
	dwToWrite, // Number of bytes to write 
	&dwWritten, // Pointer to the number of bytes written
	&osWrite)) \// Must be NULL for Windows CE
	{
        if (GetLastError() == ERROR_IO_PENDING) { 
            //
            // write is delayed
            //
            dwRes = WaitForMultipleObjects(2, hArray, FALSE, INFINITE);
            switch(dwRes)
            {
                //
                // write event set
                //
                case WAIT_OBJECT_0:
                            SetLastError(ERROR_SUCCESS);
                            if (!GetOverlappedResult(COMDEV(TTYInfo), &osWrite, &dwWritten, FALSE))
							{
                                if (GetLastError() == ERROR_OPERATION_ABORTED)
                                    UpdateStatus("Write aborted\r\n");
                                else
                                    ErrorInComm("GetOverlappedResult(in Writer)");
                            }
                            
                            if (dwWritten != dwToWrite) {
                                if ((GetLastError() == ERROR_SUCCESS) && SHOWTIMEOUTS(TTYInfo))
                                    UpdateStatus("Write timed out. (overlapped)\r\n");
                                else
                                    ErrorReporter("Error writing data to port (overlapped)");
                            }
                            break;

                //
                // thread exit event set
                //
                case WAIT_OBJECT_0 + 1:
                            break;

                //                
                // wait timed out
                //
                case WAIT_TIMEOUT:
                            UpdateStatus("Wait Timeout in WriterGeneric.\r\n");
                            break;

                case WAIT_FAILED:
                default:    ErrorInComm("WaitForMultipleObjects (WriterGeneric)");
                            break;
            }
        }
        else    
            //
            // writefile failed, but it isn't delayed
            //
            ErrorInComm("WriteFile (in Writer)");
    }
    else {
        //
        // writefile returned immediately
        //
        if (dwWritten != dwToWrite)
            UpdateStatus("Write timed out. (immediate)\r\n");
    }

    CloseHandle(osWrite.hEvent);
    
    return;
}






















	*/