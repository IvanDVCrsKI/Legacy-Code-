// COM_test_alfaView.h : interface of the CCOM_test_alfaView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_COM_TEST_ALFAVIEW_H__47022BBB_7C33_4970_84CE_48EBA0F3C26A__INCLUDED_)
#define AFX_COM_TEST_ALFAVIEW_H__47022BBB_7C33_4970_84CE_48EBA0F3C26A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CCOM_test_alfaView : public CView
{
protected: // create from serialization only
	CCOM_test_alfaView();
	DECLARE_DYNCREATE(CCOM_test_alfaView)

// Attributes
public:
	CCOM_test_alfaDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCOM_test_alfaView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CCOM_test_alfaView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
public:

	int m_data;
	CString m_status;
protected:
	//{{AFX_MSG(CCOM_test_alfaView)
	afx_msg void OnComPort();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in COM_test_alfaView.cpp
inline CCOM_test_alfaDoc* CCOM_test_alfaView::GetDocument()
   { return (CCOM_test_alfaDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COM_TEST_ALFAVIEW_H__47022BBB_7C33_4970_84CE_48EBA0F3C26A__INCLUDED_)
