//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by KreatorTrajektorija.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_KREATOTYPE                  129
#define IDD_AUTORDV                     130
#define IDB_Autor                       131
#define IDD_KORAK                       132
#define IDD_PARAMETRI                   133
#define IDD_MREZA                       134
#define IDD_DGRAFIK                     135
#define IDD_ITABLA                      136
#define IDD_ZUM                         137
#define IDD_UPUTSTVO                    138
#define IDD_ATMOSFERA                   139
#define IDB_BITMAP1                     140
#define IDD_FODT                        141
#define IDC_KORAK                       1000
#define IDC_LOOP                        1001
#define IDC_BROJTACAKA                  1002
#define IDC_IZRACUNAJ                   1003
#define IDC_MF1_DA                      1005
#define IDC_MF1_NE                      1006
#define IDC_MF2_DA                      1007
#define IDC_MF2_NE                      1008
#define IDC_MF3_DA                      1009
#define IDC_MF3_NE                      1010
#define IDC_UGAO_F1                     1011
#define IDC_BRZINA_F1                   1012
#define IDC_MREZA_DA                    1014
#define IDC_MREZA_NE                    1015
#define IDC_GMREZA                      1016
#define IDC_UGAO_F2                     1017
#define IDC_DGRAF_DA                    1017
#define IDC_BRZINA_F2                   1018
#define IDC_DGRAF_NE                    1018
#define IDC_UGAO_F3                     1019
#define IDC_TABLA_DA                    1019
#define IDC_BRZINA_F3                   1020
#define IDC_TABLA_NE                    1020
#define IDC_PI                          1021
#define IDC_ZUM                         1021
#define IDC_TROPOS                      1022
#define IDC_GRAV                        1023
#define IDC_ZUMY                        1023
#define IDC_TROPOP                      1023
#define IDC_STRATOSFERA                 1024
#define IDC_MEZOSFERA                   1025
#define IDC_PTO                         1026
#define IDC_PADT                        1027
#define IDC_PRIKAZGA_DA                 1028
#define IDC_PRIKAZGA_NE                 1029
#define IDC_TEKSTA_DA                   1030
#define IDC_TEMP                        1030
#define IDC_TEKSTA_NE                   1031
#define IDC_TEMP_DA                     1032
#define IDC_TEMP_NE                     1033
#define IDC_TAB1                        1034
#define IDM_ZUM                         32771
#define IDM_AUTOR                       32772
#define IDM_UPUTSTVO                    32773
#define IDM_PARAMETRI                   32774
#define IDM_KORAK                       32775
#define IDM_MREZA                       32776
#define IDM_TABLA                       32777
#define IDM_DGRAFIK                     32778
#define IDM_ATMOSFERA                   32779
#define IDM_FODT                        32780

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        142
#define _APS_NEXT_COMMAND_VALUE         32781
#define _APS_NEXT_CONTROL_VALUE         1035
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
