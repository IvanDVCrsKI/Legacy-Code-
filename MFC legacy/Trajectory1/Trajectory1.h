// Trajectory1.h : main header file for the TRAJECTORY1 application
//

#if !defined(AFX_TRAJECTORY1_H__7BED1361_BA88_4BAB_853A_4A4266D52369__INCLUDED_)
#define AFX_TRAJECTORY1_H__7BED1361_BA88_4BAB_853A_4A4266D52369__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CTrajectory1App:
// See Trajectory1.cpp for the implementation of this class
//

class CTrajectory1App : public CWinApp
{
public:
	CTrajectory1App();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTrajectory1App)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CTrajectory1App)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TRAJECTORY1_H__7BED1361_BA88_4BAB_853A_4A4266D52369__INCLUDED_)
