// MFC_SAVE_PROCEDURADoc.h : interface of the CMFC_SAVE_PROCEDURADoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MFC_SAVE_PROCEDURADOC_H__8D4B5333_763F_41F0_8ACB_21A9D753A81C__INCLUDED_)
#define AFX_MFC_SAVE_PROCEDURADOC_H__8D4B5333_763F_41F0_8ACB_21A9D753A81C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMFC_SAVE_PROCEDURADoc : public CDocument
{
protected: // create from serialization only
	CMFC_SAVE_PROCEDURADoc();
	DECLARE_DYNCREATE(CMFC_SAVE_PROCEDURADoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMFC_SAVE_PROCEDURADoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMFC_SAVE_PROCEDURADoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMFC_SAVE_PROCEDURADoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFC_SAVE_PROCEDURADOC_H__8D4B5333_763F_41F0_8ACB_21A9D753A81C__INCLUDED_)
