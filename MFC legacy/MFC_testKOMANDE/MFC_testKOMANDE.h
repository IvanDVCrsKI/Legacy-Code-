// MFC_testKOMANDE.h : main header file for the MFC_TESTKOMANDE application
//

#if !defined(AFX_MFC_TESTKOMANDE_H__263DC74B_F713_4CF5_A6FD_C1BE828AC685__INCLUDED_)
#define AFX_MFC_TESTKOMANDE_H__263DC74B_F713_4CF5_A6FD_C1BE828AC685__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CMFC_testKOMANDEApp:
// See MFC_testKOMANDE.cpp for the implementation of this class
//

class CMFC_testKOMANDEApp : public CWinApp
{
public:
	CMFC_testKOMANDEApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMFC_testKOMANDEApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CMFC_testKOMANDEApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFC_TESTKOMANDE_H__263DC74B_F713_4CF5_A6FD_C1BE828AC685__INCLUDED_)
