//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Pirol.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_PIROLTYPE                   129
#define IDD_DIALOG1                     130
#define IDC_BUTTON_ADD                  1000
#define IDC_ADD1                        1001
#define IDC_ZNAK                        1002
#define IDC_ADD2                        1003
#define IDC_SUM                         1004
#define IDC_HISTORY                     1006
#define IDC_CMD                         1007
#define IDM_KALKULATOR                  32771

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32772
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
