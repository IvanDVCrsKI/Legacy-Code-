// CirclesDoc.cpp : implementation of the CCirclesDoc class
//

#include "stdafx.h"


#include "CirclesDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCirclesDoc

IMPLEMENT_DYNCREATE(CCirclesDoc, CDocument)

BEGIN_MESSAGE_MAP(CCirclesDoc, CDocument)
	//{{AFX_MSG_MAP(CCirclesDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCirclesDoc construction/destruction

CCirclesDoc::CCirclesDoc()
{
	// TODO: add one-time construction code here

}

CCirclesDoc::~CCirclesDoc()
{
}

BOOL CCirclesDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CCirclesDoc serialization

void CCirclesDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CCirclesDoc diagnostics

#ifdef _DEBUG
void CCirclesDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CCirclesDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCirclesDoc commands
