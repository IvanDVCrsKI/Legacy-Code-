#include <fstream>
#include <iostream>
#include <iomanip>
using std::cout;
using std::endl;


/*

// read a file into memory
#include <iostream>
#include <fstream>
using namespace std;

int main () {
  int length;
  char * buffer;

  ifstream is;
  is.open ("test.txt", ios::binary );

  // get length of file:
  is.seekg (0, ios::end);
  length = is.tellg();
  is.seekg (0, ios::beg);

  // allocate memory:
  buffer = new char [length];

  // read data as a block:
  is.read (buffer,length);
  is.close();

  cout.write (buffer,length);

  delete[] buffer;
  return 0;
}
*/
//

int main() {
  const char* filename = "DL2_data.txt";
  std::ifstream inFile(filename);

  // Make sure the file stream is good
  if(!inFile) {
    cout << endl << "Failed to open file " << filename <<"\n" ;
    return 1;
  }

  double x = 0;
  double y= 0;
  while(!inFile.eof()) {
    inFile >> x >>y;
    cout  << x<<"   "<<y<<endl;
  }
  cout << endl;
  return 0;
}
//*/