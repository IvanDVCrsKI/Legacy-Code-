
#ifdef _WIN32
#include <windows.h>//requires that windows.h be included before either gl.h or glu.h, because some macros
#endif
#include <glut.h>
#include <stdlib.h> //Needed for "exit" function


///////////////////////////////////////////////////////////
// Called to draw scene
void RenderScene(void)
	{
	// Clear the window with current clearing color
	glClear(GL_COLOR_BUFFER_BIT);//Clearing the Color Buffer


	// Flush drawing commands
    glFlush();//This line causes any unexecuted OpenGL commands to be executed
	}

///////////////////////////////////////////////////////////
// Setup the rendering state
void SetupRC(void)
    {
    glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
//colorref (255,255,255)
//256�256�256 or more than 16 million colors


	//With OpenGL, the values for each component can be 
	//any valid floating-point value between 0 and 1
    //color output is limited on most devices to 24 bits (16 million colors) total

/*	
Black             0.0 0.0 0.0
Red               1.0 0.0 0.0
Green             0.0 1.0 0.0
Yellow            1.0 1.0 0.0
Blue              0.0 0.0 1.0
Magenta           1.0 0.0 1.0
Cyan              0.0 1.0 1.0
Dark gray         0.25 0.25 0.25
Light gray        0.75 0.75 0.75
Brown             0.60 0.40 0.12
Pumpkin orange    0.98 0.625 0.12
Pastel pink       0.98 0.04 0.7
Barney purple     0.60 0.40 0.70
White             1.0 1.0 1.0
*/

    }

///////////////////////////////////////////////////////////
// Main program entry point
int main(int argc, char* argv[])
	{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);//single-buffered window means that all drawing commands are 
	                                             //performed on the window displayed
   	                                             //
	                                            //double-buffered window, where the drawing commands are actually
	                                             //executed on an offscreen buffer and then quickly
                                                 //swapped into view on the window
	
	glutCreateWindow("Simple");//Creating the OpenGL Window
	glutDisplayFunc(RenderScene);//Displaying pointing to RenderScene function

	SetupRC();//OpenGL states need to be set only once and do not need to be reset every time you render a frame

	glutMainLoop();//This function processes all the operating system�specific messages,keystrokes, and so on 
	               //until you terminate the program
    
    return 0;
    }

