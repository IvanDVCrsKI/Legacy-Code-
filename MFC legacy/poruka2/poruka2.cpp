// poruka2.cpp : Defines the entry point for the console application.
//
/**/

#include "stdafx.h"
#include <iostream>
#include <string>
using namespace std;

int main()
{

const int br = 126;

for (int c = 0; c<br; c++)
{

cout<<c<<"    "<<hex<<showbase<<c<<endl;

}


return 0;
}
/*
strcat(stringVar, znak);//spoji predhodni stringVar sa znak // sto daje Izlaz ""add1znak 

char ch2[20];
sprintf(ch2,"%2f",add2);//pretvori double add2 u string ch2
strcat(stringVar, ch2);//spoji predhodni stringVar sa ch2 // sto daje Izlaz ""add1znakadd2 

char jednakoa[]="=";
strcat(stringVar, jednakoa);//spoji predhodni stringVar sa "=" // sto daje Izlaz ""add1znakadd2=

char ch3[40];
sprintf(ch3,"%2f",sum);//pretvori double sum u string ch3
strcat(stringVar, ch3);//spoji predhodni stringVar sa ch3 // sto daje Izlaz ""add1znakadd2=sum 
*/