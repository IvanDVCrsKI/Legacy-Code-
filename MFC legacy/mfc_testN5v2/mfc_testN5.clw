; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CParametriDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "mfc_testN5.h"
LastPage=0

ClassCount=12
Class1=CMfc_testN5App
Class2=CMfc_testN5Doc
Class3=CMfc_testN5View
Class4=CMainFrame

ResourceCount=5
Resource1=IDD_KOORDINATE_DLG
Class5=CAboutDlg
Class6=CCostumDlg
Class7=OnLButtonDown
Class8=CCDialog
Resource2=IDD_CUSTOM_DIALOG
Class9=CCustomDlg
Resource3=IDD_ABOUTBOX
Class10=CPodaci
Class11=CParametriDlg
Resource4=IDR_MAINFRAME
Class12=CAutor
Resource5=IDD_AUTOR

[CLS:CMfc_testN5App]
Type=0
HeaderFile=mfc_testN5.h
ImplementationFile=mfc_testN5.cpp
Filter=N
LastObject=IDM_KOY22

[CLS:CMfc_testN5Doc]
Type=0
HeaderFile=mfc_testN5Doc.h
ImplementationFile=mfc_testN5Doc.cpp
Filter=N

[CLS:CMfc_testN5View]
Type=0
HeaderFile=mfc_testN5View.h
ImplementationFile=mfc_testN5View.cpp
Filter=D
BaseClass=CView
VirtualFilter=VWC
LastObject=CMfc_testN5View


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T
LastObject=IDM_AUTOR
BaseClass=CFrameWnd
VirtualFilter=fWC




[CLS:CAboutDlg]
Type=0
HeaderFile=mfc_testN5.cpp
ImplementationFile=mfc_testN5.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_SAVE_AS
Command5=ID_FILE_PRINT
Command6=ID_FILE_PRINT_PREVIEW
Command7=ID_FILE_PRINT_SETUP
Command8=ID_FILE_SEND_MAIL
Command9=ID_FILE_MRU_FILE1
Command10=ID_APP_EXIT
Command11=ID_VIEW_TOOLBAR
Command12=ID_VIEW_STATUS_BAR
Command13=IDM_KOY22
Command14=IDM_AUTOR
Command15=IDM_POMOCNIDLG
CommandCount=15

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_PRINT
Command5=ID_EDIT_UNDO
Command6=ID_EDIT_CUT
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_UNDO
Command10=ID_EDIT_CUT
Command11=ID_EDIT_COPY
Command12=ID_EDIT_PASTE
Command13=ID_NEXT_PANE
Command14=ID_PREV_PANE
CommandCount=14

[TB:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_CUT
Command5=ID_EDIT_COPY
Command6=ID_EDIT_PASTE
Command7=ID_FILE_PRINT
Command8=ID_APP_ABOUT
Command9=ID_APP_KOO
CommandCount=9

[CLS:OnLButtonDown]
Type=0
HeaderFile=OnLButtonDown.h
ImplementationFile=OnLButtonDown.cpp
BaseClass=CView
Filter=C

[CLS:CCostumDlg]
Type=0
HeaderFile=CostumDlg.h
ImplementationFile=CostumDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=CCostumDlg

[DLG:IDD_CUSTOM_DIALOG]
Type=1
Class=CCustomDlg
ControlCount=4
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,button,1342177287
Control4=IDC_EDIT1,edit,1350631552

[CLS:CCustomDlg]
Type=0
HeaderFile=CustomDlg.h
ImplementationFile=CustomDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=CCustomDlg

[CLS:CPodaci]
Type=0
HeaderFile=Podaci.h
ImplementationFile=Podaci.cpp
BaseClass=CDialog
Filter=D
LastObject=IDM_KOYY
VirtualFilter=dWC

[DLG:IDD_KOORDINATE_DLG]
Type=1
Class=CParametriDlg
ControlCount=6
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_EDIT1,edit,1350631552
Control4=IDC_EDIT2,edit,1350631552
Control5=IDC_STATIC,static,1342308352
Control6=IDC_STATIC,static,1342308352

[CLS:CParametriDlg]
Type=0
HeaderFile=ParametriDlg.h
ImplementationFile=ParametriDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=ID_APP_ABOUT

[DLG:IDD_AUTOR]
Type=1
Class=CAutor
ControlCount=9
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342308352
Control5=IDC_STATIC,static,1342308352
Control6=IDC_STATIC,static,1342177294
Control7=IDC_STATIC,static,1342308352
Control8=IDC_STATIC,static,1342308352
Control9=IDC_STATIC,button,1342177287

[CLS:CAutor]
Type=0
HeaderFile=Autor.h
ImplementationFile=Autor.cpp
BaseClass=CDialog
Filter=D
LastObject=CAutor

