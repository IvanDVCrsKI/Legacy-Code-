#if !defined(AFX_MREZA_H__23B9BD0D_8393_4A9E_9836_5E9793E23E24__INCLUDED_)
#define AFX_MREZA_H__23B9BD0D_8393_4A9E_9836_5E9793E23E24__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Mreza.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMreza dialog

class CMreza : public CDialog
{
// Construction
public:
	CMreza(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CMreza)
	enum { IDD = IDD_MREZA };
	int		m_Mreza;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMreza)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMreza)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MREZA_H__23B9BD0D_8393_4A9E_9836_5E9793E23E24__INCLUDED_)
