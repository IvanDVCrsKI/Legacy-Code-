// MFC_TESTDVDDoc.cpp : implementation of the CMFC_TESTDVDDoc class
//

#include "stdafx.h"
#include "MFC_TESTDVD.h"

#include "MFC_TESTDVDDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMFC_TESTDVDDoc

IMPLEMENT_DYNCREATE(CMFC_TESTDVDDoc, CDocument)

BEGIN_MESSAGE_MAP(CMFC_TESTDVDDoc, CDocument)
	//{{AFX_MSG_MAP(CMFC_TESTDVDDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMFC_TESTDVDDoc construction/destruction

CMFC_TESTDVDDoc::CMFC_TESTDVDDoc()
{
	// TODO: add one-time construction code here

}

CMFC_TESTDVDDoc::~CMFC_TESTDVDDoc()
{
}

BOOL CMFC_TESTDVDDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CMFC_TESTDVDDoc serialization

void CMFC_TESTDVDDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMFC_TESTDVDDoc diagnostics

#ifdef _DEBUG
void CMFC_TESTDVDDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMFC_TESTDVDDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMFC_TESTDVDDoc commands
