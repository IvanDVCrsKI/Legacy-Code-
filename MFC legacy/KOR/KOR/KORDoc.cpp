// KORDoc.cpp : implementation of the CKORDoc class
//

#include "stdafx.h"
#include "KOR.h"

#include "KORDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CKORDoc

IMPLEMENT_DYNCREATE(CKORDoc, CDocument)

BEGIN_MESSAGE_MAP(CKORDoc, CDocument)
	//{{AFX_MSG_MAP(CKORDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CKORDoc construction/destruction

CKORDoc::CKORDoc()
{
	// TODO: add one-time construction code here

}

CKORDoc::~CKORDoc()
{
}

BOOL CKORDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CKORDoc serialization

void CKORDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CKORDoc diagnostics

#ifdef _DEBUG
void CKORDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CKORDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CKORDoc commands
