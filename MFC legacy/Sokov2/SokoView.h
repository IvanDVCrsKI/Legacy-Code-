// SokoView.h : interface of the CSokoView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SOKOVIEW_H__0ABAA787_9758_4495_AEBA_316F20BE4B9D__INCLUDED_)
#define AFX_SOKOVIEW_H__0ABAA787_9758_4495_AEBA_316F20BE4B9D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CSokoView : public CScrollView
{
protected: // create from serialization only
	CSokoView();
	DECLARE_DYNCREATE(CSokoView)

// Attributes
public:
	CSokoDoc* GetDocument();



  protected:
      CPoint m_FirstPoint;      // First point recorded for an element
      CPoint m_SecondPoint;     // Second point recorded for an
      CElement* m_pTempElement;    // Pointer to temporary element
      CElement* m_pSelected;       // Currently selected element

// Operations
public:
CDC m_MemDC;
CBitmap m_bmpView;
int m_nBmpWidth,m_nBmpHeight;
	int m_Zumx;
	int m_Zumy;
	int m_Orgfx;
	int m_Orgfy;
	int red;
	int green;
	int blue;
int kor;

    
	CPoint point;

protected:
   CElement* CreateElement(); // Create a new element on the heap
  CElement* SelectElement(CPoint aPoint);         // Select an element

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSokoView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void OnInitialUpdate(); // called first time after construct
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSokoView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions

	//{{AFX_MSG(CSokoView)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in SokoView.cpp
inline CSokoDoc* CSokoView::GetDocument()
   { return (CSokoDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SOKOVIEW_H__0ABAA787_9758_4495_AEBA_316F20BE4B9D__INCLUDED_)
