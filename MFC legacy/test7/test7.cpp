// test7.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <iostream>
using namespace std;

int main()
{
	// A 2-Dimensional array
	double distance[][4] = {
				{ 44.14, 720.52,  96.08,  468.78 },
				{  6.28,  68.04, 364.55, 6234.12 }
			     };

	// Scan the array from the 3rd to the 7th member
	cout << "Members of the array";
	for(int i = 0; i < 2; ++i)
		for(int j = 0; j < 4; ++j)
			cout << "\nDistance [" << i << "][" << j << "]: " << distance[i][j];

	cout << endl;
	return 0;
}