#if !defined(AFX_AUTOR_H__23307D7C_24AA_4ECF_B21C_8601DD601844__INCLUDED_)
#define AFX_AUTOR_H__23307D7C_24AA_4ECF_B21C_8601DD601844__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Autor.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAutor dialog

class CAutor : public CDialog
{
// Construction
public:
	CAutor(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CAutor)
	enum { IDD = IDD_AUTOR };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAutor)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAutor)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AUTOR_H__23307D7C_24AA_4ECF_B21C_8601DD601844__INCLUDED_)
