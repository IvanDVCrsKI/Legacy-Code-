// MFC_ZOOM_TESTDoc.h : interface of the CMFC_ZOOM_TESTDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MFC_ZOOM_TESTDOC_H__9451A256_A522_4BD9_8738_FA6219F8DC5A__INCLUDED_)
#define AFX_MFC_ZOOM_TESTDOC_H__9451A256_A522_4BD9_8738_FA6219F8DC5A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMFC_ZOOM_TESTDoc : public CDocument
{
protected: // create from serialization only
	CMFC_ZOOM_TESTDoc();
	DECLARE_DYNCREATE(CMFC_ZOOM_TESTDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMFC_ZOOM_TESTDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMFC_ZOOM_TESTDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMFC_ZOOM_TESTDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFC_ZOOM_TESTDOC_H__9451A256_A522_4BD9_8738_FA6219F8DC5A__INCLUDED_)
