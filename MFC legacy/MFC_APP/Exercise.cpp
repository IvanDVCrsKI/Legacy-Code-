#include <afxwin.h>
#include "resource.h"

class CResFrame : public CFrameWnd
{
public:
	CResFrame()
	{
	const char *RWC = AfxRegisterWndClass(NULL, NULL,
                                               (HBRUSH)::GetStockObject(WHITE_BRUSH),
                                               NULL);
			Create(RWC, "Osnove Resursa Win32", WS_OVERLAPPEDWINDOW,
			   CRect(200, 120, 640, 400), NULL,
			   MAKEINTRESOURCE(IDR_MENU1));
	}
};

class CResApp: public CWinApp
{
public:
	BOOL InitInstance()
	{
		m_pMainWnd = new CResFrame;
		m_pMainWnd->ShowWindow(SW_SHOW);
		m_pMainWnd->UpdateWindow();

		return TRUE;
	}
};


CResApp theApp;