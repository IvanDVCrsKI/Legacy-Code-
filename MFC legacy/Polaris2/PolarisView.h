// PolarisView.h : interface of the CPolarisView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_POLARISVIEW_H__81E9616E_999A_4716_A35E_8C6533ECC569__INCLUDED_)
#define AFX_POLARISVIEW_H__81E9616E_999A_4716_A35E_8C6533ECC569__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CPolarisView : public CView
{
protected: // create from serialization only
	CPolarisView();
	DECLARE_DYNCREATE(CPolarisView)

// Attributes
public:
	CPolarisDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPolarisView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CPolarisView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	int m_Zumx;
	int m_Zumy;
	int alfa1;

	double m_Akx;
	double m_Duz2;
		double m_MasaR;
		double m_MasaT;
		double m_Valfa;
		double m_Aky;
		
	//{{AFX_MSG(CPolarisView)
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnParametri();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in PolarisView.cpp
inline CPolarisDoc* CPolarisView::GetDocument()
   { return (CPolarisDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_POLARISVIEW_H__81E9616E_999A_4716_A35E_8C6533ECC569__INCLUDED_)
