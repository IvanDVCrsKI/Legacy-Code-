#if !defined(AFX_CMTABLICA_H__5B3DBF5E_DE95_46E6_BC1B_A64FD37CB5BD__INCLUDED_)
#define AFX_CMTABLICA_H__5B3DBF5E_DE95_46E6_BC1B_A64FD37CB5BD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CMtablica.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCMtablica dialog

class CCMtablica : public CDialog
{
  
	CView* m_dlgINFO;
// Construction
public:
	CCMtablica(CWnd* pParent = NULL);   // standard constructor
    CCMtablica(CView* m_dlgINFO);    
    BOOL Create();

// Dialog Data
	//{{AFX_DATA(CCMtablica)
	enum { IDD = IDD_MTABLICA };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCMtablica)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCMtablica)
	virtual void OnCancel();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CMTABLICA_H__5B3DBF5E_DE95_46E6_BC1B_A64FD37CB5BD__INCLUDED_)
