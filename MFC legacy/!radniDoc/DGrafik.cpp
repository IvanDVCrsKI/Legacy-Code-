// DGrafik.cpp : implementation file
//

#include "stdafx.h"
#include "KreatorTrajektorijaDV.h"
#include "DGrafik.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDGrafik dialog


CDGrafik::CDGrafik(CWnd* pParent /*=NULL*/)
	: CDialog(CDGrafik::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDGrafik)
	m_DGrafik = -1;
	//}}AFX_DATA_INIT
}


void CDGrafik::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDGrafik)
	DDX_Radio(pDX, IDC_DGRAF_DA, m_DGrafik);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDGrafik, CDialog)
	//{{AFX_MSG_MAP(CDGrafik)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDGrafik message handlers
