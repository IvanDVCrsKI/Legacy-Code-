// cKORView.cpp : implementation of the CCKORView class
//

#include "stdafx.h"
#include "cKOR.h"

#include "cKORDoc.h"
#include "cKORView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCKORView

IMPLEMENT_DYNCREATE(CCKORView, CView)

BEGIN_MESSAGE_MAP(CCKORView, CView)
	//{{AFX_MSG_MAP(CCKORView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCKORView construction/destruction

CCKORView::CCKORView()
{
	// TODO: add construction code here

}

CCKORView::~CCKORView()
{
}

BOOL CCKORView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CCKORView drawing

void CCKORView::OnDraw(CDC* pDC)
{
	CCKORDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CCKORView printing

BOOL CCKORView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CCKORView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CCKORView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CCKORView diagnostics

#ifdef _DEBUG
void CCKORView::AssertValid() const
{
	CView::AssertValid();
}

void CCKORView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CCKORDoc* CCKORView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCKORDoc)));
	return (CCKORDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCKORView message handlers
