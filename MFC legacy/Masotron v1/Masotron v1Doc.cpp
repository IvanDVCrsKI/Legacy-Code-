// Masotron v1Doc.cpp : implementation of the CMasotronv1Doc class
//

#include "stdafx.h"
#include "Masotron v1.h"

#include "Masotron v1Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMasotronv1Doc

IMPLEMENT_DYNCREATE(CMasotronv1Doc, CDocument)

BEGIN_MESSAGE_MAP(CMasotronv1Doc, CDocument)
	//{{AFX_MSG_MAP(CMasotronv1Doc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMasotronv1Doc construction/destruction

CMasotronv1Doc::CMasotronv1Doc()
{
	// TODO: add one-time construction code here

}

CMasotronv1Doc::~CMasotronv1Doc()
{
}

BOOL CMasotronv1Doc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CMasotronv1Doc serialization

void CMasotronv1Doc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMasotronv1Doc diagnostics

#ifdef _DEBUG
void CMasotronv1Doc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMasotronv1Doc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMasotronv1Doc commands
