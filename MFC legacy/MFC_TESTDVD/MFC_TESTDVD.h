// MFC_TESTDVD.h : main header file for the MFC_TESTDVD application
//

#if !defined(AFX_MFC_TESTDVD_H__AE75AE54_C2EA_4D81_A22A_73E4C23FF87F__INCLUDED_)
#define AFX_MFC_TESTDVD_H__AE75AE54_C2EA_4D81_A22A_73E4C23FF87F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CMFC_TESTDVDApp:
// See MFC_TESTDVD.cpp for the implementation of this class
//

class CMFC_TESTDVDApp : public CWinApp
{
public:
	CMFC_TESTDVDApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMFC_TESTDVDApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CMFC_TESTDVDApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFC_TESTDVD_H__AE75AE54_C2EA_4D81_A22A_73E4C23FF87F__INCLUDED_)
