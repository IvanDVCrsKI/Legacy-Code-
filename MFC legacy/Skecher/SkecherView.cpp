// SkecherView.cpp : implementation of the CSkecherView class
//

#include "stdafx.h"
#include "Skecher.h"

#include "SkecherDoc.h"
#include "SkecherView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSkecherView

IMPLEMENT_DYNCREATE(CSkecherView, CView)

BEGIN_MESSAGE_MAP(CSkecherView, CView)
	//{{AFX_MSG_MAP(CSkecherView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSkecherView construction/destruction

CSkecherView::CSkecherView()
{
	// TODO: add construction code here

}

CSkecherView::~CSkecherView()
{
}

BOOL CSkecherView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CSkecherView drawing

void CSkecherView::OnDraw(CDC* pDC)
{
	CSkecherDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CSkecherView printing

BOOL CSkecherView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CSkecherView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CSkecherView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CSkecherView diagnostics

#ifdef _DEBUG
void CSkecherView::AssertValid() const
{
	CView::AssertValid();
}

void CSkecherView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CSkecherDoc* CSkecherView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CSkecherDoc)));
	return (CSkecherDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSkecherView message handlers
