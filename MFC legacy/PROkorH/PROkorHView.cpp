// PROkorHView.cpp : implementation of the CPROkorHView class
//

#include "stdafx.h"
#include "PROkorH.h"

#include "PROkorHDoc.h"
#include "PROkorHView.h"


#include "GeoML.h"
#include "KONTROLA.H"
#include "CRMM.h"
#include "TankOxi.h"
#include "Oxidator.h"
#include "xcevi_mp.h"
#include "x1x2.h"
#include "xgo12.h"

#include "DLetav1.h"

#include "Performanse_HIB.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPROkorHView

IMPLEMENT_DYNCREATE(CPROkorHView, CView)

BEGIN_MESSAGE_MAP(CPROkorHView, CView)
	//{{AFX_MSG_MAP(CPROkorHView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPROkorHView construction/destruction

CPROkorHView::CPROkorHView()
{
    m_Zumx = 8070;//8070
	m_Zumy = 8070;

}

CPROkorHView::~CPROkorHView()
{
}

BOOL CPROkorHView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CPROkorHView drawing

void CPROkorHView::OnDraw(CDC* dc)
{
	CPROkorHDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;
	CRect rect;
	CBrush brsBlack;//, *brsOld;

GetClientRect(&rect);
	brsBlack.CreateSolidBrush(RGB(0, 0, 0));


	CBitmap bitID;
	CDC dcID;

	// Load the bitmap from the resource
	bitID.LoadBitmap(IDB_BITMAP1);
	// Create a memory device compatible with the above CPaintDC variable
	dcID.CreateCompatibleDC(dc);
	// Select the new bitmap
	CBitmap *BmpPrevious = dcID.SelectObject(&bitID);

	// Copy the bits from the memory DC into the current dc
	dc->BitBlt(20, 10, 1300, 400, &dcID, 0, 0, SRCCOPY);

	// Restore the old bitmap
	dc->SelectObject(BmpPrevious);
	// Do not call CView::OnPaint() for painting messages
	// TODO: add draw code for native data here

   	CClientDC;
	dc->SetMapMode(MM_ANISOTROPIC);
	dc->SetViewportOrg(90, 620); //90 -620
	dc->SetWindowExt(m_Zumx,m_Zumy); //SetWindowExtent(100,100)  SetViewportExtent(200,200)  100 jedinica = 200 pixela
	                                                                                             //1 m = 2 pixela
	
	dc->SetViewportExt(1000,-1000);//1000  -1000

//----------------------------------------------------------------------------------
//*********************PERFORMANSE TANKA SA OXIDATOROM*******************************
//---------------------------------------------------------------------------------
CCRM fluid;
double chP_amb = fluid.CCRMgetP_amb();

GEOML gml;
double chdDD = gml.GEOMLgetddidL();

GEOXI tank;
double chMaxP_tank = tank.GEOXIgetMaxP_tank();
double chL_tank = tank.GEOXIgetL_tank();
double chD_tank = tank.GEOXIgetD_tank();
//double chMaxP_tank = tank
double chm_naponZ = tank.GEOXIgetm_naponZ();
double chtank = tank.GEOXIgetde_tank_min();
double stepenSigurnosti = tank.GEOXIgetstepenS();
double chVmtank = tank.GEOXIgetV_m_tank();


OXI oxi;
	double chcp_oxi = oxi.OXIgetcp_oxi(); //
    double chcv_oxi= oxi.OXIgetcv_oxi();
	double chkapa_oxi = oxi.OXIgetkapa_oxi();
	double chR_oxi = oxi.OXIgetR_oxi();
	double chT_oxi = oxi.OXIgetT_oxi();
	double chm_oxi2 = oxi.m_oxi;
    double chc_oxi = oxi.OXIgetc_oxi();
	double chP_oxi = oxi.OXIgetP_oxi()/100000;
    double chROoxi = oxi.OXIgetROoxi();
	#ifdef SA_MASOM
	double chP_oxi2 = oxi.OXIgetP_oxi2()/100000;
    #endif
	double chm_oxi = oxi.OXIgetm_oxi();
	double chV_tanka_kalkulacije = oxi.OXIgetV_tanka_kalkulacije();

XCEVI cev;

double chD_cevi = cev.XCEVIgetD_cevi();
double chA_cevi = cev.XCEVIgetA_cevi();
double chL_cevi = cev.XCEVIgetL_cevi();
double chV_cevi = cev.XCEVIgetV_cevi();
double chodnosP = cev.XCEVIgetodnosP();
double chUocevi = cev.XCEVIgetUocevi();
double chUkriticno = cev.XCEVIgetUkriticno();
double chPkriticno = cev.XCEVIgetPkriticno();
double chMaseniProtokKr = cev.XCEVIgetMaseniProtokKr();
double chZapreminskiKR = cev.XCEVIgetZapreminskiKR();
double chROkr = cev.XCEVIgetROkr();
double chTkr = cev.XCEVIgetTkr();
double chDhidraulicki = cev.XCEVIgetDhidraulicki();


BRIZGALJKA bri;
double chA2n = bri.BRIZGALJKAgetA_otvora();
double chBrojOtvora = bri.BRIZGALJKAgetBroj_otvora();
//ouble chV_brizgaljki = bri.BRIZGALJKAgetV_otvora();
double chAuk_izlazno = bri.BRIZGALJKAgetAuk_izlazno();
double chD_otvora = bri.BRIZGALJKAgetD_otvora();
double chMP_oxi = bri.BRIZGALJKAgetMP_oxi();

PERF per;
double chPotisak = per.PERFgetPotisak();


XGO gor;
double chRsago = gor.XGOgetP_generisano();
double chrt = gor.XGOgetrt();

DLV1 dl;
double chVy = dl.DLV1getVy();

KON kon;
double chK =kon.KONgetK();
double random = 1;//zbog baga



dc->SetTextColor(RGB(0,0,0));


char ch1[15];
sprintf(ch1,"D_tank:    %2f mm",chD_tank);
dc->TextOut (7400,2500 ,ch1);//x,y

char ch2[15];
sprintf(ch2,"L_tank:    %2f mm",chL_tank);
dc->TextOut (7400,2350 ,ch2);//x,y

dc->SetTextColor(RGB(255,255,255));
UpdateData();
char ch31[10];//////// BUG!!!!!!!!!!!!!!!!!
sprintf(ch31,"error:    %2f ",random);
dc->TextOut (7400,2200 ,ch31);//x,y
dc->SetTextColor(RGB(0,0,0));

char ch3[10];
sprintf(ch3,"de_tank_min:    %2f mm",chtank);
dc->TextOut (7400,2050 ,ch3);//x,y


char ch4[15];
sprintf(ch4,"Zatezna Cvrstoca:    %2f N/mm2",chm_naponZ);
dc->TextOut (7400,1900 ,ch4);//x,y


char ch5[15];
sprintf(ch5,"Stepen Sigurnosti:    %2f ",stepenSigurnosti);
dc->TextOut (7400,1750 ,ch5);//x,y

dc->SetTextColor(RGB(255,0,0));
char ch6[15];
sprintf(ch6,"Max Pritisak:    %2f BAR",chMaxP_tank);
dc->TextOut (7400,1600 ,ch6);//x,y


//-----------------KARAKTERISTIKE GASA------------------------ 
dc->SetTextColor(RGB(0,0,0));


char ch7[15];
sprintf(ch7,"cp:    %2f KJ/kgK",chcp_oxi);
dc->TextOut (7400,1450 ,ch7);//x,y

char ch8[15];
sprintf(ch8,"cv:    %2f KJ/kgK",chcv_oxi);
dc->TextOut (7400,1300 ,ch8);//x,y

char ch9[15];
sprintf(ch9,"kapa_oxi:    %2f ",chkapa_oxi);
dc->TextOut (7400,1150 ,ch9);//x,y

char ch10[15];
sprintf(ch10,"Toxi:    %2f K  %2f C",chT_oxi,chT_oxi-273.15);
dc->TextOut (7400,1000 ,ch10);//x,y

char ch11[15];
sprintf(ch11,"Roxi:    %2f J/kgK",chR_oxi);
dc->TextOut (7400,850 ,ch11);//x,y


 #ifdef BEZ_MASE
char ch12[15];
sprintf(ch12,"m_oxi:    %2f kg",chm_oxi);
dc->TextOut (7400,700 ,ch12);//x,y
dc->SetTextColor(RGB(0,0,255));

char ch13[15];
sprintf(ch13,"Poxi:    %2f BAR",chP_oxi);
dc->TextOut (7400,550 ,ch13);//x,y
#endif


#ifdef SA_MASOM

char ch14[15];
sprintf(ch14,"Poxi2:    %2f BAR",chP_oxi2);
dc->TextOut (7400,400 ,ch14);//x,y


dc->SetTextColor(RGB(0,0,0));

char ch15[15];
sprintf(ch15,"m_oxi2:    %2f Kg",chm_oxi2);
dc->TextOut (7400,250 ,ch15);//x,y





#endif

dc->SetTextColor(RGB(255,0,0));

char ch16[15];
sprintf(ch16,"V_OXI_tanka:    %2f m3",chVmtank);
dc->TextOut (7400,100 ,ch16);//x,y


dc->SetTextColor(RGB(0,0,0));
char ch17[15];
sprintf(ch17,"c_oxi:    %2f m/s",chc_oxi);
dc->TextOut (7400,-50 ,ch17);//x,y


char ch18[15];
sprintf(ch18,"ROoxi:    %2f Kg/m3",chROoxi);
dc->TextOut (7400,-200 ,ch18);//x,y

//chV_tanka_kalkulacije


//-------MASENI PROTOK U CEVI------------

dc->SetTextColor(RGB(0,0,0));
char chc1[15];
sprintf(chc1,"D_cevi:    %2f mm",chD_cevi);
dc->TextOut (5500,2500 ,chc1);//x,y

char chc2[15];
sprintf(chc2,"A_cevi:    %2f mm2",chA_cevi);
dc->TextOut (5500,2350 ,chc2);//x,y

char chc3[15];
sprintf(chc3,"L_cevi:    %2f mm",chL_cevi);
dc->TextOut (5500,2200 ,chc3);//x,y


char chc4[15];
sprintf(chc4,"V_cevi:    %2f m3",chV_cevi);
dc->TextOut (5500,2050 ,chc4);//x,

char chc5[15];
sprintf(chc5,"P_amb:  %2f BAR",chP_amb/100000);
dc->TextOut (5500,1900 ,chc5);//x

char chc6[15];
sprintf(chc6,"P_oxi:  %2f BAR ",chP_oxi);//
dc->TextOut (5500,1750 ,chc6);//x,

char chc7[15];
sprintf(chc7,"Pamb/Poxi[t0]:  %2f %2f ",chodnosP,   0.528);//
dc->TextOut (5500,1600 ,chc7);//x,

char chc8[15];
sprintf(chc8,"Uocevi[t0]:  %2f m/s ",chUocevi);//PREDPOSTAVKE DATE U XCEVI_MP.CPP
dc->TextOut (5500,1450 ,chc8);//x,

char chc9[15];
sprintf(chc9,"Ukriticno[t0]:  %2f m/s ",chUkriticno);//
dc->TextOut (5500,1300 ,chc9);//x,


char chc10[15];
sprintf(chc10,"Pkriticno[t0]:  %2f BAR ",chPkriticno/100000);//
dc->TextOut (5500,1150 ,chc10);//x,


char chc11[15];
sprintf(chc11,"MaseniProtokKr[t0]:  %2f Kg/s ",chMaseniProtokKr);//
dc->TextOut (5500,1000 ,chc11);//x,

char chc12[15];
sprintf(chc12,"ZapreminskiKR[t0]:  %2f m3/s ",chZapreminskiKR);//
dc->TextOut (5500,850 ,chc12);//x,

char chc13[15];
sprintf(chc13,"ROkr[t0]:  %2f Kg/m3 ",chROkr);//
dc->TextOut (5500,700 ,chc13);//x,

char chc14[15];
sprintf(chc14,"Tkr_Cevi[t0]:  %2f C ",chTkr-273.15);//
dc->TextOut (5500,550,chc14);//x,

char chc15[15];
sprintf(chc15,"Dhidraulicki:  %2f mm ",chDhidraulicki);//
dc->TextOut (5500,400,chc15);//x,




/*
char chc5[15];
sprintf(chc5,"ZP:  %2f m3/s  %2f L/s",chZapreminski2,chZapreminski2*1000);//1m3 = 0.0001L   //1000L = 1m3
dc->TextOut (5500,1900 ,chc5);//x,

char chc6[15];
sprintf(chc6,"L_cevi:  %2f mm ",chL_cevi);//
dc->TextOut (5500,1750 ,chc6);//x,

char chc7[15];
sprintf(chc7,"A_cevi:  %2f mm2 ",chA_cevi);//
dc->TextOut (5500,1600 ,chc7);//x,

char chc8[15];
sprintf(chc8,"V_cevi:  %2f m3 ",chV_cevi);//
dc->TextOut (5500,1450 ,chc8);//x,


char chc9[15];
sprintf(chc9,"Vi2_cevi:  %2f m/s ",chV2_cevi);//
dc->TextOut (5500,1300 ,chc9);//x,


*/




//-------BRIZGALJKA X1X2 PP --------------------

char chb1[15];
sprintf(chb1,"D_otvora:  %2f mm ",chD_otvora);
dc->TextOut (3900,2500 ,chb1);//x,

char chb2[15];
sprintf(chb2,"A_otvora:  %2f  mm2",chA2n);
dc->TextOut (3900,2350 ,chb2);//x,



char chb3[15];
sprintf(chb3,"BrojOtvora:  %2f  ",chBrojOtvora);
dc->TextOut (3900,2200 ,chb3);//x,

char chb4[15];
sprintf(chb4,"Auk_izlazno:  %2f mm2 ",chAuk_izlazno);
dc->TextOut (3900,2050 ,chb4);//x,

char chb5[15];
sprintf(chb5,"MP_oxi:  %2f Kg/s ",chMP_oxi);
dc->TextOut (3900,1900 ,chb5);//x,




//-------GORIVO XGO1-XGO2 PP --------------------



char chg1[15];
sprintf(chg1,"r:  %2f mm/s ",chRsago);
dc->TextOut (1900,0 ,chg1);//x,

char chg2[15];
sprintf(chg2,"r:  %2f cm/s ",chrt);
dc->TextOut (1900,0 ,chg2);//x,






//----------------------------------------------------------------------------------
//*********************PERFORMANSE RAKETNOG MOTORA**********************************
//---------------------------------------------------------------------------------

dc->SetTextColor(RGB(0,0,0));
char chp1[15];
sprintf(chp1,"Potisak:    %2f N",chPotisak);
dc->TextOut (1000,2500 ,chp1);//x,y






//UpdateData();
ReleaseDC(dc);







}

/////////////////////////////////////////////////////////////////////////////
// CPROkorHView printing

BOOL CPROkorHView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CPROkorHView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CPROkorHView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CPROkorHView diagnostics

#ifdef _DEBUG
void CPROkorHView::AssertValid() const
{
	CView::AssertValid();
}

void CPROkorHView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CPROkorHDoc* CPROkorHView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CPROkorHDoc)));
	return (CPROkorHDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPROkorHView message handlers
