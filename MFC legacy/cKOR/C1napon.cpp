// C1napon.cpp : implementation file
//

#include "stdafx.h"
#include "cKOR.h"
#include "C1napon.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CC1napon dialog


CC1napon::CC1napon(CWnd* pParent /*=NULL*/)
	: CDialog(CC1napon::IDD, pParent)
{
	//{{AFX_DATA_INIT(CC1napon)
	m_rsp = 0.0;
	m_run = 0.0;
	m_debljina = 0.0;
	m_odnosr = 0.0;
	m_ntri = 0.0;
	m_nrri = 0.0;
	m_pPu = 0.0;
	m_nlri = 0.0;
	m_ena = 0;
	m_taumax = 0.0;
	m_srednjinn = 0.0;
	m_Zatezna = 0.0;
	//}}AFX_DATA_INIT
}


void CC1napon::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CC1napon)
	DDX_Text(pDX, IDC_EDIT1, m_rsp);
	DDX_Text(pDX, IDC_EDIT2, m_run);
	DDX_Text(pDX, IDC_EDIT3, m_debljina);
	DDX_Text(pDX, IDC_EDIT4, m_odnosr);
	DDX_Text(pDX, IDC_EDIT6, m_ntri);
	DDX_Text(pDX, IDC_EDIT7, m_nrri);
	DDX_Text(pDX, IDC_EDIT5, m_pPu);
	DDX_Text(pDX, IDC_EDIT8, m_nlri);
	DDX_Text(pDX, IDC_EDIT10, m_ena);
	DDX_Text(pDX, IDC_EDIT9, m_taumax);
	DDX_Text(pDX, IDC_EDIT11, m_srednjinn);
	DDX_Text(pDX, IDC_EDIT12, m_Zatezna);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CC1napon, CDialog)
	//{{AFX_MSG_MAP(CC1napon)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CC1napon message handlers

void CC1napon::OnButton2() 
{
	UpdateData(true);
	m_debljina =m_rsp-m_run;
    m_odnosr =m_rsp/m_run;

    double ppas = m_pPu*100000;


double ksikv = m_odnosr*m_odnosr;


double Cti = (ksikv+1)/(ksikv-1);

m_ntri = (ppas*Cti)/m_ena;
m_nrri = (-ppas)/m_ena;

double Cli = 1/(ksikv-1); 
m_nlri = (ppas*Cli)/m_ena;

m_srednjinn = ((ppas*Cti)+(ppas*Cli)-ppas)/(3*m_ena);
m_taumax =((-ppas)-(ppas*Cti))/(2*m_ena);

/* 
radijalni       sigma 1
longitudalni    sigma 2
tangencijalni   sigma 3

  tau max = rad-tang/2
*/




	UpdateData(false);
	
	
}

