#include <afxwin.h>

class CMainFrame : public CFrameWnd
{
public:
	CMainFrame ();

protected:
	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMove(int x, int y);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);

	DECLARE_MESSAGE_MAP()
};

CMainFrame::CMainFrame()
{
	// Create the window's frame
	Create(NULL, "Ole", WS_OVERLAPPEDWINDOW,
	       CRect(120, 100, 700, 480), NULL);
}

class CExerciseApp: public CWinApp
{
public:
	BOOL InitInstance();
};

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_KEYDOWN()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOVE()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	return 0;
}

void CMainFrame::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	switch(nChar)
	{
	case VK_RETURN:
		SetWindowText("You pressed Enter");
		break;
	case VK_F1:
		SetWindowText("F1");
		break;
	case VK_F2:
		SetWindowText("F2");
		break;
	case VK_DELETE:
		SetWindowText("Can't Delete This");
		break;
	default:
		SetWindowText("Whatever");
	}
}
 
void CMainFrame::OnLButtonDown(UINT nFlags, CPoint point) 
{
	char *dar  = new char[200];

	sprintf(dar, "Pozicija  (%d,%d)",point.x,  point.y);
	
	SetWindowText(dar);

	

}


void CMainFrame::OnMouseMove(UINT nFlags, CPoint point)
{

char *MsgCoord  = new char[20];

	sprintf(MsgCoord, "Left = %d | Top = %d", point.x, point.y);
	
	SetWindowText(MsgCoord);


}



BOOL CExerciseApp::InitInstance()
{
	m_pMainWnd = new CMainFrame ;
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();

	return TRUE;
}

CExerciseApp theApp;

void CMainFrame::OnMove(int x, int y) 
{
	CFrameWnd::OnMove(x, y);
	
	// TODO: Add your message handler code here
	char *MsgCoord  = new char[20];

	sprintf(MsgCoord, "Left = %d | Top = %d", x, y);
	
	SetWindowText(MsgCoord);
}