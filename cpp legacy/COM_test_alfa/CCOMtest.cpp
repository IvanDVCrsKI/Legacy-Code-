// CCOMtest.cpp : implementation file
//

#include "stdafx.h"
#include "COM_test_alfa.h"
#include "CCOMtest.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCCOMtest dialog


CCCOMtest::CCCOMtest(CWnd* pParent /*=NULL*/)
	: CDialog(CCCOMtest::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCCOMtest)
	m_data = 0;
	m_status = _T("");
	//}}AFX_DATA_INIT
}


void CCCOMtest::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCCOMtest)
	DDX_Text(pDX, IDC_EDIT1, m_data);
	DDX_Text(pDX, IDC_EDIT2, m_status);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCCOMtest, CDialog)
	//{{AFX_MSG_MAP(CCCOMtest)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCCOMtest message handlers

void CCCOMtest::OnButton1() 
{


/*	

	if (statusPort_ == false)  // if port is opened already, not open port again.
  {
    handlePort_ = CreateFile(portName,  // Specify port device: default "COM1"
    GENERIC_READ | GENERIC_WRITE,       // Specify mode that open device.
    0,                                  // the devide isn't shared.
    NULL,                               // the object gets a default security.
    OPEN_EXISTING,                      // Specify which action to take on file. 
    0,                                  // default.
    NULL);                              // default.
*/
    // Get current configuration of serial communication port.
  
    AfxMessageBox("Get configuration port has problem.");

    SetDlgItemText(IDC_EDIT2,"EXtra");

		
}

void CCCOMtest::OnButton2() 
{
	SetDlgItemText(IDC_EDIT2,"CPP");
}
