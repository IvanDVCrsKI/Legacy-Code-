; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CSquaresView
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "SdiSquares.h"
LastPage=0

ClassCount=5
Class1=CSquaresApp
Class2=CSquaresDoc
Class3=CSquaresView
Class4=CMainFrame

ResourceCount=2
Resource1=IDR_MAINFRAME
Class5=CAboutDlg
Resource2=IDD_ABOUTBOX

[CLS:CSquaresApp]
Type=0
HeaderFile=SdiSquares.h
ImplementationFile=SdiSquares.cpp
Filter=N

[CLS:CSquaresDoc]
Type=0
HeaderFile=SquaresDoc.h
ImplementationFile=SquaresDoc.cpp
Filter=N
BaseClass=CDocument
VirtualFilter=DC
LastObject=CSquaresDoc

[CLS:CSquaresView]
Type=0
HeaderFile=SquaresView.h
ImplementationFile=SquaresView.cpp
Filter=C
LastObject=CSquaresView


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T




[CLS:CAboutDlg]
Type=0
HeaderFile=SdiSquares.cpp
ImplementationFile=SdiSquares.cpp
Filter=D
LastObject=CAboutDlg

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_SAVE_AS
Command5=ID_FILE_MRU_FILE1
Command6=ID_APP_EXIT
Command7=ID_APP_ABOUT
Command8=ID_COLOR_RED
Command9=ID_COLOR_YELLOW
Command10=ID_COLOR_GREEN
Command11=ID_COLOR_CYAN
Command12=ID_COLOR_BLUE
Command13=ID_COLOR_WHITE
CommandCount=13

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_UNDO
Command5=ID_EDIT_CUT
Command6=ID_EDIT_COPY
Command7=ID_EDIT_PASTE
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_NEXT_PANE
Command13=ID_PREV_PANE
CommandCount=13

