// ParametriDlg.cpp : implementation file
//

#include "stdafx.h"
#include "KreatorTrajektorija.h"


//Dijalozi
#include "ParametriDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CParametriDlg dialog


CParametriDlg::CParametriDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CParametriDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CParametriDlg)
	m_Brzina_f1 = 0;
	m_Brzina_f2 = 0;
	m_Brzina_f3 = 0;
	m_Ugao_f1 = 0;
	m_Ugao_f2 = 0;
	m_Ugao_f3 = 0;
	//}}AFX_DATA_INIT
}


void CParametriDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CParametriDlg)
	DDX_Text(pDX, IDC_BRZINA_F1, m_Brzina_f1);
	DDV_MinMaxInt(pDX, m_Brzina_f1, 0, 9000);
	DDX_Text(pDX, IDC_BRZINA_F2, m_Brzina_f2);
	DDV_MinMaxInt(pDX, m_Brzina_f2, 0, 9000);
	DDX_Text(pDX, IDC_BRZINA_F3, m_Brzina_f3);
	DDV_MinMaxInt(pDX, m_Brzina_f3, 0, 9000);
	DDX_Text(pDX, IDC_UGAO_F1, m_Ugao_f1);
	DDV_MinMaxInt(pDX, m_Ugao_f1, 0, 90);
	DDX_Text(pDX, IDC_UGAO_F2, m_Ugao_f2);
	DDV_MinMaxInt(pDX, m_Ugao_f2, 0, 90);
	DDX_Text(pDX, IDC_UGAO_F3, m_Ugao_f3);
	DDV_MinMaxInt(pDX, m_Ugao_f3, 0, 90);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CParametriDlg, CDialog)
	//{{AFX_MSG_MAP(CParametriDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CParametriDlg message handlers
