// ShapesView.h : interface of the CShapesView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SHAPESVIEW_H__C00A1A21_4EE0_4BAE_97D2_F0876BCDEF0D__INCLUDED_)
#define AFX_SHAPESVIEW_H__C00A1A21_4EE0_4BAE_97D2_F0876BCDEF0D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CShapesView : public CView
{
protected: // create from serialization only
	CShapesView();
	DECLARE_DYNCREATE(CShapesView)

// Attributes
public:
	CShapesDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CShapesView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CShapesView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CShapesView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in ShapesView.cpp
inline CShapesDoc* CShapesView::GetDocument()
   { return (CShapesDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SHAPESVIEW_H__C00A1A21_4EE0_4BAE_97D2_F0876BCDEF0D__INCLUDED_)
