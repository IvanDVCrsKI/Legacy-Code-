// Masotron v1.h : main header file for the MASOTRON V1 application
//

#if !defined(AFX_MASOTRONV1_H__A0CBFFB3_98A1_4725_910C_A0F7EEE08E34__INCLUDED_)
#define AFX_MASOTRONV1_H__A0CBFFB3_98A1_4725_910C_A0F7EEE08E34__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CMasotronv1App:
// See Masotron v1.cpp for the implementation of this class
//

class CMasotronv1App : public CWinApp
{
public:
	CMasotronv1App();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMasotronv1App)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CMasotronv1App)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MASOTRONV1_H__A0CBFFB3_98A1_4725_910C_A0F7EEE08E34__INCLUDED_)
