#if !defined(AFX_PENDIALOG_H__4DBD2AB9_765C_4693_A427_ECD2A1209ED9__INCLUDED_)
#define AFX_PENDIALOG_H__4DBD2AB9_765C_4693_A427_ECD2A1209ED9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PenDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPenDialog dialog

class CPenDialog : public CDialog
{
// Construction
public:
	CPenDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPenDialog)
	enum { IDD = IDD_DIALOG1 };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPenDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CPenDialog)
	afx_msg void OnPenwidth(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PENDIALOG_H__4DBD2AB9_765C_4693_A427_ECD2A1209ED9__INCLUDED_)
