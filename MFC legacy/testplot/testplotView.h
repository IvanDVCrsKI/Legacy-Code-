// testplotView.h : interface of the CTestplotView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_TESTPLOTVIEW_H__BD6EB454_244C_4B97_83EE_DBA07274C87D__INCLUDED_)
#define AFX_TESTPLOTVIEW_H__BD6EB454_244C_4B97_83EE_DBA07274C87D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CTestplotView : public CView
{
protected: // create from serialization only
	CTestplotView();
	DECLARE_DYNCREATE(CTestplotView)

// Attributes
public:
	CTestplotDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestplotView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTestplotView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CTestplotView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in testplotView.cpp
inline CTestplotDoc* CTestplotView::GetDocument()
   { return (CTestplotDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTPLOTVIEW_H__BD6EB454_244C_4B97_83EE_DBA07274C87D__INCLUDED_)
