#if !defined(AFX_CNAPONCEVI_H__5650F506_B226_4A99_B2B9_BE79C53D0D06__INCLUDED_)
#define AFX_CNAPONCEVI_H__5650F506_B226_4A99_B2B9_BE79C53D0D06__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CNaponCevi.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCNaponCevi dialog

class CCNaponCevi : public CDialog
{
// Construction
public:
	CCNaponCevi(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCNaponCevi)
	enum { IDD = IDD_NAPONCEVI };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCNaponCevi)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCNaponCevi)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CNAPONCEVI_H__5650F506_B226_4A99_B2B9_BE79C53D0D06__INCLUDED_)
