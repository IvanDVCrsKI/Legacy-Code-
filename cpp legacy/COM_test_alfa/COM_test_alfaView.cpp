// COM_test_alfaView.cpp : implementation of the CCOM_test_alfaView class
//

#include "stdafx.h"
#include "COM_test_alfa.h"

#include "COM_test_alfaDoc.h"
#include "COM_test_alfaView.h"


#include "CCOMtest.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCOM_test_alfaView

IMPLEMENT_DYNCREATE(CCOM_test_alfaView, CView)

BEGIN_MESSAGE_MAP(CCOM_test_alfaView, CView)
	//{{AFX_MSG_MAP(CCOM_test_alfaView)
	ON_COMMAND(IDM_COM_PORT, OnComPort)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCOM_test_alfaView construction/destruction

CCOM_test_alfaView::CCOM_test_alfaView()
{
	m_data = 10;
	
	
	
	
	
	
	// TODO: add construction code here

}

CCOM_test_alfaView::~CCOM_test_alfaView()
{
}

BOOL CCOM_test_alfaView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CCOM_test_alfaView drawing

void CCOM_test_alfaView::OnDraw(CDC* pDC)
{
	CCOM_test_alfaDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CCOM_test_alfaView diagnostics

#ifdef _DEBUG
void CCOM_test_alfaView::AssertValid() const
{
	CView::AssertValid();
}

void CCOM_test_alfaView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CCOM_test_alfaDoc* CCOM_test_alfaView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCOM_test_alfaDoc)));
	return (CCOM_test_alfaDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCOM_test_alfaView message handlers





void CCOM_test_alfaView::OnComPort() 
{
	CCCOMtest dlg;

	dlg.m_data = m_data;
	dlg.m_status= m_status;

		if (dlg.DoModal () == IDOK)
	{
	
	dlg.m_data = m_data;
	dlg.m_status= m_status;

		Invalidate ();
	}
	
	
}
