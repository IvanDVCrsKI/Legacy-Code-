// PenDialog.cpp : implementation file
//

#include "stdafx.h"
#include "SDI_Dialog.h"
#include "PenDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPenDialog dialog


CPenDialog::CPenDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CPenDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPenDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CPenDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPenDialog)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPenDialog, CDialog)
	//{{AFX_MSG_MAP(CPenDialog)
	ON_UPDATE_COMMAND_UI(ID_PENWIDTH, OnPenwidth)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPenDialog message handlers



	


void CPenDialog::OnPenwidth(CCmdUI* pCmdUI) 
{
	CPenDialog aDlg; 
	// Create a local dialog object
// Display the dialog as modal
     aDlg.DoModal();// TODO: Add your command update UI handler code here
	
}
