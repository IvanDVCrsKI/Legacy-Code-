; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CChildFrame
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "GRAFIK3.h"
LastPage=0

ClassCount=7
Class1=CGRAFIK3App
Class2=CGRAFIK3Doc
Class3=CGRAFIK3View
Class4=CMainFrame

ResourceCount=4
Resource1=IDR_MAINFRAME
Resource2=IDD_ABOUTBOX
Class5=CChildFrame
Class6=CAboutDlg
Resource3=IDR_GRAFIKTYPE
Class7=CCAutor
Resource4=IDD_DIALOG1

[CLS:CGRAFIK3App]
Type=0
HeaderFile=GRAFIK3.h
ImplementationFile=GRAFIK3.cpp
Filter=N
BaseClass=CWinApp
VirtualFilter=AC
LastObject=CGRAFIK3App

[CLS:CGRAFIK3Doc]
Type=0
HeaderFile=GRAFIK3Doc.h
ImplementationFile=GRAFIK3Doc.cpp
Filter=N

[CLS:CGRAFIK3View]
Type=0
HeaderFile=GRAFIK3View.h
ImplementationFile=GRAFIK3View.cpp
Filter=C
BaseClass=CView
VirtualFilter=VWC
LastObject=CGRAFIK3View


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T
BaseClass=CMDIFrameWnd
VirtualFilter=fWC
LastObject=CMainFrame


[CLS:CChildFrame]
Type=0
HeaderFile=ChildFrm.h
ImplementationFile=ChildFrm.cpp
Filter=M
BaseClass=CMDIChildWnd
VirtualFilter=mfWC
LastObject=CChildFrame


[CLS:CAboutDlg]
Type=0
HeaderFile=GRAFIK3.cpp
ImplementationFile=GRAFIK3.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_MRU_FILE1
Command4=ID_APP_EXIT
Command5=ID_APP_ABOUT
CommandCount=5

[MNU:IDR_GRAFIKTYPE]
Type=1
Class=CGRAFIK3View
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_CLOSE
Command4=ID_FILE_SAVE
Command5=ID_FILE_SAVE_AS
Command6=ID_FILE_MRU_FILE1
Command7=ID_APP_EXIT
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_WINDOW_NEW
Command13=ID_WINDOW_CASCADE
Command14=ID_WINDOW_TILE_HORZ
Command15=ID_WINDOW_ARRANGE
Command16=ID_APP_ABOUT
Command17=IDM_AUTOR
CommandCount=17

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_UNDO
Command5=ID_EDIT_CUT
Command6=ID_EDIT_COPY
Command7=ID_EDIT_PASTE
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_NEXT_PANE
Command13=ID_PREV_PANE
CommandCount=13

[DLG:IDD_DIALOG1]
Type=1
Class=CCAutor
ControlCount=4
Control1=IDC_STATIC,static,1342177294
Control2=IDC_STATIC,static,1342177294
Control3=IDC_STATIC,static,1342308865
Control4=IDC_STATIC,static,1342308865

[CLS:CCAutor]
Type=0
HeaderFile=CAutor.h
ImplementationFile=CAutor.cpp
BaseClass=CDialog
Filter=D
LastObject=IDM_AUTOR
VirtualFilter=dWC

