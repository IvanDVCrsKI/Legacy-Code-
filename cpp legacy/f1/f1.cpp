// vezba funkcije u funkcijama
//

#include "stdafx.h"
#include <iostream> 

using namespace std;

int function2()                        //.3
{
	cout << "in f2\n";
	return 0;
}
void function1()                       //.2
{
	cout << "in f1\n";
    function2();
	cout << "in f1 again\n";
}
int main()                             //.1  
{
	cout << "main\n";
	function1();
	cout << "main end\n";
	return 0;
}

// funkcija int,void,double... dasfsdg() nikad na kraju izraza nema ;
// prvo mora da se definisu funkcije u okviru f- main() i to redosledom 3 , 2 ,1 u datom slucaju
// zagrade { i } se pisu kao granice f-je 
// f-ja void nama return 0 jer nema sta da povrati tj moze se napisati kao return; ili samo }
// 