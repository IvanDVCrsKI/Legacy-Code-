// FlickerFreeDoc.h : interface of the CFlickerFreeDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_FLICKERFREEDOC_H__715F77EE_3E91_490C_944E_B2BDA35AE2DD__INCLUDED_)
#define AFX_FLICKERFREEDOC_H__715F77EE_3E91_490C_944E_B2BDA35AE2DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CFlickerFreeDoc : public CDocument
{
protected: // create from serialization only
	CFlickerFreeDoc();
	DECLARE_DYNCREATE(CFlickerFreeDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFlickerFreeDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CFlickerFreeDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CFlickerFreeDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FLICKERFREEDOC_H__715F77EE_3E91_490C_944E_B2BDA35AE2DD__INCLUDED_)
