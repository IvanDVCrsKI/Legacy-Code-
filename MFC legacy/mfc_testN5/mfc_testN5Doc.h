// mfc_testN5Doc.h : interface of the CMfc_testN5Doc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MFC_TESTN5DOC_H__E013A533_1C0D_4698_8469_AEDAAB47E57F__INCLUDED_)
#define AFX_MFC_TESTN5DOC_H__E013A533_1C0D_4698_8469_AEDAAB47E57F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMfc_testN5Doc : public CDocument
{
protected: // create from serialization only
	CMfc_testN5Doc();
	DECLARE_DYNCREATE(CMfc_testN5Doc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMfc_testN5Doc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMfc_testN5Doc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMfc_testN5Doc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFC_TESTN5DOC_H__E013A533_1C0D_4698_8469_AEDAAB47E57F__INCLUDED_)
