#ifdef _WIN32
#include <windows.h>//requires that windows.h be included before either gl.h or glu.h, because some macros
#endif
//#include <glut.h>
#include <stdlib.h> //Needed for "exit" function
#include "gltools.h"
#include "glframe.h" 
#include "math3d.h"


#include <math.h>

#define NUM_SPHERES      2
GLFrame    spheres[NUM_SPHERES];

GLFrame fl;
 float x,z;
GLfloat y =0;
GLfloat k1 =0;
GLfloat k2 =0;
GLfloat k3 =0;

float k = 0;


void SetupRC()
    {
	
     int iSphere;

    // Bluish background
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f );
         
    // Draw everything as wire frame
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    
     // Randomly place the sphere inhabitants
    for(iSphere = 0; iSphere < NUM_SPHERES; iSphere+=1)
        {
        // Pick a random location between -20 and 20 at .1 increments
        //      float x = ((float)((rand() % 400) - 200) * 0.1f);
        //		float z = (float)((rand() % 400) - 200) * 0.1f;
    
	 x=z=iSphere;
        spheres[iSphere].SetOrigin(x, 0.0f, z);
        }
  
    }
void DrawGround(void)
    {
    GLfloat fExtent = 20.0f;
    GLfloat fStep = 1.0f;
    GLfloat y = -0.4f;
    GLint iLine;
    
    glBegin(GL_LINES);
       for(iLine = -fExtent; iLine <= fExtent; iLine += fStep)
          {
          glVertex3f(iLine, y, fExtent);    // Draw Z lines
          glVertex3f(iLine, y, -fExtent);
    
          glVertex3f(fExtent, y, iLine);
          glVertex3f(-fExtent, y, iLine);
          }
    
    glEnd();

    }
void DrawGround2(void)
    {
    GLfloat fExtent = 20.0f;
    GLfloat fStep = 1.0f;
    GLfloat y = 1+k;
    GLint iLine;
    
    glBegin(GL_LINES);
       for(iLine = -fExtent; iLine <= fExtent; iLine += fStep)
          {
          glVertex3f(iLine, y, fExtent);    // Draw Z lines
          glVertex3f(iLine, y, -fExtent);
    
          glVertex3f(fExtent, y, iLine);
          glVertex3f(-fExtent, y, iLine);
          }
    
    glEnd();
    }

/*

*/
void DrawElements(void)
    {
   for(int i = 0; i < NUM_SPHERES; i++)
        {
        glPushMatrix();
        spheres[i].ApplyActorTransform();
        glutSolidSphere(0.3f, 17, 9);
        glPopMatrix();
        }
    }
// Called to draw scene
void RenderScene(void)
    {
   
        
    // Clear the window with current clearing color
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
    glPushMatrix();
        fl.ApplyCameraTransform();
        
        // Draw the ground
        DrawGround();
        
 DrawGround2();
		 
	glPopMatrix();
	 glPushMatrix();
	
	fl.ApplyCameraTransform();
	DrawElements();spheres[0].SetOrigin(x+k1, y+k2, z+k3);

	glPopMatrix();


	 
           // Do the buffer Swap
    glutSwapBuffers();
    }
void processNormalKeys(unsigned char key, int x, int y) 
{

	if (key == 27)
		exit(0);

	switch(key) 
	{
			case 119://malo w
			fl.MoveUp(0.1f); break;
					
		
			case 115://malo s	
			fl.MoveUp (-0.1f); break;

			case 97://malo a
			k3+=0.1f; glutPostRedisplay();break;
				//fl.RotateLocalY (0.5f); break;

			
			case 100://malo d
				k3-=0.1f; glutPostRedisplay(); break;

			case 101://malo e
			k1+=0.1f;; break;

			case 113://malo q
			k1-=0.1f; break;
/*
			case 120://malo x
			h -= 1.0f; break;

			case 122://malo y  //121 je y ali tastatura je drugacije organizovana pa je y=122 umesto 121 po ascii dec
			h += 1.0f; break;
*/


	}
	    glutPostRedisplay();
}

// Respond to arrow keys by moving the camera frame of reference
void SpecialKeys(int key, int x, int y)
    {
    if(key == GLUT_KEY_UP)
		 fl.MoveForward(0.1f);
	  
        
    if(key == GLUT_KEY_DOWN)
     fl.MoveForward(-0.1f);


    if(key == GLUT_KEY_LEFT)
      fl.MoveRight(0.1f);
        
    if(key == GLUT_KEY_RIGHT)
       fl.MoveRight(-0.1f);
  
    
	// Refresh the Window
    glutPostRedisplay();
    }
//////////////////////////////////////////////////////////
// Called by GLUT library when idle (window not being
// resized or moved)
void TimerFunction(int value)
    {
    // Redraw the scene with new coordinates
    glutPostRedisplay();
    glutTimerFunc(1,TimerFunction, 1);
	k3-=0.1f;
	if (k3<=-20)
	{
		k3=20;
	}

    }

void ChangeSize(int w, int h)
    {
    GLfloat fAspect;

    // Prevent a divide by zero, when window is too short
    // (you cant make a window of zero width).
    if(h == 0)
        h = 1;

    glViewport(0, 0, w, h);
        
    fAspect = (GLfloat)w / (GLfloat)h;

    // Reset the coordinate system before modifying
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
	
    // Set the clipping volume
    gluPerspective(35.0f, fAspect, 1.0f, 50.0f);
        
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    }

int main(int argc, char* argv[])
    {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(800,600);
    glutCreateWindow("OpenGL SphereWorld Demo");
    glutReshapeFunc(ChangeSize);
    glutDisplayFunc(RenderScene);
	glutKeyboardFunc(processNormalKeys);
    glutSpecialFunc(SpecialKeys);

    SetupRC();
    glutTimerFunc(33, TimerFunction, 1);

    glutMainLoop();

    return 0;
    }
