; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CMreza
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "KreatorTrajektorijaDV.h"
LastPage=0

ClassCount=11
Class1=CKreatorTrajektorijaDVApp
Class2=CKreatorTrajektorijaDVDoc
Class3=CKreatorTrajektorijaDVView
Class4=CMainFrame

ResourceCount=8
Resource1=IDD_KORAK
Resource2=IDD_MREZA
Class5=CAboutDlg
Class6=CParametri
Resource3=IDD_UPUTSTVO
Class7=CMreza
Resource4=IDD_PARAMETRI
Class8=CKorak
Resource5=IDD_AUTORDV
Class9=CUputstvo
Resource6=IDD_ABOUTBOX
Class10=CAutorDV
Resource7=IDR_MAINFRAME
Class11=CDGrafik
Resource8=IDD_DGRAFIK

[CLS:CKreatorTrajektorijaDVApp]
Type=0
HeaderFile=KreatorTrajektorijaDV.h
ImplementationFile=KreatorTrajektorijaDV.cpp
Filter=N

[CLS:CKreatorTrajektorijaDVDoc]
Type=0
HeaderFile=KreatorTrajektorijaDVDoc.h
ImplementationFile=KreatorTrajektorijaDVDoc.cpp
Filter=N

[CLS:CKreatorTrajektorijaDVView]
Type=0
HeaderFile=KreatorTrajektorijaDVView.h
ImplementationFile=KreatorTrajektorijaDVView.cpp
Filter=C
BaseClass=CView
VirtualFilter=VWC
LastObject=IDM_KORAK


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T




[CLS:CAboutDlg]
Type=0
HeaderFile=KreatorTrajektorijaDV.cpp
ImplementationFile=KreatorTrajektorijaDV.cpp
Filter=D
LastObject=CAboutDlg

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_SAVE_AS
Command5=ID_FILE_MRU_FILE1
Command6=ID_APP_EXIT
Command7=IDM_PARAMETRI
Command8=IDM_MREZA
Command9=IDM_DGRAFIK
Command10=IDM_KORAK
Command11=ID_APP_ABOUT
Command12=IDM_AUTOR
Command13=IDM_UPUTSTVO
CommandCount=13

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_UNDO
Command5=ID_EDIT_CUT
Command6=ID_EDIT_COPY
Command7=ID_EDIT_PASTE
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_NEXT_PANE
Command13=ID_PREV_PANE
CommandCount=13

[DLG:IDD_PARAMETRI]
Type=1
Class=CParametri
ControlCount=18
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_UGAO_F1,edit,1350631552
Control4=IDC_BRZINA_F1,edit,1350631552
Control5=IDC_UGAO_F2,edit,1350631552
Control6=IDC_BRZINA_F2,edit,1350631552
Control7=IDC_UGAO_F3,edit,1350631552
Control8=IDC_BRZINA_F3,edit,1350631552
Control9=IDC_STATIC,button,1342177287
Control10=IDC_STATIC,button,1342177287
Control11=IDC_STATIC,button,1342177287
Control12=IDC_STATIC,static,1342308352
Control13=IDC_STATIC,static,1342308352
Control14=IDC_STATIC,static,1342308352
Control15=IDC_STATIC,static,1342308352
Control16=IDC_STATIC,static,1342308352
Control17=IDC_STATIC,static,1342308352
Control18=IDC_STATIC,button,1342178055

[CLS:CParametri]
Type=0
HeaderFile=Parametri.h
ImplementationFile=Parametri.cpp
BaseClass=CDialog
Filter=D
LastObject=CParametri
VirtualFilter=dWC

[DLG:IDD_MREZA]
Type=1
Class=CMreza
ControlCount=7
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_MREZA_DA,button,1342308361
Control4=IDC_MREZA_NE,button,1342177289
Control5=IDC_STATIC,button,1342177287
Control6=IDC_STATIC,button,1342178055
Control7=IDC_GMREZA,edit,1350631552

[CLS:CMreza]
Type=0
HeaderFile=Mreza.h
ImplementationFile=Mreza.cpp
BaseClass=CDialog
Filter=D
LastObject=CMreza
VirtualFilter=dWC

[DLG:IDD_KORAK]
Type=1
Class=CKorak
ControlCount=11
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_KORAK,edit,1350631552
Control4=IDC_LOOP,edit,1350631552
Control5=IDC_BROJTACAKA,edit,1350631552
Control6=IDC_STATIC,button,1342177287
Control7=IDC_STATIC,button,1342177287
Control8=IDC_IZRACUNAJ,button,1342242816
Control9=IDC_STATIC,static,1342308352
Control10=IDC_STATIC,static,1342308352
Control11=IDC_STATIC,static,1342308352

[CLS:CKorak]
Type=0
HeaderFile=Korak.h
ImplementationFile=Korak.cpp
BaseClass=CDialog
Filter=D
LastObject=IDC_IZRACUNAJ
VirtualFilter=dWC

[DLG:IDD_UPUTSTVO]
Type=1
Class=CUputstvo
ControlCount=2
Control1=IDOK,button,1342242817
Control2=IDC_STATIC,static,1342308352

[CLS:CUputstvo]
Type=0
HeaderFile=Uputstvo.h
ImplementationFile=Uputstvo.cpp
BaseClass=CDialog
Filter=D
LastObject=CUputstvo

[DLG:IDD_AUTORDV]
Type=1
Class=CAutorDV
ControlCount=6
Control1=IDOK,button,1342242817
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342308865
Control5=IDC_STATIC,static,1342308865
Control6=IDC_STATIC,static,1342177294

[CLS:CAutorDV]
Type=0
HeaderFile=AutorDV.h
ImplementationFile=AutorDV.cpp
BaseClass=CDialog
Filter=D
LastObject=CAutorDV

[DLG:IDD_DGRAFIK]
Type=1
Class=CDGrafik
ControlCount=5
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_DGRAF_DA,button,1342308361
Control4=IDC_DGRAF_NE,button,1342177289
Control5=IDC_STATIC,button,1342177287

[CLS:CDGrafik]
Type=0
HeaderFile=DGrafik.h
ImplementationFile=DGrafik.cpp
BaseClass=CDialog
Filter=D
LastObject=CDGrafik
VirtualFilter=dWC

