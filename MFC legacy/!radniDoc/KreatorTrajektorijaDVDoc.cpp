// KreatorTrajektorijaDVDoc.cpp : implementation of the CKreatorTrajektorijaDVDoc class
//

#include "stdafx.h"
#include "KreatorTrajektorijaDV.h"

#include "KreatorTrajektorijaDVDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaDVDoc

IMPLEMENT_DYNCREATE(CKreatorTrajektorijaDVDoc, CDocument)

BEGIN_MESSAGE_MAP(CKreatorTrajektorijaDVDoc, CDocument)
	//{{AFX_MSG_MAP(CKreatorTrajektorijaDVDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaDVDoc construction/destruction

CKreatorTrajektorijaDVDoc::CKreatorTrajektorijaDVDoc()
{
	// TODO: add one-time construction code here

}

CKreatorTrajektorijaDVDoc::~CKreatorTrajektorijaDVDoc()
{
}

BOOL CKreatorTrajektorijaDVDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaDVDoc serialization

void CKreatorTrajektorijaDVDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here  ar << m_Brzina_f1;

	}
	else
	{
		// TODO: add loading code here  ar >> m_Brzina_f1;

	}
}

/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaDVDoc diagnostics

#ifdef _DEBUG
void CKreatorTrajektorijaDVDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CKreatorTrajektorijaDVDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaDVDoc commands
