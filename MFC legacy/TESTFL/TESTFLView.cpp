// TESTFLView.cpp : implementation of the CTESTFLView class
//

#include "stdafx.h"
#include "TESTFL.h"

#include "memdc.h"


#include "TESTFLDoc.h"
#include "TESTFLView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTESTFLView

IMPLEMENT_DYNCREATE(CTESTFLView, CView)

BEGIN_MESSAGE_MAP(CTESTFLView, CView)
	//{{AFX_MSG_MAP(CTESTFLView)
	ON_WM_ERASEBKGND()
	ON_WM_MOUSEMOVE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTESTFLView construction/destruction

CTESTFLView::CTESTFLView()
{
	// TODO: add construction code here

}

CTESTFLView::~CTESTFLView()
{
}

BOOL CTESTFLView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CTESTFLView drawing

void CTESTFLView::OnDraw(CDC* dc)
{
    CMemDC pDC(dc);
	CTESTFLDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here

 dc->MoveTo(point.x-4, 0 );//LINIJA ZA PRACENJE X OSE  
 dc->LineTo(point.x-4, point.y+10000);

 dc->MoveTo(0,point.y-40);//LINIJA ZA PRACENJE Y OSE
 dc->LineTo(point.x+10000, point.y-40);

	CRect Recto; //PRAVOUGAONIK EKRANA
    GetClientRect (&Recto);//PRAV. KLIJENTA TJ KORISNIKA I NJEGOVE REZOLUCIJE

	int nWidth  =Recto.Width();//int MERA SIRINE
    int nHeight = Recto.Height();// int MERA DUZINE

GetCursorPos(&point);// ZADNJA POZICIJA MISA KORISNIKA



//----------------------------------------------------------	     
//POZICIJA TEKSTA NA KORDINATU EKRANA!!! KOS1 FIKSNA POZICIJA
//----------------------------------------------------------
char t1[10];
sprintf(t1, "Pozicija  (%d,%d)",point.x,  point.y);
pDC->TextOut (nWidth/1.2,nHeight/5 ,t1);



	CPen PenBlue2(PS_SOLID, 1, RGB(0, 0, 255));
	dc->SelectObject(PenBlue2);

    dc->MoveTo(nWidth / 2, 0);  //LINIJA ZA PRIKAZ POLOVINE X EKRANA
	dc->LineTo(nWidth / 2, nHeight);
	dc->MoveTo(0, nHeight/ 2);//LINIJA ZA PRIKAZ POLOVINE Y EKRANA
	dc->LineTo(nWidth, nHeight / 2);















}

/////////////////////////////////////////////////////////////////////////////
// CTESTFLView diagnostics

#ifdef _DEBUG
void CTESTFLView::AssertValid() const
{
	CView::AssertValid();
}

void CTESTFLView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTESTFLDoc* CTESTFLView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTESTFLDoc)));
	return (CTESTFLDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTESTFLView message handlers

void CTESTFLView::OnMouseMove(UINT nFlags, CPoint point) 
{

	::SetCursor(::LoadCursor(NULL, IDC_CROSS));

	//Invalidate();
}

BOOL CTESTFLView::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default
	
	return false;
}

void CTESTFLView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo) 
{
	pDC->SetBkColor(RGB(0, 100, 100));
	pDC->SetMapMode(MM_ANISOTROPIC);
	CView::OnPrepareDC(pDC, pInfo);
}


