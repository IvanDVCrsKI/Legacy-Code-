// C3napon.cpp : implementation file
//

#include "stdafx.h"
#include "cKOR.h"
#include "C3napon.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CC3napon dialog


CC3napon::CC3napon(CWnd* pParent /*=NULL*/)
	: CDialog(CC3napon::IDD, pParent)
{
	//{{AFX_DATA_INIT(CC3napon)
	m_zateznaT = 0.0;
	m_kvalitetS = 0.0;
	m_PuD = 0.0;
	m_Dun = 0.0;
	m_Dun2 = 0.0;
	m_Dsp1 = 0.0;
	m_Dsp2 = 0.0;
	m_PuD2 = 0.0;
	m_DD1 = 0.0;
	m_DD2 = 0.0;
	m_DP1 = 0.0;
	m_DP2 = 0.0;
	m_PP1 = 0.0;
	m_PP2 = 0.0;
	//}}AFX_DATA_INIT
}


void CC3napon::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CC3napon)
	DDX_Text(pDX, IDC_EDIT3, m_zateznaT);
	DDX_Text(pDX, IDC_EDIT4, m_kvalitetS);
	DDV_MinMaxDouble(pDX, m_kvalitetS, 0., 1.);
	DDX_Text(pDX, IDC_EDIT1, m_PuD);
	DDX_Text(pDX, IDC_EDIT2, m_Dun);
	DDX_Text(pDX, IDC_EDIT7, m_Dun2);
	DDX_Text(pDX, IDC_EDIT14, m_Dsp1);
	DDX_Text(pDX, IDC_EDIT19, m_Dsp2);
	DDX_Text(pDX, IDC_EDIT13, m_PuD2);
	DDX_Text(pDX, IDC_EDIT6, m_DD1);
	DDX_Text(pDX, IDC_EDIT18, m_DD2);
	DDX_Text(pDX, IDC_EDIT5, m_DP1);
	DDX_Text(pDX, IDC_EDIT17, m_DP2);
	DDX_Text(pDX, IDC_EDIT9, m_PP1);
	DDX_Text(pDX, IDC_EDIT22, m_PP2);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CC3napon, CDialog)
	//{{AFX_MSG_MAP(CC3napon)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CC3napon message handlers

void CC3napon::OnButton2() 
{
	// TODO: Add your control notification handler code here
	
}
