// MFC_TVer3View.h : interface of the CMFC_TVer3View class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MFC_TVER3VIEW_H__1ED45A35_CF20_4FC7_8FFE_67C9B6757961__INCLUDED_)
#define AFX_MFC_TVER3VIEW_H__1ED45A35_CF20_4FC7_8FFE_67C9B6757961__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMFC_TVer3View : public CView
{
protected: // create from serialization only
	CMFC_TVer3View();
	DECLARE_DYNCREATE(CMFC_TVer3View)

// Attributes
public:
	CMFC_TVer3Doc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMFC_TVer3View)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMFC_TVer3View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMFC_TVer3View)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in MFC_TVer3View.cpp
inline CMFC_TVer3Doc* CMFC_TVer3View::GetDocument()
   { return (CMFC_TVer3Doc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFC_TVER3VIEW_H__1ED45A35_CF20_4FC7_8FFE_67C9B6757961__INCLUDED_)
