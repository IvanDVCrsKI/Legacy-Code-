// Skecher.h : main header file for the SKECHER application
//

#if !defined(AFX_SKECHER_H__449B81AC_1238_4198_86D6_C64992C5067E__INCLUDED_)
#define AFX_SKECHER_H__449B81AC_1238_4198_86D6_C64992C5067E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CSkecherApp:
// See Skecher.cpp for the implementation of this class
//

class CSkecherApp : public CWinApp
{
public:
	CSkecherApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSkecherApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CSkecherApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SKECHER_H__449B81AC_1238_4198_86D6_C64992C5067E__INCLUDED_)
