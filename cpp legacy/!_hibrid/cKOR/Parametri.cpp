// Parametri.cpp : implementation file
//

#include "stdafx.h"
#include "cKOR.h"
#include "Parametri.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CParametri dialog


CParametri::CParametri(CWnd* pParent /*=NULL*/)
	: CDialog(CParametri::IDD, pParent)
{
	//{{AFX_DATA_INIT(CParametri)
	m_alfa = 0.0;
	m_beta = 0.0;
	m_DDI = 0.0;
	m_DKO = 0.0;
	m_RKR = 0.0;
	m_DKR = 0.0;
	//}}AFX_DATA_INIT
}


void CParametri::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CParametri)
	DDX_Text(pDX, IDC_ALFA, m_alfa);
	DDX_Text(pDX, IDC_BETA, m_beta);
	DDX_Text(pDX, IDC_DUZ_DDI, m_DDI);
	DDX_Text(pDX, IDC_DUZ_DKO, m_DKO);
	DDX_Text(pDX, IDC_RKR, m_RKR);
	DDX_Text(pDX, IDC_DKR, m_DKR);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CParametri, CDialog)
	//{{AFX_MSG_MAP(CParametri)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CParametri message handlers
