/************************************************************************************************************************
 * NAME: DirectCSPort.cpp
 * DATE:  July 1, 2003
 * PGMR: Y. Bai
 * DESC:  Serial communication   test program using C code directly access the serial port.
 ************************************************************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <conio.h>
#include <ctype.h>
#include <math.h>
#include <dos.h>
#define  MAX 5000 // max length of delay period of time
#define  COM1 (unsigned short)0x3F8 // serial port COM1
#define  COM2 (unsigned short)0x2F8 // serial port COM2
int   TestSerial();
//void Delay(int num);


void main(void)
{
         int rc = 0;
         printf("\n");
         rc = TestSerial();
         if (rc != 0)
    printf("Error in TestSerial()!\n");
         else
printf("Success in TestSerial()!\n");
         return;
}

/***************************************************************************
*              Subroutine to loop testing for the RS-232C port.                                                    *
 ***************************************************************************/
int TestSerial()
{
      int  index, value, result = 0;
     printf("Begin to execute outp() & inp()...\n");
     for (index = 0; index < 10; index++)
    {
           printf("Data sent to COM1 is: %d\n", index);
           _outp(COM1, index);
           //Delay(500);
           value = _inp(COM1);
           printf("Data returned from COM1 is: %d\n", value);
           printf("\n");
           if (value != index)
          {
    printf("Error in loop testing of the COM1!\n");
    result = -1;
    return result;
           }
    }
    return result;
}
/*
*****************************************************************************
 *              Delay subroutine to delay certain period of time                                                      *
 ****************************************************************************
void Delay(int num)
{
        int m, n, cycle = MAX;
        for (m = 0; m <= num * cycle; m++)
n = m - 1;
        return;
}
/******************************* End of Code *********************************/