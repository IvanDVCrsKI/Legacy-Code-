#include <stdio.h>
#include <stdlib.h>
//sizeof how much memory compiler assign for different variables
int main()
{
    int a;
    char b;
    float c;
    short int d;
    double e;
    const double F;
    long double g;
    printf("Size of INT is:\t%d bytes",sizeof(a));
    printf("\nSize of CHAR is:\t%d bytes",sizeof(b));
    printf("\nSize of FLOAT is:\t%d bytes",sizeof(c));
    printf("\nSize of SHORT INT is:\t%d bytes",sizeof(d));
    printf("\nSize of DOUBLE is:\t%d bytes",sizeof(e));
    printf("\nSize of CONST DOUBLE is:\t%d bytes",sizeof(F));
    printf("\nSize of LONG DOUBLE is:\t%d bytes",sizeof(g));




    return 0;
}
