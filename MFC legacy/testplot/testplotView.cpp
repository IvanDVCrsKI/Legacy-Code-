// testplotView.cpp : implementation of the CTestplotView class
//

#include "stdafx.h"
#include "testplot.h"

#include "testplotDoc.h"
#include "testplotView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestplotView

IMPLEMENT_DYNCREATE(CTestplotView, CView)

BEGIN_MESSAGE_MAP(CTestplotView, CView)
	//{{AFX_MSG_MAP(CTestplotView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestplotView construction/destruction

CTestplotView::CTestplotView()
{
	// TODO: add construction code here

}

CTestplotView::~CTestplotView()
{
}

BOOL CTestplotView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CTestplotView drawing

void CTestplotView::OnDraw(CDC* pDC)
{
	CTestplotDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CTestplotView printing

BOOL CTestplotView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CTestplotView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CTestplotView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CTestplotView diagnostics

#ifdef _DEBUG
void CTestplotView::AssertValid() const
{
	CView::AssertValid();
}

void CTestplotView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTestplotDoc* CTestplotView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTestplotDoc)));
	return (CTestplotDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTestplotView message handlers
