#if !defined(AFX_CAUTOR_H__2CC95ACA_857C_47EE_A913_E9551BDE2FE5__INCLUDED_)
#define AFX_CAUTOR_H__2CC95ACA_857C_47EE_A913_E9551BDE2FE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CAutor.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCAutor dialog

class CCAutor : public CDialog
{
// Construction
public:
	CCAutor(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCAutor)
	enum { IDD = IDD_AUTOR };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCAutor)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCAutor)
	afx_msg void OnAutor();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CAUTOR_H__2CC95ACA_857C_47EE_A913_E9551BDE2FE5__INCLUDED_)
