//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by mfc_test10.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_MFC_TETYPE                  129
#define IDD_OPTIONS                     130
#define IDC_EDIT1                       1000
#define IDC_RESET                       1001
#define IDC_EDIT2                       1002
#define ID_MENUITEM32771                32771
#define ID_FILE_OPTIONS                 32772

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
