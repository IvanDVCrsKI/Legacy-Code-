// MFC_TestN7View.h : interface of the CMFC_TestN7View class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MFC_TESTN7VIEW_H__32B6EEAC_D3C0_4597_8CA1_6568EFEAB869__INCLUDED_)
#define AFX_MFC_TESTN7VIEW_H__32B6EEAC_D3C0_4597_8CA1_6568EFEAB869__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMFC_TestN7View : public CView
{
protected: // create from serialization only
	CMFC_TestN7View();
	DECLARE_DYNCREATE(CMFC_TestN7View)

// Attributes
public:
	CMFC_TestN7Doc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMFC_TestN7View)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMFC_TestN7View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMFC_TestN7View)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in MFC_TestN7View.cpp
inline CMFC_TestN7Doc* CMFC_TestN7View::GetDocument()
   { return (CMFC_TestN7Doc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFC_TESTN7VIEW_H__32B6EEAC_D3C0_4597_8CA1_6568EFEAB869__INCLUDED_)
