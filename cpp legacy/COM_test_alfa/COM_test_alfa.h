// COM_test_alfa.h : main header file for the COM_TEST_ALFA application
//

#if !defined(AFX_COM_TEST_ALFA_H__D75FB09D_A3EB_47FE_80B2_F0F58035BC74__INCLUDED_)
#define AFX_COM_TEST_ALFA_H__D75FB09D_A3EB_47FE_80B2_F0F58035BC74__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CCOM_test_alfaApp:
// See COM_test_alfa.cpp for the implementation of this class
//

class CCOM_test_alfaApp : public CWinApp
{
public:
	CCOM_test_alfaApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCOM_test_alfaApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CCOM_test_alfaApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COM_TEST_ALFA_H__D75FB09D_A3EB_47FE_80B2_F0F58035BC74__INCLUDED_)
