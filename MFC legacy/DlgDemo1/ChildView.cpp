// ChildView.cpp : implementation of the CChildView class
//

#include "stdafx.h"
#include "DlgDemo1.h"
#include "ChildView.h"

#include "OptionsDialog.h"
#include "Math.h"




#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChildView

CChildView::CChildView()
{
}

CChildView::~CChildView()
{
}


BEGIN_MESSAGE_MAP(CChildView,CWnd )
	//{{AFX_MSG_MAP(CChildView)
	ON_WM_PAINT()
	ON_COMMAND(ID_FILE_OPTIONS, OnFileOptions)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CChildView message handlers

BOOL CChildView::PreCreateWindow(CREATESTRUCT& cs) 
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_CLIENTEDGE;
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), HBRUSH(COLOR_WINDOW+1), NULL);

	return TRUE;
}

void CChildView::OnPaint() 
{


    CPaintDC dc (this);
    
    //
    // Initialize the device context.
    //
    dc.SetMapMode(MM_ISOTROPIC);
	dc.SetViewportOrg(60, 420);
	dc.SetWindowExt(10000,10000);
	dc.SetViewportExt(100,-100);
	

	CPen PenBlack(PS_SOLID, 200, RGB(0, 0, 0));
	dc.SelectObject(PenBlack);

	// OSE
	dc.MoveTo(-1000,     0);
	dc.LineTo( 20000000,     0);
	dc.MoveTo(   0, -1000);
	dc.LineTo(   0,  20000000);

	
;   
	
	
	
//KONSTANTE

double PI = 3.14159;
double g = 9.81;

//PROMENE BRZINE I UGLA
int alfa1 = m_Ugao;
int alfa2 = 44;
int alfa3 = 45/2;

long int v1 = 1000;
int v2 = 890;
int v3 = 900;
	
//KORAK I MAKSIMUM
CString string;

dc.SetTextColor(RGB(255, 0, 0));
string.Format (("-Funkcija f1 Ugao %d, Brzina %d m/s "), alfa1, v1);

dc.TextOut (60000,35000 , string);

dc.SetTextColor(RGB(0, 0, 255));
string.Format (("-Funkcija f1 Ugao %d, Brzina %d m/s "), alfa2, v2);
dc.TextOut (60000,33000 , string);

dc.SetTextColor(RGB(0, 255, 0));
string.Format (("-Funkcija f1 Ugao %d, Brzina %d m/s "), alfa3, v3);
dc.TextOut (60000,31000 , string);

double kor = 1;

double max = 150000; //oko milion


// Multiplikator koordinata pozicije teksta
	double kox = 1500;
	double koy = 1000;
/*
    dc.SetTextColor(RGB(255, 0, 0)); 
	dc.TextOut(50*kox, 40*koy, "Funkcija  f1" , 12);
	dc.SetTextColor(RGB(0, 0, 255));
	dc.TextOut(50*kox, 35*koy, "Funkcija  f2" , 12);
	dc.SetTextColor(RGB(0, 255, 0));
	dc.TextOut(50*kox, 30*koy, "Funkcija  f3" , 12);
*/

//Funkcija f1 CRVENA

	for(double i = 1; i < max; i +=kor)
	{ 
        
		double c1 = 1  ;


	    double f = (i*tan((PI*m_Ugao)/180))-((g*i*i)/(2*v1*v1*cos((PI*m_Ugao)/180)*cos((PI*m_Ugao)/180)));
	    dc.SetPixel(i, f, RGB(255, 0, 0));
	}

//Funkcija f2 PLAVA

	for(double i2 = 1; i2 < max; i2 +=kor)
	{
		int c2 = 0;
	    double f2 = (i2*tan((PI*alfa2)/180))-((g*i2*i2)/(2*v2*v2*cos((PI*alfa2)/180)*cos((PI*alfa2)/180)));
			//tan(i2)*i2;
	    dc.SetPixel(i2, f2, RGB(0, 0, 255));
	}

//Funkcija f3 ZELENA

    for(double i3 = 1; i3 < max; i3 +=kor)
	{
		int c3 = 0;
	    double f3 = (i3*tan((PI*alfa3)/180))-((g*i3*i3)/(2*v3*v3*cos((PI*alfa3)/180)*cos((PI*alfa3)/180)));;
	    dc.SetPixel(i3, f3, RGB(0, 255, 0));
	}



//POZICIJA OZNAKA
	
	
	//Koordinate 
	dc.SetTextColor(RGB(100, 100, 0));

    dc.TextOut(56000, 2000, 'X');
	dc.TextOut(500, 35000, 'Y');
   
    //Lenjiri po x i y osi
    CPen PenBlack3(PS_SOLID, 100, RGB(0, 0, 0));
	dc.SelectObject(PenBlack3);
    
	for(int koox = 0; koox < 1000000; koox += 1000)
    {
        dc.MoveTo(koox, 0);
        dc.LineTo(koox, -1000);
    }

	for(int kooy = 0; kooy < 1000000; kooy += 1000)
    {
        dc.MoveTo(0, kooy);
        dc.LineTo(-1000, kooy);
    }



	dc.SelectObject(PenBlack3);
    for(int koox2 = 0; koox2 < 1000000; koox2 += 5000)
    {
        dc.MoveTo(koox2, 0);
        dc.LineTo(koox2, -2000);
    }
	for(int kooy2 = 0; kooy2 < 1000000; kooy2 += 5000)
    {
        dc.MoveTo(0, kooy2);
        dc.LineTo(-2000, kooy2);
    }

	for(int koox3 = 0; koox3 < 1000000; koox3 += 10000)
    {
        dc.MoveTo(koox3, 0);
        dc.LineTo(koox3, -3000);
    }
	for(int kooy3 = 0; kooy3 < 1000000; kooy3 += 10000)
    {
        dc.MoveTo(0, kooy3);
        dc.LineTo(-3000, kooy3);
    }

    dc.SetTextColor(RGB(0, 0, 0));
	for (int kt1= 0; kt1<1000000; kt1+=10000) 
	{
        
        CString string;
        string.Format (("%d km"), (kt1/1000 ) );
        dc.TextOut (kt1, -3000, string);
    }

	dc.SetTextColor(RGB(0, 0, 0));
	for (int kt2= 1000; kt2<1000000; kt2+=10000) 
	{
        
        CString string;
        string.Format (("%dkm"), (((kt2-1000)/1000) ) );
        dc.TextOut (-6000, kt2, string);
    }
	// Do not call CWnd::OnPaint() for painting messages
}


LRESULT CChildView::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CWnd ::DefWindowProc(message, wParam, lParam);
}


void CChildView::OnFileOptions() 
{
	// TODO: Add your command handler code here
	OptionsDialog dlg;

    dlg.m_Ugao = m_Ugao;
    dlg.m_nHeight = m_nHeight;
    dlg.m_nUnits = m_nUnits;

    if (dlg.DoModal () == IDOK) {
        m_Ugao = dlg.m_Ugao;
        m_nHeight = dlg.m_nHeight;
        m_nUnits = dlg.m_nUnits;
        Invalidate ();
    }    
}
