#if !defined(AFX_DGRAFIK_H__CA9CD75D_7E7D_446F_9F52_DBA8DFBC54B3__INCLUDED_)
#define AFX_DGRAFIK_H__CA9CD75D_7E7D_446F_9F52_DBA8DFBC54B3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DGrafik.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDGrafik dialog

class CDGrafik : public CDialog
{
// Construction
public:
	CDGrafik(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDGrafik)
	enum { IDD = IDD_DGRAFIK };
	int		m_DGrafik;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDGrafik)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDGrafik)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DGRAFIK_H__CA9CD75D_7E7D_446F_9F52_DBA8DFBC54B3__INCLUDED_)
