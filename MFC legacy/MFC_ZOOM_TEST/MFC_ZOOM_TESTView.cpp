// MFC_ZOOM_TESTView.cpp : implementation of the CMFC_ZOOM_TESTView class
//

#include "stdafx.h"
#include "MFC_ZOOM_TEST.h"

#include "MFC_ZOOM_TESTDoc.h"
#include "MFC_ZOOM_TESTView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMFC_ZOOM_TESTView

IMPLEMENT_DYNCREATE(CMFC_ZOOM_TESTView, CView)

BEGIN_MESSAGE_MAP(CMFC_ZOOM_TESTView, CView)
	//{{AFX_MSG_MAP(CMFC_ZOOM_TESTView)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMFC_ZOOM_TESTView construction/destruction

CMFC_ZOOM_TESTView::CMFC_ZOOM_TESTView()
{
	// TODO: add construction code here

}

CMFC_ZOOM_TESTView::~CMFC_ZOOM_TESTView()
{
}

BOOL CMFC_ZOOM_TESTView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CMFC_ZOOM_TESTView drawing

void CMFC_ZOOM_TESTView::OnDraw(CDC* pDC)
{
	CMFC_ZOOM_TESTDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	
	// TODO: add draw code for native data here

	srand(0);
	int width = 2000/10;
	int height = 1000/10;
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		pDC->FillSolidRect(CRect(i * width, i * height, 
								 i * width + width, i * height + height), RGB(rand() % 255, rand() % 255, rand() % 255));
	}

	if (m_bSelectMode == TRUE) 
	{
		CPen* pPen = pDC->GetCurrentPen();
		LOGPEN logPen;
		pPen->GetLogPen(&logPen);
		logPen.lopnColor = RGB(133,142,144);
		logPen.lopnStyle = PS_SOLID;

		LOGBRUSH logBrush;
		::ZeroMemory(&logBrush,sizeof(logBrush));
		logBrush.lbStyle = BS_HOLLOW;
		
		CPen newPen;
		newPen.CreatePenIndirect(&logPen);
		CBrush newBrush;
		newBrush.CreateBrushIndirect(&logBrush);
		CPen* oldPen = pDC->SelectObject(&newPen);
		CBrush* oldBrush = pDC->SelectObject(&newBrush);

		pDC->Rectangle(m_rubberBand);

		pDC->SelectObject(oldBrush);
		pDC->SelectObject(oldPen);
	}
}


/////////////////////////////////////////////////////////////////////////////
// CMFC_ZOOM_TESTView diagnostics

#ifdef _DEBUG
void CMFC_ZOOM_TESTView::AssertValid() const
{
	CView::AssertValid();
}

void CMFC_ZOOM_TESTView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMFC_ZOOM_TESTDoc* CMFC_ZOOM_TESTView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMFC_ZOOM_TESTDoc)));
	return (CMFC_ZOOM_TESTDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMFC_ZOOM_TESTView message handlers

void CMFC_ZOOM_TESTView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	if (m_bSelectMode == FALSE) 
	{
		m_bSelectMode = TRUE;
		m_ptStart = point;
	
		m_rubberBand.SetRect(m_ptStart, m_ptStart);
		Invalidate(FALSE);
	}
	
	CView::OnLButtonDown(nFlags, point);
}

void CMFC_ZOOM_TESTView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	m_bSelectMode = FALSE;
	Invalidate(FALSE);

	
	
	
	CView::OnLButtonUp(nFlags, point);
}

void CMFC_ZOOM_TESTView::OnMouseMove(UINT nFlags, CPoint point) 
{
	if (m_bSelectMode != FALSE) 
	{
		
		m_rubberBand.SetRect(m_ptStart, point);
		m_rubberBand.NormalizeRect();
	}
	
	Invalidate(FALSE);
	
	CView::OnMouseMove(nFlags, point);
}
