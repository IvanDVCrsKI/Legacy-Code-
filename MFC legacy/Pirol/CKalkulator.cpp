// CKalkulator.cpp : implementation file
//

#include "stdafx.h"
#include "Pirol.h"
#include "CKalkulator.h"


#include <string>
 #include <iostream>



#include <mmsystem.h>
#pragma comment(lib,"winmm.lib")

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCKalkulator dialog


CCKalkulator::CCKalkulator(CWnd* pParent /*=NULL*/)
	: CDialog(CCKalkulator::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCKalkulator)
	add1 = 0.0;
	add2 = 0.0;
	sum = 0.0;
	m_cstring = _T("");
	//}}AFX_DATA_INIT
}


void CCKalkulator::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCKalkulator)
	DDX_Control(pDX, IDC_HISTORY, lista);
	DDX_Text(pDX, IDC_ADD1, add1);
	DDV_MinMaxDouble(pDX, add1, -100000000., 100000000.);
	DDX_Text(pDX, IDC_ADD2, add2);
	DDV_MinMaxDouble(pDX, add2, -100000000., 100000000.);
	DDX_Text(pDX, IDC_SUM, sum);
	DDX_Text(pDX, IDC_ZNAK, m_cstring);
	DDV_MaxChars(pDX, m_cstring, 1);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCKalkulator, CDialog)
	//{{AFX_MSG_MAP(CCKalkulator)
	ON_BN_CLICKED(IDC_BUTTON_ADD, OnButtonAdd)
	ON_BN_CLICKED(IDC_CMD, OnCmd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCKalkulator message handlers

void CCKalkulator::OnButtonAdd() 
{
	PlaySound(_T("Type.wav"),NULL,SND_ASYNC);

	this->lista.ResetContent();//resetuj listu klikom na dugme
	if (UpdateData(TRUE))
	{
	
		UpdateData(FALSE);
       
int sw1;//swich 1 sluzi da menja slucaj u swich-u switch(sw1) cime se menja i znak u stringu znak[]

if (m_cstring == '-') //Ulaz je jednak char -
{
	sum = add1-add2; //Izlaz sum
	sw1 = 0;         //promeni slucaj u swich-u na -
}

if (m_cstring == '+')//Ulaz je jednak char +
{
	sum = add1+add2; //Izlaz sum
	sw1 = 1;//promeni slucaj u swich-u na +
}

if (m_cstring == '/')//Ulaz je jednak char /
{
	sum = add1/add2; //Izlaz sum
	sw1 = 2;//promeni slucaj u swich-u na /
}

if (m_cstring == '*')//Ulaz je jednak char *
{
	sum = add1*add2; //Izlaz sum
	sw1 = 3;//promeni slucaj u swich-u na *
}


char ch1[20];//BITNO VELICINA ULAZNOG BROJA JE DEFINISANA AKO PREDJE 20 CIFARA == CRASH !!!
sprintf(ch1,"%2f",add1);//pretvori double add1 u string ch1


char stringVar[200] = "";//Prazan string ""  //	MAKSIMUM BROJEVA U EDIT BOXU > == CRASH !!!

strcat(stringVar, ch1);//spoji stringVar sa ch1 // sto daje Izlaz ""add1  


//primer strcat---------------------
/*
char stringVar[20] = "The rain";
strcat(stringVar, "in Spain");

Izlaz je "The rainin Spain"

[Definicija]

-strcat(Target_String_Var, Src_String)

Concatenates the C-string value Src_String onto the end of the 
C-string in the C-string variable Target_String_Var.

*/

/**/

char znak[2] ="E";//znak koji se swicuje pomocu sw1

switch (sw1)//int sw1 se menja
	
	{
    case 0://ako je vrednost sw1 = 0
znak[0] = '-';//promeni prvi(napomena za offset zero!) znak u stringu na -
	break;

	case 1:
znak[0] = '+';
    break;

	case 2:
znak[0] = '/';
    break;

	case 3:
znak[0] = '*';
    break;

	default:
	
	break;
	}
/**/
	//UpdateData(TRUE);

strcat(stringVar, znak);//spoji predhodni stringVar sa znak // sto daje Izlaz ""add1znak 

char ch2[20];
sprintf(ch2,"%2f",add2);//pretvori double add2 u string ch2
strcat(stringVar, ch2);//spoji predhodni stringVar sa ch2 // sto daje Izlaz ""add1znakadd2 

char jednakoa[]="=";
strcat(stringVar, jednakoa);//spoji predhodni stringVar sa "=" // sto daje Izlaz ""add1znakadd2=

char ch3[40];
sprintf(ch3,"%2f",sum);//pretvori double sum u string ch3
strcat(stringVar, ch3);//spoji predhodni stringVar sa ch3 // sto daje Izlaz ""add1znakadd2=sum 





this->lista.AddString(stringVar);//ubaci u datu listu konacni spoj stringVar



//this->lista.AddString(zz);
//this->lista.ResetContent();
	Invalidate();
	}
	
}

/*
   char letter1 = 'A', letter2 = 'Z', result = 0;
   result = letter1 & letter2;
   
//		char ch5 = ch1&ch2&ch3;
//CString alfa;
int a = 'A';//65
char c;
c = 'A' + 7;   // Adds 7 to the ASCII character = H 65+7=72

char Name[10] = "Ted Jones";
Name[1] = 'A';//TAd Jones 

int af = 5;
_strset(Name,af);

char plus[]="+";

char minus[]="-";
char puta[]="*";
char podeljno[]="/";



  CString sx;
	sx.Format("x = %d, y = %d",x,y);
	dc.TextOut(0,0,sx);


  */

void CCKalkulator::OnCmd() 
{
	char f; 



}
