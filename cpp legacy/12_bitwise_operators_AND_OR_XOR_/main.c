#include <stdio.h>
#include <stdlib.h>

int main()
{
    int a,b;
    a=59; //111011
    b=42; //101010

    printf("Bitwise AND is:%d",a&b);
    /*
    111011
    101010
    -------AND a&b 1&1=1 1&0=0 0&0=0
    101010
    */
    printf("\nBitwise OR is:%d",a|b);
    /*
    111011
    101010
    -------OR a|b 1|1=1 1|0=1 0|0=0
    111011
    */
     printf("\nBitwise XOR is:%d",a^b);
    /*
    111011
    101010
    -------XOR a^b 1^1=0 1^0=1 0^0=0 if bits are same print 0 otherwise print 1
    010001
    */
    return 0;
}
