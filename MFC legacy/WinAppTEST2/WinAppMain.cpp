#include <windows.h>
#include "resource.h"
bool gbDrawLine     = false;
bool gbDrawEllipse  = false;
LPCTSTR ClsName = "BasicApp";
LPCTSTR WndName = "URAAAAAAAAAAAAAAAAAAAAAAAAAAA";

LRESULT CALLBACK WndProcedure(HWND hWnd, UINT uMsg,
			      WPARAM wParam, LPARAM lParam);

INT WINAPI WinMain(HINSTANCE hInstance, 
				   HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine, 
			       int nCmdShow)

{
	MSG        Msg;
	HWND       hWnd;
	WNDCLASSEX WndClsEx;
// Create the application window
	WndClsEx.cbSize        = sizeof(WNDCLASSEX);                     //1)
	WndClsEx.style         = CS_HREDRAW | CS_VREDRAW;                //2)
	WndClsEx.lpfnWndProc   = WndProcedure;                           //3)
	WndClsEx.cbClsExtra    = 0;                                      //4)
	WndClsEx.cbWndExtra    = 0;                                      //5)
	WndClsEx.hIcon         = LoadIcon(NULL, IDI_APPLICATION);        //6)
	WndClsEx.hCursor       = LoadCursor(hInstance,IDC_ARROW);       //7)
	WndClsEx.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);  //8)
	WndClsEx.lpszMenuName  = MAKEINTRESOURCE(IDR_MENU1);                                   //9)
	WndClsEx.lpszClassName = ClsName;                                //10)
	WndClsEx.hInstance     = hInstance;                              //11)
	WndClsEx.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);        //12)-6)
// Register the application
	RegisterClassEx(&WndClsEx);
// Create the window object 
	hWnd = CreateWindow(              // hWnd = CreateWindow(. . .);
ClsName,                        //1)name of class
WndName,                        //2)name of window "Ura"
WS_OVERLAPPEDWINDOW,            //3)style
CW_USEDEFAULT,                  //4)distance of window from left side of screen
CW_USEDEFAULT,                  //5)distance of window from top of screen
CW_USEDEFAULT,                  //6)width of window
 CW_USEDEFAULT,                  //7)height of window
NULL,                           //8)handle to parent of this window
 NULL,                           //9)handle to menu
hInstance,                      //10)handle to instance of application
NULL);                          //11)window creation data
// Find out if the window was created
	if( !hWnd ) // If the window was not created,
		return 0; // stop the application

	// Display the window to the user
	ShowWindow(hWnd, SW_SHOWNORMAL);
	UpdateWindow(hWnd);

	// Decode and treat the messages
	// as long as the application is running
	while( GetMessage(&Msg, NULL, 0, 0) )   // ili while(GetMessage(&Msg, NULL, 0, 0) > 0)
	{
             TranslateMessage(&Msg);
             DispatchMessage(&Msg);
	}

	return Msg.wParam;
}

LRESULT CALLBACK WndProcedure(HWND hWnd, UINT Msg,
			   WPARAM wParam, LPARAM lParam)
{
	HDC hDC;
    PAINTSTRUCT Ps;

    switch(Msg)
    {
    // If the user wants to close the application
    
	case WM_DESTROY:
        // then close it
        PostQuitMessage(WM_QUIT);
        break;

	case WM_PAINT:
	    
        break;
	
	case WM_COMMAND:// komanda da bi se koristio meni
		{
	switch(LOWORD(wParam))
		{
	      case IDM_OBJEKAT_LINIJA:
          gbDrawLine = !gbDrawLine;
          InvalidateRect(hWnd, 0, TRUE);
           break;

		  case IDM_OBJEKAT_ELIPSA:
          gbDrawEllipse = !gbDrawEllipse;
          InvalidateRect(hWnd, 0, TRUE);
          break;
		}
		}
	
    default:
        // Process the left-over messages
        return DefWindowProc(hWnd, Msg, wParam, lParam);
    }
	HPEN hPenOld;

if (gbDrawLine) {
    // Draw a red line
    HPEN hLinePen;
    COLORREF qLineColor;
    qLineColor = RGB(255, 0, 0);
    hLinePen = CreatePen(PS_SOLID, 7, qLineColor);
    hPenOld = (HPEN)SelectObject(hDC, hLinePen);

    MoveToEx(hDC, 100, 100, NULL);
    LineTo(hDC, 500, 250);

    SelectObject(hDC, hPenOld);
    DeleteObject(hLinePen);
}

if (gbDrawEllipse) {
    // Draw a blue ellipse
    HPEN hEllipsePen;
    COLORREF qEllipseColor;
    qEllipseColor = RGB(0, 0, 255);
    hEllipsePen = CreatePen(PS_SOLID, 3, qEllipseColor);
    hPenOld = (HPEN)SelectObject(hDC, hEllipsePen);

    Arc(hDC, 100, 100, 500, 250, 0, 0, 0, 0);

    SelectObject(hDC, hPenOld);
    DeleteObject(hEllipsePen);
}
    // If something was not done, let it go
    return 0;
}