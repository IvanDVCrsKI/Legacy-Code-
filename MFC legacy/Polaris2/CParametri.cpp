// CParametri.cpp : implementation file
//

#include "stdafx.h"
#include "Polaris.h"
#include "CParametri.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCParametri dialog


CCParametri::CCParametri(CWnd* pParent /*=NULL*/)
	: CDialog(CCParametri::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCParametri)
	m_Duz2 = 0.0;
	m_MasaR = 0.0;
	m_MasaT = 0.0;
	m_Valfa = 0.0;
	m_Akx = 0.0;
	m_Aky = 0.0;
	//}}AFX_DATA_INIT
}


void CCParametri::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCParametri)
	DDX_Text(pDX, IDC_EDIT_D2, m_Duz2);
	DDX_Text(pDX, IDC_EDIT_M1, m_MasaR);
	DDX_Text(pDX, IDC_EDIT_MT, m_MasaT);
	DDX_Text(pDX, IDC_EDIT_V_ALFA1, m_Valfa);
	DDX_Text(pDX, IDC_EDIT_AKY, m_Akx);
	DDV_MinMaxDouble(pDX, m_Akx, 0., 5000.);
	DDX_Text(pDX, IDC_EDIT_D1, m_Aky);
	DDV_MinMaxDouble(pDX, m_Aky, 0., 5000.);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCParametri, CDialog)
	//{{AFX_MSG_MAP(CCParametri)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCParametri message handlers
