// CMtablica.cpp : implementation file
//

#include "stdafx.h"
#include "cKOR.h"
#include "CMtablica.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCMtablica dialog


CCMtablica::CCMtablica(CWnd* pParent /*=NULL*/)
	: CDialog(CCMtablica::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCMtablica)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	
}


void CCMtablica::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCMtablica)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCMtablica, CDialog)
	//{{AFX_MSG_MAP(CCMtablica)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCMtablica message handlers

void CCMtablica::OnCancel() 
{
	DestroyWindow ();
}

void CCMtablica::OnOK() 
{
		UpdateData(true);
	
}
BOOL CCMtablica::Create()
{
	return CDialog::Create(CCMtablica::IDD);
}
