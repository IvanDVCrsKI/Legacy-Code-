// COMtestAlfaDoc.h : interface of the CCOMtestAlfaDoc class
//


#pragma once


class CCOMtestAlfaDoc : public CDocument
{
protected: // create from serialization only
	CCOMtestAlfaDoc();
	DECLARE_DYNCREATE(CCOMtestAlfaDoc)

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementation
public:
	virtual ~CCOMtestAlfaDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
};


