// PANDA.cpp : implementation file
//

#include "stdafx.h"
#include "MFC_TestN7.h"
#include "PANDA.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPANDA dialog


CPANDA::CPANDA(CWnd* pParent /*=NULL*/)
	: CDialog(CPANDA::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPANDA)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CPANDA::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPANDA)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPANDA, CDialog)
	//{{AFX_MSG_MAP(CPANDA)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPANDA message handlers
