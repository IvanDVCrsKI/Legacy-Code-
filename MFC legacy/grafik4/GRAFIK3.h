// GRAFIK3.h : main header file for the GRAFIK3 application
//

#if !defined(AFX_GRAFIK3_H__C19CF54E_6CA0_4E4A_9A4D_24D41BF7866C__INCLUDED_)
#define AFX_GRAFIK3_H__C19CF54E_6CA0_4E4A_9A4D_24D41BF7866C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK3App:
// See GRAFIK3.cpp for the implementation of this class
//

class CGRAFIK3App : public CWinApp
{
public:
	CGRAFIK3App();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGRAFIK3App)
	public:
	virtual BOOL InitInstance();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CGRAFIK3App)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRAFIK3_H__C19CF54E_6CA0_4E4A_9A4D_24D41BF7866C__INCLUDED_)
