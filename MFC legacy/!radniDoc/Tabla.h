#if !defined(AFX_TABLA_H__4A61C17A_B555_4B29_ADA7_808911DE3EDE__INCLUDED_)
#define AFX_TABLA_H__4A61C17A_B555_4B29_ADA7_808911DE3EDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Tabla.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTabla dialog

class CTabla : public CDialog
{
// Construction
public:
	CTabla(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTabla)
	enum { IDD = IDD_ITABLA };
	int		m_ITabla;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTabla)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTabla)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TABLA_H__4A61C17A_B555_4B29_ADA7_808911DE3EDE__INCLUDED_)
