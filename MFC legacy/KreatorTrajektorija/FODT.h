#if !defined(AFX_FODT_H__0EAB2D42_D317_4715_A217_E005DD1370CD__INCLUDED_)
#define AFX_FODT_H__0EAB2D42_D317_4715_A217_E005DD1370CD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FODT.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFODT dialog

class CFODT : public CDialog
{
// Construction
private:
	CView* m_dlgFODT;
public:
	CFODT(CWnd* pParent = NULL);   // standard constructor
	CFODT(CView* m_dlgFODT);    
    BOOL Create();


// Dialog Data
	//{{AFX_DATA(CFODT)
	enum { IDD = IDD_FODT };
	CString	m_FodT;
	//}}AFX_DATA
    

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFODT)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFODT)
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FODT_H__0EAB2D42_D317_4715_A217_E005DD1370CD__INCLUDED_)
