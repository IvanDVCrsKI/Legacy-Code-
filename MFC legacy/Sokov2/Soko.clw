; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CSokoView
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Soko.h"
LastPage=0

ClassCount=6
Class1=CSokoApp
Class2=CSokoDoc
Class3=CSokoView
Class4=CMainFrame

ResourceCount=4
Resource1=IDD_ABOUTBOX
Resource2=IDR_CURSOR_MENU
Class5=CChildFrame
Class6=CAboutDlg
Resource3=IDR_SOKOTYPE
Resource4=IDR_MAINFRAME

[CLS:CSokoApp]
Type=0
HeaderFile=Soko.h
ImplementationFile=Soko.cpp
Filter=N
BaseClass=CWinApp
VirtualFilter=AC
LastObject=CSokoApp

[CLS:CSokoDoc]
Type=0
HeaderFile=SokoDoc.h
ImplementationFile=SokoDoc.cpp
Filter=N
LastObject=CSokoDoc
BaseClass=CDocument
VirtualFilter=DC

[CLS:CSokoView]
Type=0
HeaderFile=SokoView.h
ImplementationFile=SokoView.cpp
Filter=C
LastObject=CSokoView
BaseClass=CScrollView
VirtualFilter=VWC


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T
BaseClass=CMDIFrameWnd
VirtualFilter=fWC
LastObject=CMainFrame


[CLS:CChildFrame]
Type=0
HeaderFile=ChildFrm.h
ImplementationFile=ChildFrm.cpp
Filter=M
BaseClass=CMDIChildWnd
VirtualFilter=mfWC
LastObject=CChildFrame


[CLS:CAboutDlg]
Type=0
HeaderFile=Soko.cpp
ImplementationFile=Soko.cpp
Filter=D
LastObject=CAboutDlg

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_MRU_FILE1
Command4=ID_APP_EXIT
Command5=ID_APP_ABOUT
CommandCount=5

[MNU:IDR_SOKOTYPE]
Type=1
Class=CSokoView
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_CLOSE
Command4=ID_FILE_SAVE
Command5=ID_FILE_SAVE_AS
Command6=ID_FILE_MRU_FILE1
Command7=ID_APP_EXIT
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_WINDOW_NEW
Command13=ID_WINDOW_CASCADE
Command14=ID_WINDOW_TILE_HORZ
Command15=ID_WINDOW_ARRANGE
Command16=ID_APP_ABOUT
Command17=IDM_DUZ
Command18=IDM_POVRS_P
Command19=IDM_POVRS_K
Command20=ID_BOJA_CRVENA
Command21=ID_BOJA_PLAVA
Command22=ID_BOJA_ZELENA
Command23=ID_BOJA_CRNA
CommandCount=23

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_UNDO
Command5=ID_EDIT_CUT
Command6=ID_EDIT_COPY
Command7=ID_EDIT_PASTE
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_NEXT_PANE
Command13=ID_PREV_PANE
CommandCount=13

[TB:IDR_MAINFRAME]
Type=1
Class=?
Command1=IDM_DUZ
Command2=IDM_POVRS_P
Command3=IDM_POVRS_K
Command4=ID_BOJA_CRVENA
Command5=ID_BOJA_ZELENA
Command6=ID_BOJA_PLAVA
Command7=ID_BOJA_CRNA
CommandCount=7

[MNU:IDR_CURSOR_MENU]
Type=1
Class=?
Command1=ID_ELEMENT_DELITE
Command2=ID_ELEMENT_MOVE
Command3=ID_LINE
Command4=ID_NOELEMENT_BLACK
Command5=ID_NOELEMENT_RED
Command6=ID_NOELEMENT_GREEN
Command7=ID_NOELEMENT_BLUE
CommandCount=7

