#if !defined(AFX_UPUTSTVO_H__688B24E3_4D1B_490E_BB83_58AF80313B88__INCLUDED_)
#define AFX_UPUTSTVO_H__688B24E3_4D1B_490E_BB83_58AF80313B88__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Uputstvo.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CUputstvo dialog

class CUputstvo : public CDialog
{
// Construction
public:
	CUputstvo(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CUputstvo)
	enum { IDD = IDD_UPUTSTVO };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUputstvo)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CUputstvo)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UPUTSTVO_H__688B24E3_4D1B_490E_BB83_58AF80313B88__INCLUDED_)
