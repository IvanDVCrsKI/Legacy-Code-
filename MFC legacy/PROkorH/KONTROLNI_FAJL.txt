kapa=  1.395      
kapa_oxi=  1.395      
P_amb=  1.01325 BAR   
T_kom=  1000  K    
V_tanka_kalkulacije=  -9.25596e+061  m3     
ROoxi=  1.799  kg/m3     
D_tank=  70  mm    
de_tank_min=  2  mm    
L_tank=  1638.69  mm    
c_oxi=  274.033  m/s    
pi=  1.01325 BAR     
m_naponZ=  300  N/m3    
ROmaterijala=  2700  kg/m3     
MaxP_tank=  42.8571 BAR     
stepenS=  4      
V_m_tank=  0.00630642  m3     
V_tank=  6.30642e+006  mm3     

cp_oxi=  0.7556  J/KgK    
cv_oxi=  0.5667  J/KgK    

R_oxi=  188.9  J/KgK    

m_oxi=  0.559868  kg     

T_oxi=  298.15  K    

P_oxi=  50 BAR   

L_tankodPV=  4.68307  m    

-------------------------------- Performanse ze(a)ljene   --------------------------------      

dP=  0.0506625   -->Neizentropsko (udarni talasi)  <  0.582  < Izentropsko    
P_kom=  20 BAR   
Pkr=  10.5825 BAR   
Rgg=  260  J/KgK    
T_kom=  1000  K    
G[korenizkapa*koefKAPA]=  0.683886      
Cf_Adaptiran=  1.37251      
CZZ=  745.595      

Precnik_u_kpp=  20  mm    

mpp[G*(poAkr/korenRTo)]=  0.842707  Kg/s    

 Alfa=  12     
 L_ml=  44.4734  mm   

EE=  2.89062      
Diz_precnik=  57.8125  mm    

Vi=  1023.34  m/s    
Vkr=  550.346  m/s    
Ti=  429.761  K    
Tkr=  835.073  K    

Isp=  1023.34  Ns    
Fp=  862.374  N    

Fp=  87.93  Kg    

