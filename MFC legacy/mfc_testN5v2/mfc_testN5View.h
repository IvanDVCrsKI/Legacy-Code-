// mfc_testN5View.h : interface of the CMfc_testN5View class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MFC_TESTN5VIEW_H__6583AC5A_E36E_481D_BACA_2CE1EE895DF2__INCLUDED_)
#define AFX_MFC_TESTN5VIEW_H__6583AC5A_E36E_481D_BACA_2CE1EE895DF2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMfc_testN5View : public CView
{
protected: // create from serialization only
	CMfc_testN5View();
	DECLARE_DYNCREATE(CMfc_testN5View)

// Attributes
public:
	CMfc_testN5Doc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMfc_testN5View)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	
	
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMfc_testN5View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
// Generated message map functions
protected:
	int m_Ugao;
	int m_Brzina;
	//{{AFX_MSG(CMfc_testN5View)
	
	afx_msg void OnPomocnidlg();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in mfc_testN5View.cpp
inline CMfc_testN5Doc* CMfc_testN5View::GetDocument()
   { return (CMfc_testN5Doc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFC_TESTN5VIEW_H__6583AC5A_E36E_481D_BACA_2CE1EE895DF2__INCLUDED_)
