// mfc_test10.h : main header file for the MFC_TEST10 application
//

#if !defined(AFX_MFC_TEST10_H__80A37968_A732_4E08_9ED1_A32F904391C5__INCLUDED_)
#define AFX_MFC_TEST10_H__80A37968_A732_4E08_9ED1_A32F904391C5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CMfc_test10App:
// See mfc_test10.cpp for the implementation of this class
//

class CMfc_test10App : public CWinApp
{
public:
	CMfc_test10App();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMfc_test10App)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CMfc_test10App)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFC_TEST10_H__80A37968_A732_4E08_9ED1_A32F904391C5__INCLUDED_)
