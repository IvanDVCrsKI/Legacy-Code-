// pointerscope.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;


void funkcija (int x);      // deklarisemo dve funkcije 1 i 2  
void funkcija2 (int * ptr); //u 1 je klasicna f-ja dok je druga operise sa pointerima






int main()
{


int var = 0;  // zadajemo vrednost (nije bitan naziv ni broj)


cout << "vrednost varijable var je: " << var <<endl;

int * pointer = &var; //adresu varijable var prepisujemo pointeru

cout  <<"pointer sada ima vrednost: " << *pointer <<endl; // opreatorom * koji stoji ispred pointera, pointer preuzima celu-
                                                          // -vrednost varijable tako da je isti kao i int var = 10 -
                                                          // -(kao da je tako napisan) sa razlikom sto ga mozemo sada koristiti
                                                          // -izvan granica skopa tj vidljivosti
funkcija(var);

cout <<"nakon prve funkcije pointer je: "<< *pointer << endl;
//funkcija2(& var);
funkcija2(pointer);

cout <<"nakon druge funkcije: "<< *pointer <<" =>pointer je pomocu druge f-je izbegao pravilo (skopa)"<< endl;


//funkcija2(& var); bez suvisnog pretvaranja jer ce  c++ prepoznati &var kao *pointer


	return 0;
}



void funkcija(int x) //definicija f-je 1
{
	x++;
}

void funkcija2(int * ptr) //definicija f-je 2
{
	(*ptr)++;            //*ptr tako u funkciji postaje varijabla (napomena ptr je u zagradi zbog redosleda operacije da ne bi doslo do promene adrese)
}

