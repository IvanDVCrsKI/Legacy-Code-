// MFC_TVer3Doc.cpp : implementation of the CMFC_TVer3Doc class
//

#include "stdafx.h"
#include "MFC_TVer3.h"

#include "MFC_TVer3Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMFC_TVer3Doc

IMPLEMENT_DYNCREATE(CMFC_TVer3Doc, CDocument)

BEGIN_MESSAGE_MAP(CMFC_TVer3Doc, CDocument)
	//{{AFX_MSG_MAP(CMFC_TVer3Doc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMFC_TVer3Doc construction/destruction

CMFC_TVer3Doc::CMFC_TVer3Doc()
{
	// TODO: add one-time construction code here

}

CMFC_TVer3Doc::~CMFC_TVer3Doc()
{
}

BOOL CMFC_TVer3Doc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CMFC_TVer3Doc serialization

void CMFC_TVer3Doc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMFC_TVer3Doc diagnostics

#ifdef _DEBUG
void CMFC_TVer3Doc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMFC_TVer3Doc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMFC_TVer3Doc commands
