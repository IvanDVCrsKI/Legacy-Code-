// SkrolTest.h : main header file for the SKROLTEST application
//

#if !defined(AFX_SKROLTEST_H__FC7F0A45_B647_46AC_9A16_6272FEA7C8DD__INCLUDED_)
#define AFX_SKROLTEST_H__FC7F0A45_B647_46AC_9A16_6272FEA7C8DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CSkrolTestApp:
// See SkrolTest.cpp for the implementation of this class
//

class CSkrolTestApp : public CWinApp
{
public:
	CSkrolTestApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSkrolTestApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CSkrolTestApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SKROLTEST_H__FC7F0A45_B647_46AC_9A16_6272FEA7C8DD__INCLUDED_)
