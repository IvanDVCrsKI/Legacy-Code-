// Polaris.h : main header file for the POLARIS application
//

#if !defined(AFX_POLARIS_H__ED94C0DB_7CA5_4888_ABA4_62A9193A0F13__INCLUDED_)
#define AFX_POLARIS_H__ED94C0DB_7CA5_4888_ABA4_62A9193A0F13__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CPolarisApp:
// See Polaris.cpp for the implementation of this class
//

class CPolarisApp : public CWinApp
{
public:
	CPolarisApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPolarisApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CPolarisApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_POLARIS_H__ED94C0DB_7CA5_4888_ABA4_62A9193A0F13__INCLUDED_)
