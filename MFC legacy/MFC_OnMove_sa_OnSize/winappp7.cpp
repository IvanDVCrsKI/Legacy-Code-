#include <afxwin.h>

class CMainFrame : public CFrameWnd
{
public:
	CMainFrame ();

protected:
	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMove(int x, int y);
	DECLARE_MESSAGE_MAP()
};

CMainFrame::CMainFrame()
{
	// Create the window's frame
	Create(NULL, "Windows Application", WS_OVERLAPPEDWINDOW,
	       CRect(120, 100, 700, 480), NULL);
}

class CExerciseApp: public CWinApp
{
public:
	BOOL InitInstance();
};

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_SHOWWINDOW()
	ON_WM_ACTIVATE()
	ON_WM_PAINT()
	ON_WM_SIZE()
	ON_WM_MOVE()
END_MESSAGE_MAP()

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	return 0;
}

void CMainFrame::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CFrameWnd::OnShowWindow(bShow, nStatus);
	
	// TODO: Add your message handler code here
	//ShowWindow(SW_SHOW);
}

void CMainFrame::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized) 
{
	CFrameWnd::OnActivate(nState, pWndOther, bMinimized);
	
	// TODO: Add your message handler code here
	switch( nState )
	{
	case WA_ACTIVE:
		SetWindowText("This window has been activated, without the mouse!");
		break;
	case WA_INACTIVE:
		SetWindowText("This window has been deactivated and cannot be changed now!!");
		break;
	case WA_CLICKACTIVE:
		SetWindowText("This window has been activated using the mouse!!!");
		break;
	}	
}

void CMainFrame::OnPaint() 
{
	CFrameWnd::OnPaint();

	SetWindowText("The window has been painted<==>");
}

void CMainFrame::OnSize(UINT nType, int cx, int cy) 
{
/*	CMainFrame::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	char *MsgToShow = new char[20];
	char *MsgCoord  = new char[20];
	
	switch(nType)
	{
	case SIZE_MINIMIZED:
		strcpy(MsgToShow, "Minimized ");
		break;
	case SIZE_MAXIMIZED:
		strcpy(MsgToShow, "Maximized ");
		break;
	case SIZE_RESTORED:
		strcpy(MsgToShow, "Restored ");
		break;
	case SIZE_MAXHIDE:
		strcpy(MsgToShow, "Maximized Not Me ");
		break;
	case SIZE_MAXSHOW:
		strcpy(MsgToShow, "Restored Not Me ");
		break;
	}

	sprintf(MsgCoord, "Left = %d | Top = %d", cx, cy);
	strcat(MsgToShow, MsgCoord);
	SetWindowText(MsgToShow);
*/
}

void CMainFrame::OnMove(int x, int y) 
{
	CFrameWnd::OnMove(x, y);
	
	// TODO: Add your message handler code here
	char *MsgCoord  = new char[20];

	sprintf(MsgCoord, "X = %d |Y= %d", x, y);
	
	SetWindowText(MsgCoord);
}

BOOL CExerciseApp::InitInstance()
{
	m_pMainWnd = new CMainFrame ;
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();

	return TRUE;
}

CExerciseApp theApp;