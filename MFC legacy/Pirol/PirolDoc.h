// PirolDoc.h : interface of the CPirolDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_PIROLDOC_H__C65B8A08_EB08_46CB_80A4_E28435FDB451__INCLUDED_)
#define AFX_PIROLDOC_H__C65B8A08_EB08_46CB_80A4_E28435FDB451__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CPirolDoc : public CDocument
{
protected: // create from serialization only
	CPirolDoc();
	DECLARE_DYNCREATE(CPirolDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPirolDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CPirolDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CPirolDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PIROLDOC_H__C65B8A08_EB08_46CB_80A4_E28435FDB451__INCLUDED_)
