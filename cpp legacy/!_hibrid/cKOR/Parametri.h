#if !defined(AFX_PARAMETRI_H__84D1D28C_5514_490B_92DF_655308451C9D__INCLUDED_)
#define AFX_PARAMETRI_H__84D1D28C_5514_490B_92DF_655308451C9D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Parametri.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CParametri dialog

class CParametri : public CDialog
{
// Construction
public:
	CParametri(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CParametri)
	enum { IDD = IDD_PARAMETRI };
	double	m_alfa;
	double	m_beta;
	double	m_DDI;
	double	m_DKO;
	double	m_RKR;
	double	m_DKR;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CParametri)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CParametri)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PARAMETRI_H__84D1D28C_5514_490B_92DF_655308451C9D__INCLUDED_)
