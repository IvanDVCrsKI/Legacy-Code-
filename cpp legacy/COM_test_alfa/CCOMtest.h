#if !defined(AFX_CCOMTEST_H__A7B7FDDE_9654_4FA6_B13E_1F0483B9FE0E__INCLUDED_)
#define AFX_CCOMTEST_H__A7B7FDDE_9654_4FA6_B13E_1F0483B9FE0E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CCOMtest.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCCOMtest dialog

class CCCOMtest : public CDialog
{
// Construction
public:
	CCCOMtest(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCCOMtest)
	enum { IDD = IDD_DIALOG1 };
	int		m_data;
	CString	m_status;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCCOMtest)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCCOMtest)
	afx_msg void OnButton1();
	afx_msg void OnButton2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CCOMTEST_H__A7B7FDDE_9654_4FA6_B13E_1F0483B9FE0E__INCLUDED_)
