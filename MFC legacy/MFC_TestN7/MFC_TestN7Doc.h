// MFC_TestN7Doc.h : interface of the CMFC_TestN7Doc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MFC_TESTN7DOC_H__97C28F60_F618_4F59_ADE3_9D8881FD9563__INCLUDED_)
#define AFX_MFC_TESTN7DOC_H__97C28F60_F618_4F59_ADE3_9D8881FD9563__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMFC_TestN7Doc : public CDocument
{
protected: // create from serialization only
	CMFC_TestN7Doc();
	DECLARE_DYNCREATE(CMFC_TestN7Doc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMFC_TestN7Doc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMFC_TestN7Doc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMFC_TestN7Doc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFC_TESTN7DOC_H__97C28F60_F618_4F59_ADE3_9D8881FD9563__INCLUDED_)
