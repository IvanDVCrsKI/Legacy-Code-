// KreatorTrajektorijaMDIDoc.cpp : implementation of the CKreatorTrajektorijaMDIDoc class
//

#include "stdafx.h"
#include "KreatorTrajektorijaMDI.h"

#include "KreatorTrajektorijaMDIDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaMDIDoc

IMPLEMENT_DYNCREATE(CKreatorTrajektorijaMDIDoc, CDocument)

BEGIN_MESSAGE_MAP(CKreatorTrajektorijaMDIDoc, CDocument)
	//{{AFX_MSG_MAP(CKreatorTrajektorijaMDIDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaMDIDoc construction/destruction

CKreatorTrajektorijaMDIDoc::CKreatorTrajektorijaMDIDoc()
{
	// TODO: add one-time construction code here

}

CKreatorTrajektorijaMDIDoc::~CKreatorTrajektorijaMDIDoc()
{
}

BOOL CKreatorTrajektorijaMDIDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaMDIDoc serialization

void CKreatorTrajektorijaMDIDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaMDIDoc diagnostics

#ifdef _DEBUG
void CKreatorTrajektorijaMDIDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CKreatorTrajektorijaMDIDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaMDIDoc commands
