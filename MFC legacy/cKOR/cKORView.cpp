// cKORView.cpp : implementation of the CCKORView class
//

#include "stdafx.h"
#include "cKOR.h"

#include "Math.h"
#include <stdlib.h> //Needed for "exit" function
#include <fstream.h>// data stream


//DIJALOZI
#include "Parametri.h"
#include "CAutorA.h"
#include "CMasaCevi.h"
#include "CMtablica.h"
#include "C1napon.h"
#include "C3napon.h"
#include "CTankozid.h"
#include "CDeformacije.h"
#include "CRMosnovno.h"


//PERFORMANSE_1 PRORACUN
#include "Performanse_1.h"
#include "CRMM.h"
#include "GeoML.h"
#include "MAR.h"


#include "SaintRobert.h"
#include "PODT.h"



#include "cKORDoc.h"
#include "cKORView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCKORView

IMPLEMENT_DYNCREATE(CCKORView, CView)

BEGIN_MESSAGE_MAP(CCKORView, CView)
	//{{AFX_MSG_MAP(CCKORView)
	ON_WM_KEYDOWN()
	ON_COMMAND(IDM_PAR, OnPar)
	ON_COMMAND(IDM_MASACENA, OnMasacena)
	ON_COMMAND(IDM_AUTOR, OnAutor)
	ON_COMMAND(IDM_MTABLICA, OnMtablica)
	ON_COMMAND(IDM_1NAPON, On1napon)
	ON_COMMAND(IDM_3NAPON, On3napon)
	ON_COMMAND(IDM_TANKOZIDNE, OnTankozidne)
	ON_COMMAND(IDM_TANKOZID, OnTankozid)
	ON_COMMAND(IDM_RMO, OnRmo)
	ON_COMMAND(IDM_TEST_SAVE, OnTestSave)
	ON_COMMAND(IDM_TEST_OPEN, OnTestOpen)
	ON_COMMAND(IDM_EXE1, OnExe1)
	ON_COMMAND(IDM_KT, OnKt)
	ON_COMMAND(IDM_POLARIS, OnPolaris)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCKORView construction/destruction

CCKORView::CCKORView()
{
	m_Zumx = 8070;//8070
	m_Zumy = 8070;
	m_beta = 30;
	m_alfa = 30;
	m_DKO = 500;
	m_DDI = 500;
	m_RKR = 500;
	m_DKR = 500;
	PTX = 1500;
	PTY = 1500;


	m_poason = 0.33; //POASON
	m_modulE = 70000000000; // MODUL ELASTICNOSTI Pa N/mm^2

    m_Rsp = 50; //mm
	m_Run = 48;  //mm
	m_Lduz = 2000; //mm
	m_ROg = 2700;   //kg po m^3
	//m_Umasa = 0.0;  //kg
	m_CenapoKg = 825; //valuta
	//m_CenaUkupno = 0.0;
	m_naponZ = 300; //zarezna cvrstoca N/mm2
	m_Pmax = 0.0;//pmax ODUZETI 10%
	
	m_rsp = m_Rsp;
	m_run = m_Run;
	m_debljina = 0.0;
	m_odnosr = 0.0;
	m_ntri = 0.0;
	m_nrri = 0.0;
	m_nlri = 0.0;
	m_pPu = m_Pmax; //bara
	m_srednjinn = 0.0;
	m_taumax = 0.0;

	m_Zatezna = m_naponZ;

	m_ena = 1;

	m_zateznaT = 0.0;
	m_kvalitetS = 0;
	m_PuD = 0.0; //pritisak zadat 1
	m_PuD2 = 0.0; //pritisak zadat 2

	m_Dun = 0.0; //precnik unutrasnji 1
	m_Dun2 = 0.0; //precnik unutrasnji 2

	m_Dsp1 = 0.0; //precnik spoljni 1
	m_Dsp2 = 0.0; //precnik spoljni 2
	
	m_DD1 = 0.0;  //debljina data 1
	m_DD2 = 0.0;  //debljina data 2

	m_DP1 = 0.0;  //debljina prpracunata 1
	m_DP2 = 0.0;  //debljina prpracunata 2

	m_PP1 = 0.0;  //pritisak proracunat 1
	m_PP2 = 0.0;  //pritisak proracunat 2

	// Za tankozidne 
	m_rsp2 = 0.0;
	m_run2 = 0.0;
	m_debljina2 = 0.0;
	m_pritisakPi = m_pPu;
	m_hoop = 0.0;
	m_long = 0.0;
	m_radi = 0.0;
	m_zatezna2 = 0.0;
	m_ena2 = 0.0;
	m_odnosDsR = 0.0;
	m_gnn2 = 0.0;
	m_taumax2 = 0.0;

    m_dex =0;
    m_dey =0;
    m_dez=0;
	m_zapreminaVo = 0.0;
	m_dVo = 0.0;
	m_radnaT = 0.0;
	m_defT = 0.0;

	 t_radi =m_radi ;
	 t_long=m_long;
	 t_hoop=m_hoop;
	 t_poason=m_poason;
	 t_modulE =m_modulE;

	 //CCRMosnovno fluid;
	

	m_Po = 0;
	//m_Pi = 0.0;
	m_kapa = 0;//fluid.RMOgetm_kapa();
//	m_Pamb = 0.0;
//	m_To = 0.0;
//	m_Rgg = 0.0;


}

CCKORView::~CCKORView()
{
}

BOOL CCKORView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CCKORView drawing

void CCKORView::OnDraw(CDC* pDC)
{
	CClientDC;
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetViewportOrg(90, 620); //90 -620
	pDC->SetWindowExt(m_Zumx,m_Zumy); //SetWindowExtent(100,100)  SetViewportExtent(200,200)  100 jedinica = 200 pixela
	                                                                                             //1 m = 2 pixela
	
	pDC->SetViewportExt(1000,-1000);//1000  -1000


//--------------------------------------------------------
CPen PenBlack1(PS_SOLID,1,RGB(0,0,0));
pDC->SelectObject(PenBlack1);

	pDC->MoveTo(-1000, 0);
	pDC->LineTo( 200000,0);

	pDC->MoveTo(   0, -1000);
	pDC->LineTo(   0,  200000);

//osnovna skala I


/*
//--------------------------------------------------------
//MREZA
//--------------------------------------------------------
pDC->SelectObject(PenBlack1);
for(int kooxa2 = 0; kooxa2 < 5100; kooxa2 += 500)
    {
        pDC->MoveTo(kooxa2, 0);
        pDC->LineTo(kooxa2, 5000);
	

    }

for(int kooya2 = 0; kooya2 < 5100; kooya2 += 500)
    {
        pDC->MoveTo(0, kooya2);
        pDC->LineTo(5000, kooya2);
	
    }
//--------------------------------------------------------
//MREZA MARGINE
//--------------------------------------------------------
CPen PenBlack20(PS_SOLID,20,RGB(0,0,0));
pDC->SelectObject(PenBlack20);


for(int kooxa3 = 0; kooxa3 < 5001; kooxa3 += 5000)
    {
        pDC->MoveTo(kooxa3, 0);
        pDC->LineTo(kooxa3, 5000);
	

    }

for(int kooya3 = 0; kooya3 < 5000; kooya3 += 5000)
    {
        pDC->MoveTo(0, kooya3);
        pDC->LineTo(5000, kooya3);
	
    }
//--------------------------------------------------------
//TEKST NA LENJIRIMA
//--------------------------------------------------------

pDC->SetTextColor(RGB(0, 0, 0));
pDC->SetBkMode(TRANSPARENT);

//ZA X OSU
	for (int kt1= 0; kt1<=5000; kt1+=500) //kt1<=5000-I  kt1=1000
	{
		        
        CString string;
        string.Format (("%d"), kt1 );
        pDC->TextOut (kt1, -250, string);//kt1+I
    }
//ZA Y OSU
	for (int kt2= 0; kt2<=5000; kt2+=500) 
	{
        
        CString string;
        string.Format (("%d"), kt2 );
        pDC->TextOut (-300, kt2, string);
    }
//--------------------------------------------------------
//PODEOCI NA LENJIRU
//--------------------------------------------------------
	pDC->SelectObject(PenBlack1);
    for(int koox2 = 0; koox2 < 5000; koox2 += 1000)
    {
        pDC->MoveTo(koox2, 0);
        pDC->LineTo(koox2, -250);
	

    }
	for(int kooy2 = 0; kooy2 < 5000; kooy2 += 1000)
    {
        pDC->MoveTo(0, kooy2);
        pDC->LineTo(-250, kooy2);
	
    }

	for(int koox3 = 0; koox3 < 5500; koox3 += (1000)/4)
    {
        pDC->MoveTo(koox3, 0);
        pDC->LineTo(koox3, -100);
	
    }
	for(int kooy3 = 0; kooy3 < 5500; kooy3 += (1000)/4)
    {
        pDC->MoveTo(0, kooy3);
        pDC->LineTo(-100, kooy3);
	
    }
	*/
//----------------------------------------------------------------------------------
//********************GRAFICKI PRIKAZ MLAZNIKA RAKETNOG MOTORA**********************
//----------------------------------------------------------------------------------
double PI = 3.14159265359;
double g = 9.8041;

CPen PenBlue20(PS_SOLID,20,RGB(0,0,255));
pDC->SelectObject(PenBlue20);




double T3X = PTX;
double T3Y = PTY;
double DM1 = m_DKR;


CPoint(PTX,PTY);

pDC->Ellipse(T3X-100, T3Y+100,T3X+100, T3Y-100); 
pDC->MoveTo(T3X,T3Y);     //T3 (X3,Y3)


double T2X = T3X+DM1;     // T2 (X2,Y2)
double T2Y = T3Y;
pDC->LineTo(T2X,T2Y); 


pDC->MoveTo(T3X,T3Y); 


double alfa1 =m_alfa;  

double sin_alfa1;
double cos_alfa1;

sin_alfa1 =sin((PI*alfa1)/180);
cos_alfa1 =cos((PI*alfa1)/180);

double T4X;
double T4Y;

double A;
double B;

double dKKO = m_DKO;
double KKO = -dKKO;

A = sin_alfa1*KKO;
B = cos_alfa1*KKO;

T4X = T3X+B;
T4Y = T3Y+A;
pDC->LineTo(T4X,T4Y);
//-----------------------

pDC->MoveTo(T2X,T2Y);


double beta1 =180-m_beta;

double sin_beta1;
double cos_beta1;

sin_beta1 =sin((PI*beta1)/180);
cos_beta1 =cos((PI*beta1)/180);

double dKDI = m_DDI;
double KDI = -dKDI;

double Q;
double W;

Q = sin_beta1*KDI;
W = cos_beta1*KDI;


double T1X;
double T1Y;


T1X = T2X+W;
T1Y = T2Y+Q;
pDC->LineTo(T1X,T1Y);
//-------------------------------------
//OGLEDALO

double X3O;
double Y3O;

double RM1 = m_RKR;

X3O = T3X;
Y3O = T3Y+RM1;

pDC->MoveTo(X3O,Y3O);
double X2O = T2X;
double Y2O = T2Y+RM1;
pDC->LineTo(X2O,Y2O); 


//---------------------

pDC->MoveTo(X2O,Y2O);

double beta2 = 270-m_beta;
double sin_beta2;
double cos_beta2;

sin_beta2 =sin((PI*beta2)/180);
cos_beta2 =cos((PI*beta2)/180);

double Q1;
double W1;

Q1 = sin_beta2*KDI;
W1 = cos_beta2*KDI;

double X1O = Q1+X2O;
double Y1O = W1+Y2O;
pDC->LineTo(X1O,Y1O); 

//-----------------------------------

pDC->MoveTo(X3O,Y3O);

double alfa2 =90+m_alfa;  

double sin_alfa2;
double cos_alfa2;

sin_alfa2 =sin((PI*alfa2)/180);
cos_alfa2 =cos((PI*alfa2)/180);
double A1;
double B1;

A1= sin_alfa2*KKO;
B1 = cos_alfa2*KKO;

double X4O = A1+X3O;
double Y4O = B1+Y3O;

pDC->LineTo(X4O,Y4O); 
//**********************************************************************************
//----------------------------------------------------------------------------------



//----------------------------------------------------------------------------------
//*********************************POMOCNE LINIJE***********************************
//----------------------------------------------------------------------------------


double ppl1 = 500;
double ppl2 = 1000;


double TPX1 = X4O;
double TPX2 = X1O;
double TPX3 = X4O-ppl1;
double TPX4 = T4X-ppl1;
double TPX5 = X1O+ppl2;
double TPX6 = T1X+ppl2;
double TPY1 = Y4O+ppl1; 
double TPY2 = Y4O+ppl1;
double TPY3 = Y4O;
double TPY4 = T4Y;
double TPY5 = Y1O;
double TPY6 = T1Y;

CPen PenRED5(PS_SOLID,5,RGB(255,0,0));
pDC->SelectObject(PenRED5);

//-----------------------------------
//P LINIJE Lu
//-----------------------------------
pDC->MoveTo(X4O,Y4O); 
pDC->LineTo(TPX1,TPY1); 

pDC->MoveTo(TPX1,TPY1); 
pDC->LineTo(TPX2,TPY2); 

pDC->MoveTo(X1O,Y1O); 
pDC->LineTo(TPX2,TPY2); 
//-----------------------------------





//-----------------------------------
//P LINIJE DIVERGENTNI DEO (LEVI)
//-----------------------------------
pDC->MoveTo(X4O,Y4O); 
pDC->LineTo(TPX3,TPY3); 

pDC->MoveTo(TPX3,TPY3); 
pDC->LineTo(TPX4,TPY4); 

pDC->MoveTo(T4X,T4Y); 
pDC->LineTo(TPX4,TPY4);
//-----------------------------------





//-----------------------------------
//P LINIJE KONVERGENTNI DEO DESNI
//-----------------------------------
pDC->MoveTo(X1O,Y1O); 
pDC->LineTo(TPX5,TPY5); 

pDC->MoveTo(TPX5,TPY5); 
pDC->LineTo(TPX6,TPY6); 

pDC->MoveTo(T1X,T1Y); 
pDC->LineTo(TPX6,TPY6); 
//-----------------------------------


//**********************************************************************************
//----------------------------------------------------------------------------------



//----------------------------------------------------------------------------------
//************************PRORACUN DUZINA i MERA MLAZNIKA***************************
//----------------------------------------------------------------------------------


double Lu = TPX2-TPX1;
double DKO = TPY3-TPY4;
double DDI = TPY6-TPY5;
double test = A;
 

double Akr = (RM1/2)*(RM1/2)*PI;
double Adi = (DKO/2)*(DKO/2)*PI;

double Akonv = (DDI/2)*(DDI/2)*PI;
double Akonv1 = 1;



//**********************************************************************************
//----------------------------------------------------------------------------------



//----------------------------------------------------------------------------------
//************************PRORACUN TEZINE TELA RAKETE***************************
//----------------------------------------------------------------------------------
/*
    m_Rsp = 0.0; //mm
	m_Run = 0.0;  //mm
	m_Lduz = 0.0; //mm
	m_ROg = 0.0;   //kg po m^3
	m_Umasa = 0.0;  //kg
	m_CenapoKg = 0.0; //valuta
	m_CenaUkupno = 0.0;
*/
double Vcevi;
double Acevi;
double VceviM3;
double Konv = 1000000000; //pretvaranje mm3 u m3
double KvZ; //razlika pod zagradom R^2-r^2

KvZ = (m_Rsp*m_Rsp)-(m_Run*m_Run);
Acevi = KvZ*PI;
Vcevi = Acevi*m_Lduz;
VceviM3 = Vcevi/Konv;

m_Umasa = VceviM3*m_ROg;
m_CenaUkupno = m_Umasa*m_CenapoKg;


//**********************************************************************************
//----------------------------------------------------------------------------------

//----------------------------------------------------------------------------------
//************************PRORACUN MAX PRITISKA ZA CEV***************************
//----------------------------------------------------------------------------------
    //m_naponZ = 300; //zarezna cvrstoca daN/mm2
	//m_Pmax = 333;//pmax ODUZETI 10% bar
     //p = 2*d*qdoz/D 

double dzida =(m_Rsp-m_Run)/1000;
double KonvBar = 100000;//pas u bare 10^5
double Konv2 = 1000000; //mm^2 u m^2
double NaponZ = m_naponZ*Konv2;
double Ppas;
Ppas = (2*dzida*NaponZ)/(m_Run/1000);
m_Pmax = Ppas/KonvBar;





//**********************************************************************************
//----------------------------------------------------------------------------------

//----------------------------------------------------------------------------------
//************************PRORACUN NAPONA U CEVI***************************
//----------------------------------------------------------------------------------

//PRIMARNA ANALIZA NAPONA



m_pPu=m_Pmax;
m_rsp = m_Rsp;
m_run = m_Run;
m_Zatezna = m_naponZ;
m_debljina =m_rsp-m_run;
m_odnosr =m_rsp/m_run; // ksi
double ppas = m_pPu*100000;


double ksikv = m_odnosr*m_odnosr;


double Cti = (ksikv+1)/(ksikv-1);
m_ntri = (ppas*Cti)/m_ena;
m_nrri = (-ppas)/m_ena;

double Cli = 1/(ksikv-1); 
m_nlri = (ppas*Cli)/m_ena;

m_srednjinn = ((ppas*Cti)+(ppas*Cli)-ppas)/(3*m_ena);
m_taumax =((-ppas)-(ppas*Cti))/(2*m_ena);
/* 
radijalni       sigma 1
longitudalni    sigma 2
tangencijalni   sigma 3

  tau max = rad-tang/2
*/



//SEKUNDARNA ANALIZA NAPONA


	m_zateznaT = m_Zatezna;
	m_kvalitetS = 1;

	m_PuD = m_PuD2 = ppas; //p dati

	m_Dun =m_Dun2 = 2*m_Run/1000;//precnik unutrasnji 
	 
	m_Dsp1 =m_Dsp2 = 2*m_Rsp/1000; //precnik spoljni 
	 	
	m_DD1 = m_DD2=m_debljina/1000 ;  //debljina data 


//po unutrasnjoj meri

m_DP1 = ((m_PuD*m_Dun)/((m_zateznaT*m_kvalitetS)-(0.6*m_PuD)))*1000;//debljina proracunata 1
m_PP1 = (m_zateznaT*m_kvalitetS*m_DD1)/(m_Dun+(0.6*m_DD1));//pritisak proracunat 1



//po spoljnoj meri
m_DP2 = (m_PuD*m_Dsp1)/((m_zateznaT*m_kvalitetS)-(0.4*m_PuD))*1000;//debljina prpracunata 2
m_PP2 = (m_zateznaT*m_kvalitetS*m_DP1)/(m_Dsp1+(0.4*m_DP1));//pritisak proracunat 2


//m_PuD = m_PuD2 = ppas/100000; //p dati

m_Dun =m_Dun2 = 2*m_Run;//precnik unutrasnji 
	 
m_Dsp1 =m_Dsp2 = 2*m_Rsp; //precnik spoljni 
	 	
m_DD1 = m_DD2 =m_debljina;  //debljina data 


//TANKOZIDNA ANALIZA

	// Za tankozidne 
	m_rsp2 = m_Rsp;
	m_run2 = m_Run;

	m_pritisakPi = ppas;

	m_zatezna2 = m_naponZ;
	m_ena2 = 1;


m_debljina2 = m_rsp2-m_run2;
m_odnosDsR = m_debljina2/m_rsp2;


	m_hoop = (m_pritisakPi*m_run2)/(2*m_debljina2)*m_ena2;
	m_radi = 0;
	m_long = m_hoop/2*m_ena2;

m_pritisakPi = ppas/100000;

	m_gnn2 = (m_hoop+m_radi+m_long)/3*m_ena2;
	m_taumax2 = (m_radi-m_hoop)/2*m_ena2;



//**********************************************************************************
//----------------------------------------------------------------------------------


//----------------------------------------------------------------------------------
//*********************PRORACUN DEFORMACIJA CEVI USLED P I T************************
//----------------------------------------------------------------------------------


/* 
radijalni       sigma N1
longitudalni    sigma N2
tangencijalni   sigma N3 = hoop

  tau max = rad-tang/2


def =1/E (N1-P(N2+N3))
   	      N2 N3 N1
	      N3 N1 N2
*/		  
m_dex =(1/m_modulE)*(m_radi-(m_poason*(m_long+m_hoop)));
m_dey =(1/m_modulE)*(m_long-(m_poason*(m_radi+m_hoop)));
m_dez=(1/m_modulE)*(m_hoop-(m_poason*(m_long+m_radi)));



    
	m_zapreminaVo = Vcevi;
	m_dVo = 0.0;
	m_radnaT = 0.0;
	m_defT = 0.0;


//**********************************************************************************
//----------------------------------------------------------------------------------



//----------------------------------------------------------------------------------
//*********************PERFORMANSE RAKETNOG MOTORA**********************************
//----------------------------------------------------------------------------------


CCRM fluid;
double Pamb = fluid.CCRMgetP_amb();
double Pkom = fluid.CCRMgetP_kom();
double kapa = fluid.CCRMgetKapa();
double Rgg =fluid.CCRMgetRgg();
double To = fluid.CCRMgetT_kom();







double Pizl=fluid.CCRMgetP_izl();
double ROo = fluid.CCRMgetROo();
double ROkr = fluid.CCRMgetROkr();
double ROiz = fluid.CCRMgetROiz();




PERF funkcija;
double EE = funkcija.PERFgetEE(); // YEAH !!!
double CZZ = funkcija.PERFgetCZZ();
double GkaoGornjiMilanovac = funkcija.PERFgetG();
double Cf =funkcija.PERFgetCf_Neadaptiran();
double Isp = funkcija.PERFgetIsp();
double Vizlaz = funkcija.PERFgetVi();
double Vkr = funkcija.PERFgetVkr();
double Tkr =funkcija.PERFgetTkr();
double Ti = funkcija.PERFgetTi();
double Pkr = funkcija.PERFgetPkr();
double mp = funkcija.PERFgetMaseniProtok();
double Fp1 = funkcija.PERFgetPotisak();
double Fp2 = funkcija.PERFgetPotisakII();
double FiMeh = funkcija.PERFgetFi();
double t_rada = funkcija.PERFgetVremeRadaRM();

	

      

	
 

	



 	


/*
fstream myfile;	
myfile.open ("data6.txt",ios::out);
myfile <<""<<Fp1<<"      "<<""<<endl;
Invalidate();

	  
myfile.close();
	*/

GEOML geo;
double dkr = geo.GEOMLgetDkr(); //NAPOMENA POLUPRECNIK r^2 *PI u mm
double Akriticno = geo.GEOMLgetAkr();
double DuzinaL = geo.GEOMLgetL();
double diz = geo.GEOMLgetDiz();
double alfa_pk = geo.GEOMLgetAlfa();

MAR masa;
double MPM = masa.MARgetMp();
double ROpu = masa.MARgetROpu();



STRO par;

double re = par.STROgetMODEL_Lenoir_Robillard();
double r = par.STROgetR();
double ro = par.STROgetRo();
double b = par.STROgetb();
double n = par.STROgetn();

PODT pri;

double sigma2 = pri.PODTgetSIGMApII();


double nam = EE;




//**********************************************************************************
//----------------------------------------------------------------------------------






pDC->SetTextColor(RGB(0,0,0));


char t1[10];
sprintf(t1,"Lu:    %2f  ",Lu);
pDC->TextOut (5200,5000 ,t1);

char t2[10];
sprintf(t2,"DKO:    %2f",DKO );
pDC->TextOut (5200,4800 ,t2);

char t3[10];
sprintf(t3,"DDI:    %2f",-DDI );
pDC->TextOut (5200,4600 ,t3);

char t5[10];
sprintf(t5,"E (odnos Ai/Akr):    %2f",EE);
pDC->TextOut (5200,4200 ,t5);





char t7[10];
sprintf(t7,"Akriticni:    %2f",Akr);
pDC->TextOut (5200,4000 ,t7);

char t8[10];
sprintf(t8,"Adivergentni:    %2f",Adi);
pDC->TextOut (5200,3800 ,t8);

char t9[10];
sprintf(t9,"Akonvergentni:   %2f",Akonv);
pDC->TextOut (5200,3600 ,t9);



char t11[10];
sprintf(t11,"C* brzina isticanja:    %2f m/s",CZZ);
pDC->TextOut (5200,3200 ,t11);

char t12[10];
sprintf(t12,"G(od kapa):    %2f ",GkaoGornjiMilanovac);
pDC->TextOut (5200,3000 ,t12);

char t13[10];
sprintf(t13,"Cf za adaptiran:    %2f",Cf);
pDC->TextOut (5200,2800 ,t13);

char t14[10];
sprintf(t14,"Isp:    %2f Ns/Kg",Isp);
pDC->TextOut (5200,2600 ,t14);
/*
char t15[10];
sprintf(t15,"E odnos:    %2f m/s",EE);
pDC->TextOut (5200,2400 ,t15);
*/
char t22[10];
sprintf(t22,"Vkr kriticno:    %2f m/s",Vkr);
pDC->TextOut (5200,2200 ,t22);

char t23[10];
sprintf(t23,"Vi izlazno:    %2f m/s",Vizlaz);
pDC->TextOut (5200,1800 ,t23);

char t25[10];
sprintf(t25,"Tkr kriticno:    %2f K",Tkr);
pDC->TextOut (5200,1600 ,t25);

char t26[10];
sprintf(t26,"Ti izlazno:    %2f K",Ti);
pDC->TextOut (5200,1400 ,t26);

char t27[10];
sprintf(t27,"Pkr kriticno:    %2f Bara",Pkr/100000);
pDC->TextOut (5200,1200 ,t27);

char t28[10];
sprintf(t28,"Maseni Protok:    %2f kg/s",mp);
pDC->TextOut (5200,1000 ,t28);

char t29[10];
sprintf(t29,"Akr kriticno:    %2f m^2",Akriticno);
pDC->TextOut (5200,800 ,t29);


char t30[10];
sprintf(t30,"Ai izlazno:    %2f m^2",(Akriticno*EE));
pDC->TextOut (5200,600 ,t30);


char t33[10];
sprintf(t33,"Potisak:    %2f N",Fp1);
pDC->TextOut (5200,400 ,t33);

char t34[10];
sprintf(t34,"Potisak II :    %2f N",Fp2);
pDC->TextOut (5200,200 ,t34);



//kolona 2
char t16[10];
sprintf(t16,"Po (U komori): %2f Bara",Pkom/100000);
pDC->TextOut (7500,5000 ,t16);

char t17[10];
sprintf(t17,"Pa (Atmosferski) :    %2f Bara",Pamb/100000);
pDC->TextOut (7500,4800 ,t17);

char t18[10];
sprintf(t18,"Pi (Izlazni):    %2f Bara",Pizl/100000);
pDC->TextOut (7500,4600 ,t18);


char t19[10];
sprintf(t19,"kapa:    %2f",kapa);
pDC->TextOut (7500,4400 ,t19);

char t20[10];
sprintf(t20,"R(gasna konst.):    %2f J/KgK", Rgg );
pDC->TextOut (7500,4200 ,t20);

char t21[10];
sprintf(t21,"To (U komori):    %2f K",To );
pDC->TextOut (7500,4000 ,t21);

char t24[10];
sprintf(t24,"dkr kriticni poluprecnik: %2f mm",dkr);
pDC->TextOut (7500,3800 ,t24);

char t31[10];
sprintf(t31,"diz izlazni poluprecnik: %2f mm",diz);
pDC->TextOut (7500,3600 ,t31);

char t32[10];
sprintf(t32,"Duzina L: %2f mm",DuzinaL);
pDC->TextOut (7500,3400 ,t32);

char t35[10];
sprintf(t35,"Alfa ugao polukonusa: %2f ste",alfa_pk);
pDC->TextOut (7500,3200 ,t35);

char t36[10];
sprintf(t36,"Fi meh gubici: %2f ",FiMeh);
pDC->TextOut (7500,3000 ,t36);

char t37[10];
sprintf(t37,"Vreme rada: %2f s",t_rada);
pDC->TextOut (7500,2800 ,t37);

char t38[10];
sprintf(t38,"Masa PM: %2f Kg",MPM);
pDC->TextOut (7500,2600 ,t38);

char t39[10];
sprintf(t39,"ROo: %2f Kg/m^3",ROo);
pDC->TextOut (7500,1800 ,t39);

char t40[10];
sprintf(t40,"ROkr: %2f Kg/m^3",ROkr);
pDC->TextOut (7500,1600 ,t40);

char t41[10];
sprintf(t41,"ROiz: %2f Kg/m^3",ROiz);
pDC->TextOut (7500,1400 ,t41);





char t42[10];
sprintf(t42,"Koeficijent b: %2f ",b);
pDC->TextOut (500,5000 ,t42);

char t43[10];
sprintf(t43,"Koeficijent (manji od 1) n: %2f ",n);
pDC->TextOut (500,4800 ,t43);

char t44[10];
sprintf(t44,"Gustina punjenja: %2f kg/m^3",ROpu);
pDC->TextOut (500,4600 ,t44);

char t45[10];
sprintf(t45,"C*: %2f ","dato");
pDC->TextOut (500,4400 ,t45);

char t46[10];
sprintf(t46,"Po: %2f Bara",Pkom/100000);
pDC->TextOut (500,4200 ,t46);

char t47[10];
sprintf(t47,"ro: %2f m/s",ro);
pDC->TextOut (500,4000 ,t47);

char t48[10];
sprintf(t48,"re: %2f m/s",re);
pDC->TextOut (500,3800 ,t48);



char t49[10];
sprintf(t49,"r: %2f m/s",r);
pDC->TextOut (500,3600 ,t49);

char t50[10];
sprintf(t50,"sigma2: %2f 1/K",sigma2);
pDC->TextOut (500,3400 ,t50);
/**/






ReleaseDC(pDC);
}

/////////////////////////////////////////////////////////////////////////////
// CCKORView printing

BOOL CCKORView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CCKORView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CCKORView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CCKORView diagnostics

#ifdef _DEBUG
void CCKORView::AssertValid() const
{
	CView::AssertValid();
	
}

void CCKORView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CCKORDoc* CCKORView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCKORDoc)));
	return (CCKORDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////






void CCKORView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
			switch (nChar)
		{


	case 0x51://q
		m_alfa+=1;

	
		Invalidate();
		break;

	case 0x57://w
		m_alfa-=1;

		Invalidate();
		break;


	case 0x41://a
		m_beta-=1;

		Invalidate();
		break;

	case 0x53://s
		m_beta+=1;

		Invalidate();
		break;
	
	case VK_UP://
		PTY+=10;
		::SetCursor(::LoadCursor(NULL, IDC_CROSS));

		Invalidate();
		break;
		
	case VK_DOWN://
		PTY-=10;
		::SetCursor(::LoadCursor(NULL, IDC_CROSS));

		Invalidate();
		break;
	
	case VK_LEFT://
		PTX-=10;
		::SetCursor(::LoadCursor(NULL, IDC_CROSS));

		Invalidate();
		break;
	
	case VK_RIGHT://
		PTX+=10;
		::SetCursor(::LoadCursor(NULL, IDC_CROSS));

		Invalidate();
		break;

	case 0x1b://
	// exit(0);
	::MessageBox(NULL, "IZLAZ ?", "", MB_OK);
	exit(0);
	
	break;












	case 0x50://P

STRO par;
double r = par.STROgetR();
double n = par.STROgetn();
double b = par.STROgetb();
double G = par.STROgetMODEL_Lenoir_Robillard();


CCRM fluid;
double Pamb = fluid.CCRMgetP_amb();
double Pkom = fluid.CCRMgetP_kom();



fstream myfile;
myfile.open ("data2.txt",ios::out);

myfile <<"b = "<<b<<"      "<<""<<endl;
myfile <<"n = "<<n<<"      "<<""<<endl;
myfile <<"G = "<<G<<"      "<<""<<endl;


myfile <<"Pritisak u komori =    "<<Pkom/100000<<"      "<<"BAR-a"<<endl;

myfile <<"Brzina gorenja r =   "<<r<<"      "<<"m/s"<<endl;
myfile.close();

		Invalidate();
		break;




/*

	case 0x4d://M
		
fstream myfile;
myfile.open ("data.txt",ios::out);
myfile <<"i     "<<"     f=i*sin(i)       "<<endl;




for (double i2=0.0; i2<360.0; i2+=0.1)
{
	


myfile <<""<<i2<<"      "<<i2*sin(i2) <<endl;

cout<<""<<i2<<"      "<<i2*sin(i2)<<endl;

   
	if(i2 ==360)
{	
    break;
	
}


}
myfile.close();	

		Invalidate();
		break;

  */
		
}
	



	
	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}








void CCKORView::OnPar() 
{
    CParametri dlg;

	dlg.m_alfa = m_alfa;
	dlg.m_beta = m_beta;
	dlg.m_DDI = m_DDI;
	dlg.m_DKO = m_DKO;
	dlg.m_RKR = m_RKR;
	dlg.m_DKR = m_DKR;

	  if (dlg.DoModal () == IDOK)
	{
    
	m_alfa = dlg.m_alfa;
	m_beta= dlg.m_beta;
	m_DDI= dlg.m_DDI;
	m_DKO= dlg.m_DKO;
	m_RKR= dlg.m_RKR;
	m_DKR= dlg.m_DKR;



             
        Invalidate ();
    } 


}




void CCKORView::OnMasacena() 
{

	CCMasaCevi dlg;
	dlg.m_Rsp = m_Rsp;
    dlg.m_Run = m_Run;
	dlg.m_Lduz =m_Lduz;
	dlg.m_ROg = m_ROg;
	dlg.m_Umasa =m_Umasa;
	dlg.m_CenapoKg =m_CenapoKg;
	dlg.m_CenaUkupno =m_CenaUkupno;
	dlg.m_naponZ = m_naponZ;
    dlg.m_Pmax = m_Pmax;
	dlg.m_poason =m_poason;
	dlg.m_modulE =m_modulE;


	if (dlg.DoModal () == IDOK)
	{

    UpdateData(true);
	m_Rsp = dlg.m_Rsp;
	m_Run = dlg.m_Run;
	m_Lduz = dlg.m_Lduz;
	m_ROg = dlg.m_ROg;
	m_Umasa = dlg.m_Umasa;
	m_CenapoKg = dlg.m_CenapoKg;
	m_CenaUkupno = dlg.m_CenaUkupno;
	m_naponZ = dlg.m_naponZ;
	m_Pmax = dlg.m_Pmax;
	dlg.m_poason =m_poason;
	dlg.m_modulE =m_modulE;

	   Invalidate ();
    } 
	
}


void CCKORView::OnAutor() 
{
	CCAutorA dlg;
	if (dlg.DoModal () == IDOK)
	{

	

	   Invalidate ();
    } 	
}

void CCKORView::OnMtablica() 
{
	m_dlgINFO = new CCMtablica;
    m_dlgINFO->Create();
    m_dlgINFO->ShowWindow (SW_SHOW);	
}

void CCKORView::On1napon() 
{
	CC1napon dlg;
	dlg.m_rsp = m_rsp;
	dlg.m_run = m_run;
	dlg.m_debljina = m_debljina;
	dlg.m_odnosr =m_odnosr;
	dlg.m_pPu = m_pPu;
	dlg.m_nlri = m_nlri;
    dlg.m_nrri = m_nrri;
	dlg.m_ntri =m_ntri;
	dlg.m_ena = m_ena;
	dlg.m_srednjinn = m_srednjinn;
	dlg.m_taumax =m_taumax;
	dlg.m_Zatezna = m_Zatezna;


	if (dlg.DoModal () == IDOK)
	{

	UpdateData(true);	
	dlg.m_rsp = m_rsp;
	dlg.m_run = m_run;
	dlg.m_debljina = m_debljina;
	dlg.m_odnosr =m_odnosr;
	dlg.m_pPu = m_pPu;
	dlg.m_nlri = m_nlri;
	dlg.m_nrri = m_nrri;
	dlg.m_ntri =m_ntri;
	dlg.m_ena = m_ena;
	dlg.m_srednjinn = m_srednjinn;
	dlg.m_taumax =m_taumax;
	dlg.m_Zatezna = m_Zatezna;

		Invalidate ();
	}
}

void CCKORView::On3napon() 
{

	CC3napon dlg;

	 dlg.m_zateznaT=m_zateznaT;
	 dlg.m_kvalitetS=m_kvalitetS;
	 dlg.m_PuD=m_PuD;
	 dlg.m_Dun=m_Dun;
	 dlg.m_Dun2=m_Dun2;
	 dlg.m_Dsp1=m_Dsp1;
	 dlg.m_Dsp2=m_Dsp2;
	 dlg.m_PuD2=m_PuD2;
	 dlg.m_DD1=m_DD1;
	 dlg.m_DD2=m_DD2;
	 dlg.m_DP1=m_DP1;
	 dlg.m_DP2=m_DP2;
	 dlg.m_PP1=m_PP1;
	 dlg.m_PP2=m_PP2;	

	 	if (dlg.DoModal () == IDOK)
	{

	 dlg.m_zateznaT=m_zateznaT;
	 dlg.m_kvalitetS=m_kvalitetS;
	 dlg.m_PuD=m_PuD;
	 dlg.m_Dun=m_Dun;
	 dlg.m_Dun2=m_Dun2;
	 dlg.m_Dsp1=m_Dsp1;
	 dlg.m_Dsp2=m_Dsp2;
	 dlg.m_PuD2=m_PuD2;
	 dlg.m_DD1=m_DD1;
	 dlg.m_DD2=m_DD2;
	 dlg.m_DP1=m_DP1;
	 dlg.m_DP2=m_DP2;
	 dlg.m_PP1=m_PP1;
	 dlg.m_PP2=m_PP2;	

	

	   Invalidate ();
    } 


}

void CCKORView::OnTankozidne() 
{
	CCTankozid dlg;

	dlg.m_rsp2= m_rsp2 ;
	dlg.m_run2=m_run2;
	dlg.m_debljina2=m_debljina2;
	dlg.m_pritisakPi=m_pritisakPi;
	dlg.m_hoop=m_hoop;
	dlg.m_long=m_long;
	dlg.m_radi=m_radi;
	dlg.m_zatezna2=m_zatezna2;
	dlg.m_ena2=m_ena2;
	dlg.m_odnosDsR=m_odnosDsR;
	dlg.m_gnn2=m_gnn2;
	dlg.m_taumax2=m_taumax2;
	
 	if (dlg.DoModal () == IDOK)
	{

	dlg.m_rsp2= m_rsp2;
	dlg.m_run2=m_run2;
	dlg.m_debljina2=m_debljina2;
	dlg.m_pritisakPi=m_pritisakPi;
	dlg.m_hoop=m_hoop;
	dlg.m_long=m_long;
	dlg.m_radi=m_radi;
	dlg.m_zatezna2=m_zatezna2;
	dlg.m_ena2=m_ena2;
	dlg.m_odnosDsR=m_odnosDsR;
	dlg.m_gnn2=m_gnn2;
	dlg.m_taumax2=m_taumax2;
	
	Invalidate();

	}



}

void CCKORView::OnTankozid() 
{
    CCDeformacije dlg;

	dlg.m_dex = m_dex;
	dlg.m_dey = m_dey;
	dlg.m_dez = m_dez;
	dlg.m_zapreminaVo = m_zapreminaVo;
	dlg.m_dVo = m_dVo;
	dlg.m_radnaT = m_radnaT;
	dlg.m_defT =m_defT ;

	if (dlg.DoModal () == IDOK)
	{
	dlg.m_dex = m_dex;
	dlg.m_dey = m_dey;
	dlg.m_dez = m_dez;
	dlg.m_zapreminaVo = m_zapreminaVo;
	dlg.m_dVo = m_dVo;
	dlg.m_radnaT = m_radnaT;
	dlg.m_defT =m_defT ;

		Invalidate();
	}
}

void CCKORView::OnRmo() 
{
	
	CCRMosnovno dlg;
	
    	dlg.m_kapa = m_kapa;
		dlg.m_Pamb = m_Pamb;
		dlg.m_Pi =m_Pi;
		dlg.m_Po =m_Po;
		dlg.m_Rgg =m_Rgg;
		dlg.m_To =m_To;



	if (dlg.DoModal () == IDOK)
	{


		dlg.m_kapa = m_kapa;
		dlg.m_Pamb = m_Pamb;
		dlg.m_Pi =m_Pi;
		dlg.m_Po =m_Po;
		dlg.m_Rgg =m_Rgg;
		dlg.m_To =m_To;
		

	     
		Invalidate();
	}
	
}
/*CCRMosnovno fluid;
    	dlg.m_kapa = fluid.RMOgetm_kapa();
		dlg.m_Pamb = fluid.RMOgetm_Pamb() ;
		dlg.m_Pi =fluid.RMOgetm_Pi();
		dlg.m_Po =fluid.RMOgetm_Po();
		dlg.m_Rgg =fluid.RMOgetm_Rgg();
		dlg.m_To =fluid.RMOgetm_To();
*/



void CCKORView::OnTestSave() 
{
	CFileDialog*dlg = new CFileDialog(false,".SALSA");
		dlg->m_ofn.lpstrTitle = "Super Save";
	    dlg->DoModal();	
	
}

void CCKORView::OnTestOpen() 
{
    	CFileDialog*dlg = new CFileDialog(true, "txt", NULL, NULL, "Text Files (*.txt)|*.txt|""All Files||");
		dlg->m_ofn.lpstrTitle = "Matori oces li da otvoris fajl ili ne ?";
		dlg->MessageBox("Hi",NULL,MB_OK);

	    dlg->DoModal();	
	
}

void CCKORView::OnExe1() 
{
ShellExecute(this->m_hWnd,"open","C:\\Program Files\\VideoLAN\\VLC\\vlc.exe",
			 "E:\\muzika\\x\\101-snake-eater.mp3","",SW_SHOW );
}


void CCKORView::OnKt() 

{       
	ShellExecute(this->m_hWnd,"open","C:\\Documents and Settings\\PROJEKTOVANJE\\Desktop\\PROkorC\\cKOR\\kt.exe",
		"","",SW_SHOW );
	
}

void CCKORView::OnPolaris() 
{
	ShellExecute(this->m_hWnd,"open","C:\\Documents and Settings\\PROJEKTOVANJE\\Desktop\\PROkorC\\cKOR\\polaris.exe",
		"","",SW_SHOW );
	
}
