// paint2.h : main header file for the PAINT2 application
//

#if !defined(AFX_PAINT2_H__003ED9AB_6B76_4B19_B680_033C7503B3D9__INCLUDED_)
#define AFX_PAINT2_H__003ED9AB_6B76_4B19_B680_033C7503B3D9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CPaint2App:
// See paint2.cpp for the implementation of this class
//

class CPaint2App : public CWinApp
{
public:
	CPaint2App();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPaint2App)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CPaint2App)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PAINT2_H__003ED9AB_6B76_4B19_B680_033C7503B3D9__INCLUDED_)
