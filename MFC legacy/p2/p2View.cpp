// p2View.cpp : implementation of the CP2View class
//

#include "stdafx.h"
#include "p2.h"
#include <math.h>

#include "p2Doc.h"
#include "p2View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CP2View

IMPLEMENT_DYNCREATE(CP2View, CView)

BEGIN_MESSAGE_MAP(CP2View, CView)
	//{{AFX_MSG_MAP(CP2View)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CP2View construction/destruction

CP2View::CP2View()
{
	// TODO: add construction code here

}

CP2View::~CP2View()
{
}

BOOL CP2View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CP2View drawing

void CP2View::OnDraw(CDC* pDC)
{
	CP2Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
    return;
//+++++++++++++++++++++++++++++++++++++++


COLORREF qCircleColor = RGB(255,0,0);
CPen qCirclePen(PS_SOLID, 7, qCircleColor);
CPen* pqOrigPen = pDC->SelectObject(&qCirclePen);



for (int a = 5 ; a < 6 ; a++)


pDC->Ellipse(a+100, a+100, a*50, a*50);
//+++++++++++++++++++++++++++++++++++++++

COLORREF qLineColor = RGB(0,0,0);
CPen qLinePen(PS_SOLID, 7, qLineColor);
pDC->SelectObject(&qLinePen);


pDC->MoveTo(10, 0);
pDC->LineTo(10, 500);
pDC->SelectObject(pqOrigPen);

//+++++++++++++++++++++++++++++++++++++++
COLORREF qLineColor1 = RGB(0,0,0);
CPen qLinePen1(PS_SOLID, 7, qLineColor);
pDC->SelectObject(&qLinePen);


pDC->MoveTo(700, 400);
pDC->LineTo(10, 400);
pDC->SelectObject(pqOrigPen);


//+++++++++++++++++++++++++++++++++++++++


for (int bas = 1 ; bas < 687 ; bas++)

   for (int i = 1 ; i < 100 ; i++)
   {
	 
	   
	   COLORREF qLineColor = RGB(i,53,bas);
       CPen qLinePen(PS_SOLID, 7, qLineColor);
       pDC->SelectObject(&qLinePen);


        pDC->MoveTo(sin(i)*bas+100,cos(bas)*i+100);
        pDC->LineTo(bas+cos(i-bas*sin(i)+10)-1,cos(bas)*100);
        pDC->SelectObject(pqOrigPen);
	  
   }
}

//+++++++++++++++++++++++++++++++++++++++

	
		
	// TODO: add draw code for native data here


/////////////////////////////////////////////////////////////////////////////
// CP2View printing

BOOL CP2View::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CP2View::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CP2View::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CP2View diagnostics

#ifdef _DEBUG
void CP2View::AssertValid() const
{
	CView::AssertValid();
}

void CP2View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CP2Doc* CP2View::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CP2Doc)));
	return (CP2Doc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CP2View message handlers
