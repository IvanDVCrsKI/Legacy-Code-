// CirclesView.cpp : implementation of the CCirclesView class
//

#include "stdafx.h"


#include "CirclesDoc.h"
#include "CirclesView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCirclesView

IMPLEMENT_DYNCREATE(CCirclesView, CView)

BEGIN_MESSAGE_MAP(CCirclesView, CView)
	//{{AFX_MSG_MAP(CCirclesView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCirclesView construction/destruction

CCirclesView::CCirclesView()
{
	// TODO: add construction code here

}

CCirclesView::~CCirclesView()
{
}

BOOL CCirclesView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CCirclesView drawing

void CCirclesView::OnDraw(CDC* pDC)
{
	CCirclesDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CCirclesView diagnostics

#ifdef _DEBUG
void CCirclesView::AssertValid() const
{
	CView::AssertValid();
}

void CCirclesView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CCirclesDoc* CCirclesView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCirclesDoc)));
	return (CCirclesDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCirclesView message handlers
