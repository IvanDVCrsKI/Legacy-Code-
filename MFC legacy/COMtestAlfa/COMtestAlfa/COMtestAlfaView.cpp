// COMtestAlfaView.cpp : implementation of the CCOMtestAlfaView class
//

#include "stdafx.h"
#include "COMtestAlfa.h"

#include "COMtestAlfaDoc.h"
#include "COMtestAlfaView.h"

//DIJALOZI
#include "CComTest.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CCOMtestAlfaView

IMPLEMENT_DYNCREATE(CCOMtestAlfaView, CView)

BEGIN_MESSAGE_MAP(CCOMtestAlfaView, CView)
	ON_COMMAND(ID_COMTEST, &CCOMtestAlfaView::OnComtest)
END_MESSAGE_MAP()

// CCOMtestAlfaView construction/destruction

CCOMtestAlfaView::CCOMtestAlfaView()
{
	// TODO: add construction code here

}

CCOMtestAlfaView::~CCOMtestAlfaView()
{
}

BOOL CCOMtestAlfaView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CCOMtestAlfaView drawing

void CCOMtestAlfaView::OnDraw(CDC* /*pDC*/)
{
	CCOMtestAlfaDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: add draw code for native data here
}


// CCOMtestAlfaView diagnostics

#ifdef _DEBUG
void CCOMtestAlfaView::AssertValid() const
{
	CView::AssertValid();
}

void CCOMtestAlfaView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CCOMtestAlfaDoc* CCOMtestAlfaView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCOMtestAlfaDoc)));
	return (CCOMtestAlfaDoc*)m_pDocument;
}
#endif //_DEBUG


// CCOMtestAlfaView message handlers

void CCOMtestAlfaView::OnComtest()
{
	CCComTest dlg;// TODO: Add your command handler code here
		if (dlg.DoModal () == IDOK)
	{
	
     Invalidate ();
	}
	
}
