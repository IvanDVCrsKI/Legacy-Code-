// MFC_testKOMANDEView.h : interface of the CMFC_testKOMANDEView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MFC_TESTKOMANDEVIEW_H__CC90329A_0CC0_47C9_9B5A_B4988475E778__INCLUDED_)
#define AFX_MFC_TESTKOMANDEVIEW_H__CC90329A_0CC0_47C9_9B5A_B4988475E778__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMFC_testKOMANDEView : public CView
{
protected: // create from serialization only
	CMFC_testKOMANDEView();
	DECLARE_DYNCREATE(CMFC_testKOMANDEView)

// Attributes
public:
	CMFC_testKOMANDEDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMFC_testKOMANDEView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMFC_testKOMANDEView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	
	CPoint m_FirstPoint;
	CPoint m_SecondPoint;




	//{{AFX_MSG(CMFC_testKOMANDEView)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in MFC_testKOMANDEView.cpp
inline CMFC_testKOMANDEDoc* CMFC_testKOMANDEView::GetDocument()
   { return (CMFC_testKOMANDEDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFC_TESTKOMANDEVIEW_H__CC90329A_0CC0_47C9_9B5A_B4988475E778__INCLUDED_)
