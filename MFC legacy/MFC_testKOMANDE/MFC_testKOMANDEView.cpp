// MFC_testKOMANDEView.cpp : implementation of the CMFC_testKOMANDEView class
//

#include "stdafx.h"
#include "MFC_testKOMANDE.h"
#include "Cline.h"
#include "MFC_testKOMANDEDoc.h"
#include "MFC_testKOMANDEView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMFC_testKOMANDEView

IMPLEMENT_DYNCREATE(CMFC_testKOMANDEView, CView)

BEGIN_MESSAGE_MAP(CMFC_testKOMANDEView, CView)
	//{{AFX_MSG_MAP(CMFC_testKOMANDEView)
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMFC_testKOMANDEView construction/destruction

CMFC_testKOMANDEView::CMFC_testKOMANDEView():m_FirstPoint(CPoint(0,0)),m_SecondPoint(CPoint(0,0))

{
	
	// TODO: add construction code here

}

CMFC_testKOMANDEView::~CMFC_testKOMANDEView()
{
}

BOOL CMFC_testKOMANDEView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CMFC_testKOMANDEView drawing

void CMFC_testKOMANDEView::OnDraw(CDC* pDC)
{
	CMFC_testKOMANDEDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CMFC_testKOMANDEView printing

BOOL CMFC_testKOMANDEView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CMFC_testKOMANDEView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CMFC_testKOMANDEView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CMFC_testKOMANDEView diagnostics

#ifdef _DEBUG
void CMFC_testKOMANDEView::AssertValid() const
{
	CView::AssertValid();
}

void CMFC_testKOMANDEView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMFC_testKOMANDEDoc* CMFC_testKOMANDEView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMFC_testKOMANDEDoc)));
	return (CMFC_testKOMANDEDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMFC_testKOMANDEView message handlers


void CMFC_testKOMANDEView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	

	char *MsgCoord  = new char[20];

	sprintf(MsgCoord, "Left Button at P(%d, %d)", point.x, point.y);
	
	SetWindowText(MsgCoord);
}
