// KreatorTrajektorijaMDIView.cpp : implementation of the CKreatorTrajektorijaMDIView class
//

#include "stdafx.h"
#include "KreatorTrajektorijaMDI.h"

#include "KreatorTrajektorijaMDIDoc.h"
#include "KreatorTrajektorijaMDIView.h"





//
//DIJALOZI

#include "UputstvoDLG.h"




#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaMDIView

IMPLEMENT_DYNCREATE(CKreatorTrajektorijaMDIView, CView)

BEGIN_MESSAGE_MAP(CKreatorTrajektorijaMDIView, CView)
	//{{AFX_MSG_MAP(CKreatorTrajektorijaMDIView)
	ON_COMMAND(IDM_UPUTSTVO, OnUputstvo)
	ON_COMMAND(ID_OPCIJE_PRIKAZ_INFOTABLA, OnOpcijePrikazInfotabla)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaMDIView construction/destruction

CKreatorTrajektorijaMDIView::CKreatorTrajektorijaMDIView()
{
	m_ITabla = 0;


}

CKreatorTrajektorijaMDIView::~CKreatorTrajektorijaMDIView()
{
}

BOOL CKreatorTrajektorijaMDIView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaMDIView drawing

void CKreatorTrajektorijaMDIView::OnDraw(CDC* pDC)
{
	CKreatorTrajektorijaMDIDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	



//////////////////////////////////////////////////////
//CENTAR I GRAFICKI PRIKAZ
//////////////////////////////////////////////////////
	


	
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetViewportOrg(60, 620);
	pDC->SetWindowExt(100000,100000); //SetWindowExtent(100,100)  SetViewportExtent(200,200)  100 jedinica = 200 pixela
	                                                                                             //1 m = 2 pixela
	pDC->SetViewportExt(1000,-1000);// Orijentacija osa




//////////////////////////////////////////////////////
// OSE
//////////////////////////////////////////////////////	
	pDC->MoveTo(-1000,     0);
	pDC->LineTo( 20000000,     0);
	pDC->MoveTo(   0, -1000);
	pDC->LineTo(   0,  20000000);


//////////////////////////////////////////////////////	
//POZICIJA OZNAKA
//////////////////////////////////////////////////////		
	
	//Koordinate 
	//////////////////////////////////////////////////////	
	pDC->SetTextColor(RGB(100, 100, 0));

    pDC->TextOut(56000, 2000, 'X');
	pDC->TextOut(500, 35000, 'Y');
   //////////////////////////////////////////////////////	
    
	
	
	//Lenjiri po x i y osi
	//////////////////////////////////////////////////////	

    CPen PenBlack3(PS_SOLID, 2, RGB(0, 0, 0));
	pDC->SelectObject(PenBlack3);
    
	for(int koox = 0; koox < 1000000; koox += 1000)
    {
        pDC->MoveTo(koox, 0);
        pDC->LineTo(koox, -1000);
    }

	for(int kooy = 0; kooy < 1000000; kooy += 1000)
    {
        pDC->MoveTo(0, kooy);
        pDC->LineTo(-1000, kooy);
    }



	pDC->SelectObject(PenBlack3);
    for(int koox2 = 0; koox2 < 1000000; koox2 += 5000)
    {
        pDC->MoveTo(koox2, 0);
        pDC->LineTo(koox2, -2000);
    }
	for(int kooy2 = 0; kooy2 < 1000000; kooy2 += 5000)
    {
        pDC->MoveTo(0, kooy2);
        pDC->LineTo(-2000, kooy2);
    }

	for(int koox3 = 0; koox3 < 1000000; koox3 += 10000)
    {
        pDC->MoveTo(koox3, 0);
        pDC->LineTo(koox3, -3000);
    }
	for(int kooy3 = 0; kooy3 < 1000000; kooy3 += 10000)
    {
        pDC->MoveTo(0, kooy3);
        pDC->LineTo(-3000, kooy3);
    }

    pDC->SetTextColor(RGB(0, 0, 0));
	for (int kt1= 0; kt1<1000000; kt1+=10000) 
	{
        
        CString string;
        string.Format (("%d km"), (kt1/1000 ) );
        pDC->TextOut (kt1, -3000, string);
    }

	pDC->SetTextColor(RGB(0, 0, 0));
	for (int kt2= 1000; kt2<1000000; kt2+=10000) 
	{
        
        CString string;
        string.Format (("%dkm"), (((kt2-1000)/1000) ) );
        pDC->TextOut (-6000, kt2, string);
    }

}



























































/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaMDIView diagnostics

#ifdef _DEBUG
void CKreatorTrajektorijaMDIView::AssertValid() const
{
	CView::AssertValid();
}

void CKreatorTrajektorijaMDIView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CKreatorTrajektorijaMDIDoc* CKreatorTrajektorijaMDIView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CKreatorTrajektorijaMDIDoc)));
	return (CKreatorTrajektorijaMDIDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaMDIView message handlers








//

void CKreatorTrajektorijaMDIView::OnUputstvo() 
{
    CUputstvoDLG dlg;
	dlg.DoModal();	
}

void CKreatorTrajektorijaMDIView::OnOpcijePrikazInfotabla() 
{
	// TODO: Add your command handler code here
	
}
