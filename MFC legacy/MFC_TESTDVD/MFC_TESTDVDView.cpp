// MFC_TESTDVDView.cpp : implementation of the CMFC_TESTDVDView class
//

#include "stdafx.h"
#include "MFC_TESTDVD.h"

#include "MFC_TESTDVDDoc.h"
#include "MFC_TESTDVDView.h"
#include "Opcije.h"
//#include "Math.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMFC_TESTDVDView

IMPLEMENT_DYNCREATE(CMFC_TESTDVDView, CView)

BEGIN_MESSAGE_MAP(CMFC_TESTDVDView, CView)
	//{{AFX_MSG_MAP(CMFC_TESTDVDView)
	ON_COMMAND(IDM_OPCIJE, OnOpcije)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSEWHEEL()
	ON_WM_KEYDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMFC_TESTDVDView construction/destruction

CMFC_TESTDVDView::CMFC_TESTDVDView()
{
	// TODO: add construction code here
	m_Linija = 100;

}

CMFC_TESTDVDView::~CMFC_TESTDVDView()
{
}

BOOL CMFC_TESTDVDView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CMFC_TESTDVDView drawing

void CMFC_TESTDVDView::OnDraw(CDC* pDC)
{
	CMFC_TESTDVDDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);


	pDC->SetMapMode(MM_ISOTROPIC);
	pDC->SetViewportOrg(60, 420);
	pDC->SetWindowExt(1000,1000);
	pDC->SetViewportExt(1000,-1000);

	CPen PenBlack(PS_SOLID, 2, RGB(0, 0, 0));
	pDC->SelectObject(PenBlack);
	



	//pDC->LineTo(1000,1000);
	//pDC->MoveTo(500,500);
	
	pDC->TextOut(2*m_Linija,2*m_Linija,"URAAA");


	CPen PenBlue(PS_SOLID,2,RGB(0,0,255));
	pDC->SelectObject(PenBlue);


	for (int i = 10; i < 100; i+=10)
	{

			pDC->LineTo(i,100);
	        pDC->MoveTo(i,900);
			
	}
	for (int j = 10; j < 100; j+=10)
	{

			pDC->LineTo(100,j);
	        pDC->MoveTo(900,j);
			pDC->TextOut(i,j, "Ura");
	}
	
		
	






	











	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CMFC_TESTDVDView diagnostics

#ifdef _DEBUG
void CMFC_TESTDVDView::AssertValid() const
{
	CView::AssertValid();
}

void CMFC_TESTDVDView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMFC_TESTDVDDoc* CMFC_TESTDVDView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMFC_TESTDVDDoc)));
	return (CMFC_TESTDVDDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMFC_TESTDVDView message handlers

void CMFC_TESTDVDView::OnOpcije() 
{
	// TODO: Add your command handler code here
	COpcije dlg;
	dlg.m_Linija = m_Linija;

	if(dlg.DoModal() == IDOK)
	{
		m_Linija = dlg.m_Linija;
		Invalidate();
	}
}

void CMFC_TESTDVDView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	CView::OnLButtonDown(nFlags, point);

	CClientDC dc (this);
    dc.SetMapMode (MM_LOENGLISH);
    CPoint pos = point;
    dc.DPtoLP (&pos);
	dc.SetMapMode(MM_ISOTROPIC);
	dc.SetViewportOrg(60, 420);
	dc.SetWindowExt(1000,1000);
	dc.SetViewportExt(1000,-1000);
	
	CString string;

     dc.SetTextColor(RGB(255, 0, 0));
     string.Format (("  %d  ,  %d  "), pos.x, pos.y);
	 dc.TextOut(pos.x,pos.y,string);



    //for (int t = 10; t<100; t+=10)
	//{
	 dc.TextOut(pos.x,pos.y,"string");
	 //dc.LineTo(pos.x+t,pos.y+t);
	 //dc.MoveTo(pos.x+t,pos.y+100*t);
	 //dc.Ellipse(pos.x,pos.y,pos.x+t,pos.y+t);
	//}
}

void CMFC_TESTDVDView::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
		UpdateData(false);
	CView::OnMouseMove(nFlags, point);
	CClientDC dc (this);
 
    CPoint pos = point;
    dc.DPtoLP (&pos);
		
	dc.SetMapMode(MM_ISOTROPIC);
	dc.SetViewportOrg(pos.x, pos.y);
	dc.SetWindowExt(1000,1000);
	dc.SetViewportExt(100,-100);
	
	CRect Recto;
	GetClientRect(&Recto);
	
	//dc.MoveTo(Recto.Width() / 2, 0);
	dc.LineTo(Recto.Width() / 2, Recto.Height());
	dc.MoveTo(0, Recto.Height() / 2);
	//dc.LineTo(Recto.Width(), Recto.Height() / 2);
}
	//dc.LineTo(,);
	
		//for (int t = 10; t<100; t+=10)
	//{
		
	 
	 //dc.LineTo(pos.x+t,pos.y+t);
	 //dc.MoveTo(pos.x+t,pos.y+100*t);
	// dc.Rectangle(pos.x,pos.y,pos.x+t,pos.y+t);
	 //dc.TextOut(pos.x+30,pos.y+40,"POBEDA!");
	//}




void CMFC_TESTDVDView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	
	CView::OnKeyDown(nChar, nRepCnt, nFlags);
	CClientDC dc (this);

}















BOOL CMFC_TESTDVDView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
	// TODO: Add your message handler code here and/or call default
	
	return CView::OnMouseWheel(nFlags, zDelta, pt);
}
