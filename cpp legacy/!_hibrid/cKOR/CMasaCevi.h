#if !defined(AFX_CMASACEVI_H__2AE70AE8_4BCD_4A99_A842_9D7F3E07A2D3__INCLUDED_)
#define AFX_CMASACEVI_H__2AE70AE8_4BCD_4A99_A842_9D7F3E07A2D3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CMasaCevi.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCMasaCevi dialog

class CCMasaCevi : public CDialog
{
// Construction
public:
	CCMasaCevi(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCMasaCevi)
	enum { IDD = IDD_MASACEVI };
	double	m_Rsp;
	double	m_Run;
	double	m_Lduz;
	double	m_CenapoKg;
	double	m_CenaUkupno;
	double	m_Umasa;
	double	m_ROg;
	double	m_naponZ;
	double	m_Pmax;
	double	m_poason;
	double	m_modulE;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCMasaCevi)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCMasaCevi)
	afx_msg void OnButton1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CMASACEVI_H__2AE70AE8_4BCD_4A99_A842_9D7F3E07A2D3__INCLUDED_)
