

/*****************************************************************************
 * NAME: SerialADEmbed.cpp
 * DATE:  7/5/2003
 * PRGR: Y. Bai - Copyright 2003 - 2007
 * DESC:  Embedded assembly code (intel 8088/80x86) to communicate with a serial A/D
                   converter  (TLC-548) to get analog signal inputs from a RS-232 port
 ******************************************************************************/
#include "SerialADEmbed.h"
void main()
{
       FILE* fp;
       char     cc;
       int        index;
       BYTE  value[100];
       printf("\n****** WELCOME TO SERIAL A/D CONVERTER TESTING *****\n");
       printf("\nBegin the A/D conversion?  y/n\n");
       scanf("%c", &cc);
       if (cc == 'y' || cc == 'Y')
      {
Port_Init();
fp = fopen("C:\\SData\\Serialad.dat", "w");
if (fp == NULL)
{
       printf("File open error! program exit\n");
       return;
}
for (index = 0; index < MAXNUM; index++)
{
        value[index] = Port_Read();
        printf("Received data =: %d\n", value[index]);
        fprintf(fp, "%d\n", value[index]);
}
fclose(fp);
       }
       printf("\n************* END OF THE TEST ***************\n\n");
}

void  Port_Init()
{
        printf("\nBegin to initialize the serial port...\n");
        c_inial();
        return;
}
void  c_inial()
{
        _asm
       {
mov  dx, MCR ; Set up MCR to set DTR & RTS to 1 (space or active).
mov  al, 03h      ; Output power (DTR = 1) & disable CS of TLC548 (RTS = 1).
out   dx, al       ; Finish set up the MCR.
mov dx, LCR ; Select the Line Control Register to set up the clock of TLC548.
         mov al, 00h ;
         out   dx, al ; Disable the AD clock.
       }
}
BYTE  Port_Read()
{
       BYTE  value;
       value = c_getad();
       return value;
}
BYTE  c_getad()
{
       BYTE  rc;
       _asm
      {
mov  dx, MCR ; Reset MCR - RTS to 0 (CS of TLC548 active - low).
mov  al,  01h      ; Still keep output power (DTR = 1).
out   dx,  al       ; Finish set up the MCR.
mov  cx,  08h ; Setup the counter of the reading loop
mov  bl,  00h ; Initialize the result (bl is used to store the result).
       get: shl    bl,  1 ; Multiple result by 2 (equivalent to shift bl left with 1 bit).
mov dx,  LCR ; Select the Line Control Register to active the clock of TLC548.
         mov al,   40h ;
          out   dx,  al ; Enable the clock.
; mov al,   08h ; You may need to add these three lines to start up the TLC548.
; mov dx,  COM1  ; new added
;out   dx,  al ; new added
mov dx,  MSR ; Check the MSR b5 (when b5 = 1 -> CTS is set).
in     al,   dx ; CTS is connected to output of the TLC548. CTS = 1 means that -
test  al,   10h ; A binary value of 1 is obtained from TLC548 output.
jz done ; b5 = 0, TLC548 outputs a binary 0. No operation is needed.
inc   bl ; b5 = 1, TLC548 outputs a binary 1. Increment result by 1.
    done: mov dx, LCR ; Select the Line Control Register to reset the clock of TLC548.
         mov al,  00h ;
         out   dx, al ; Disable the clock.
dec   cx ; cx - 1 at each loop.
jnz   get ; Loop until all 8-bit output of TLC548 is done.
mov dx, MCR ; Set up the MCR to set DTR & RTS to 1 (space or active).
mov al,  03h      ; Output power (DTR = 1) & disable CS of TLC548 (RTS = 1).
out  dx,  al       ; Finish set up the MCR.
mov rc,  bl ; Reserve the result into rc.
       }
       return  rc;
}