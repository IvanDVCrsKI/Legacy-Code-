#if !defined(AFX_INFODLG_H__042BF065_E48E_474D_99DD_A6B5062EA825__INCLUDED_)
#define AFX_INFODLG_H__042BF065_E48E_474D_99DD_A6B5062EA825__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InfoDLG.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CInfoDLG dialog

class CInfoDLG : public CDialog
{
// Construction
public:
	CInfoDLG(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CInfoDLG)
	enum { IDD = IDD_INFO };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInfoDLG)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CInfoDLG)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INFODLG_H__042BF065_E48E_474D_99DD_A6B5062EA825__INCLUDED_)
