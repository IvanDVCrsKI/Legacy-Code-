#if !defined(AFX_C3NAPON_H__0441577F_A481_4C31_AACF_FBB80F881F33__INCLUDED_)
#define AFX_C3NAPON_H__0441577F_A481_4C31_AACF_FBB80F881F33__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// C3napon.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CC3napon dialog

class CC3napon : public CDialog
{
// Construction
public:
	CC3napon(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CC3napon)
	enum { IDD = IDD_3NAPON };
	double	m_zateznaT;
	double	m_kvalitetS;
	double	m_PuD;
	double	m_Dun;
	double	m_Dun2;
	double	m_Dsp1;
	double	m_Dsp2;
	double	m_PuD2;
	double	m_DD1;
	double	m_DD2;
	double	m_DP1;
	double	m_DP2;
	double	m_PP1;
	double	m_PP2;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CC3napon)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CC3napon)
	afx_msg void OnButton2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_C3NAPON_H__0441577F_A481_4C31_AACF_FBB80F881F33__INCLUDED_)
