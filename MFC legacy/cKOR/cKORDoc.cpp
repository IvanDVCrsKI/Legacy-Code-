// cKORDoc.cpp : implementation of the CCKORDoc class
//

#include "stdafx.h"
#include "cKOR.h"

#include "cKORDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCKORDoc

IMPLEMENT_DYNCREATE(CCKORDoc, CDocument)

BEGIN_MESSAGE_MAP(CCKORDoc, CDocument)
	//{{AFX_MSG_MAP(CCKORDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCKORDoc construction/destruction

CCKORDoc::CCKORDoc()
{
	// TODO: add one-time construction code here

}

CCKORDoc::~CCKORDoc()
{
}

BOOL CCKORDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CCKORDoc serialization

void CCKORDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CCKORDoc diagnostics

#ifdef _DEBUG
void CCKORDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CCKORDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCKORDoc commands
