// KreatorTrajektorijaDVView.cpp : implementation of the CKreatorTrajektorijaDVView class
//

#include "stdafx.h"
#include "KreatorTrajektorijaDV.h"

#include "KreatorTrajektorijaDVDoc.h"
#include "KreatorTrajektorijaDVView.h"

#include "Math.h"



//DIJALOZI
#include "AutorDV.h"
#include "Uputstvo.h"
#include "Parametri.h"
#include "Mreza.h"
#include "Korak.h"
#include "DGrafik.h"
#include "Tabla.h"
#include "Zum.h"
#include "Atmosfera.h"





#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaDVView

IMPLEMENT_DYNCREATE(CKreatorTrajektorijaDVView, CView)

BEGIN_MESSAGE_MAP(CKreatorTrajektorijaDVView, CView)
	//{{AFX_MSG_MAP(CKreatorTrajektorijaDVView)
	ON_COMMAND(IDM_UPUTSTVO, OnUputstvo)
	ON_COMMAND(IDM_PARAMETRI, OnParametri)
	ON_COMMAND(IDM_MREZA, OnMreza)
	ON_COMMAND(IDM_KORAK, OnKorak)
	ON_COMMAND(IDM_DGRAFIK, OnDgrafik)
	ON_COMMAND(IDM_AUTOR, OnAutor)
	ON_COMMAND(IDM_TABLA, OnTabla)
	ON_COMMAND(IDM_ZUM, OnZum)
	ON_COMMAND(IDM_ATMOSFERA, OnAtmosfera)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaDVView construction/destruction

CKreatorTrajektorijaDVView::CKreatorTrajektorijaDVView()
{
	// TODO: add construction code here
	m_Brzina_f1 = 0;
	m_Brzina_f2 = 0;
	m_Brzina_f3= 0;
	m_Ugao_f1= 0;
    m_Ugao_f2= 0;
	m_Ugao_f3= 0;
	m_Mreza = 0;
	m_DGrafik = 0;
	m_Zumx= 100000;
	m_Zumy = 100000;
	m_Korak = 1;
	m_IDev = 150000;
	m_GMreza = 5000;
	m_ITabla = 0;
	m_PI = 3.14159;
	m_Grav = 9.81;
	m_Mf1 = 0;
	m_Mf2 = 0;
	m_Mf3 = 0;
	m_PAG = 0;
	m_TAG = 0;
	m_tropos = 16000;
	m_troposp = 18000;
	m_stratos = 55000;
	m_mezos = 80000;

	m_PTG =1;
	m_PadT = 6,5;
	m_To = 25;

	



		

}

CKreatorTrajektorijaDVView::~CKreatorTrajektorijaDVView()
{
}

BOOL CKreatorTrajektorijaDVView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaDVView drawing

void CKreatorTrajektorijaDVView::OnDraw(CDC* pDC)
{
	CKreatorTrajektorijaDVDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);



//////////////////////////////////////////////////////
//CENTAR I GRAFICKI PRIKAZ
//////////////////////////////////////////////////////
	


	
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetViewportOrg(60, 620);
	pDC->SetWindowExt(m_Zumx,m_Zumy); //SetWindowExtent(100,100)  SetViewportExtent(200,200)  100 jedinica = 200 pixela
	                                                                                             //1 m = 2 pixela
	pDC->SetViewportExt(1000,-1000);// Orijentacija osa

    




//////////////////////////////////////////////////////
//ATMOSFERA PRIKAZ
//////////////////////////////////////////////////////


CString stringA;


CPen PenBlue1(PS_SOLID, 1, RGB(200,200,200));


pDC->SelectObject(PenBlue1);

int tropos = m_tropos;
	
	
int troposp = m_troposp;
int stratos = m_stratos;
int mezos = m_mezos;

switch (m_PAG)
	{
	case 0:
		
	      
	break;



	case 1:
		pDC->MoveTo(0, tropos);
pDC->LineTo(150000, tropos);
//troposfera
	CBrush brush2 (RGB (240, 240, 255));
	pDC->SelectObject(brush2);
	pDC->Rectangle(0,0,150000,tropos);

	

//tropopauza

pDC->MoveTo(0, troposp);
pDC->LineTo(150000, troposp);
    CBrush brush3 (RGB (250, 250, 255));
	pDC->SelectObject(brush3);
	pDC->Rectangle(0,tropos,150000,troposp);
	
	
	
          


//stratosfera
pDC->MoveTo(0, stratos);
pDC->LineTo(150000, stratos);
    CBrush brush4 (RGB (250, 250, 255));
	pDC->SelectObject(brush4);
	pDC->Rectangle(0,troposp,150000,stratos); 
	
	
	      
//mezosfera
	
    CBrush brush5 (RGB (252, 252, 255));
	pDC->SelectObject(brush5);
	pDC->Rectangle(0,55000,150000,80000);
	
		break;
	}

switch (m_TAG)
	{
	case 0:
		
	      
	break;



	case 1:
		pDC->SetTextColor(RGB(0, 0, 255));
    stringA.Format (("Troposfera od %d, do %d m "), 0, tropos);

    pDC->TextOut (90000,tropos/2 , stringA);
	
	pDC->SetTextColor(RGB(0, 0, 255));
    stringA.Format (("Tropopauza od %d, do %d m "), tropos, troposp);

    pDC->TextOut (90000,17800 , stringA);
 

    pDC->SetTextColor(RGB(0, 0, 255));
    stringA.Format (("Stratosfera od %d, do %d m "), troposp, stratos);
    pDC->TextOut (90000,(stratos+troposp)/2 , stringA);

    pDC->SetTextColor(RGB(0, 0, 255));
    stringA.Format (("Mezosfera od %d, do %d m "), stratos, mezos);

    pDC->TextOut (90000,stratos+2000 , stringA);
		break;
	}



//merac sa tekstom
////////////////////////////////////////////////////////
/////TEMPERATURA
////////////////////////////////////////////////////////

CPen PenBlackT(PS_SOLID, 10, RGB(0,0,0));


pDC->SelectObject(PenBlackT);


switch (m_PTG)
	
	{
	case 0:
    
	

	break;

	case 1:
		
    
        
    
	


    double dT=m_To-m_PadT;
	CString string;
	pDC->SetTextColor(RGB(255, 0, 0));

	for(int iT = 0; iT<m_tropos;iT +=1000)
	{		
		dT-=(6.5);
		

		  string.Format ("T: ");
		  pDC->TextOut (84000,1500+iT , string);
		  char ct1[10];		 
		  sprintf(ct1,"%2f",dT);
		  pDC->TextOut (87000,1500+iT ,ct1);
		  stringA.Format (("na %d m "), iT);
          pDC->TextOut (95000,1500+iT , stringA);

		
	
	}
		break;
	}

///////////////////////////////////////////////////////

//	
//////////////////////////////////////////////////////	
//KONSTANTE
//////////////////////////////////////////////////////	
double PI = m_PI;
double g = m_Grav;
//////////////////////////////////////////////////////		



//////////////////////////////////////////////////////
//MREZA
//////////////////////////////////////////////////////	
	CPen PenBlack4(PS_SOLID, 1, RGB(0, 0, 0));
	pDC->SelectObject(PenBlack4);
	
	switch (m_Mreza)
	
	{
	case 0:
    
	

	break;

	case 1:
		for(int kooxx = 0; kooxx < 1000000; kooxx += m_GMreza)
    {
        pDC->MoveTo(kooxx, 0);
        pDC->LineTo(kooxx, 150000);
    }
	

	for(int kooyy = 0; kooyy < 1000000; kooyy += m_GMreza)
    {
        pDC->MoveTo(0, kooyy);
        pDC->LineTo(150000, kooyy);
    }
		break;
	}
//////////////////////////////////////////////////////	
//PROMENE BRZINE I UGLA
//////////////////////////////////////////////////////	
int alfa1 = m_Ugao_f1;
int alfa2 = m_Ugao_f2;
int alfa3 = m_Ugao_f3;

double v1 = m_Brzina_f1;
double v2 = m_Brzina_f2;
int v3 = m_Brzina_f3;



///////////////////////////////////////////////////////
//PRIKAZ TABLE
///////////////////////////////////////////////////////
 switch (m_ITabla)
	
	{
	case 0:
    
	

	break;

	case 1:
		CRect Proz;
		GetClientRect(&Proz);
		 
		int pozTx = 1000;
		int pozTy = 25000;

          //61-80
		CString string;
//////////////////////////////////////////////////////////////////////////////
//Tekst parametri za funkciju F1 CRVENA
//////////////////////////////////////////////////////////////////////////////
		  pDC->SetTextColor(RGB(255, 0, 0));
          string.Format (("Ugao: %d;"),alfa1);
          pDC->TextOut (60000+pozTx,35000+pozTy , string);//pDC->TextOut (60000+pozTx,35000+pozTy , string);

		  string.Format ("Brzina: ");
		  pDC->TextOut (60000+pozTx,33000+pozTy , string);
		  char cv1[10];		 
		  sprintf(cv1,"%2f",v1);
		  pDC->TextOut (69000+pozTx,33000+pozTy ,cv1);

		  string.Format ("Vreme Leta: ");
		  pDC->TextOut (60000+pozTx,31000+pozTy , string);
          char ct1[10];
		  double resH1 = sin (alfa1*PI/180);
		  double VREME1 = (2*v1*resH1)/(9.81);
		  sprintf(ct1, "%2f",VREME1);
		  pDC->TextOut (69000+pozTx,31000+pozTy ,ct1);

		  string.Format ("Domet:");
		  pDC->TextOut (60000+pozTx,29000+pozTy , string);
		  double VISINA1 = ((v1*v1*resH1*resH1)/(2*9.81));
		  char cd1[10];
		  double resD1;
          resD1 = sin (2*alfa1*PI/180);
          double DOMET1 = (v1*v1*resD1)/9.81;
		  sprintf(cd1, "%2f",DOMET1);
		  pDC->TextOut (69000+pozTx,29000+pozTy ,cd1);

		  string.Format ("Visina:");
		  pDC->TextOut (60000+pozTx,27000+pozTy , string);
		  char ch1[10];
		  sprintf(ch1,"%2f",VISINA1);
		  pDC->TextOut (69000+pozTx,27000+pozTy ,ch1);

//////////////////////////////////////////////////////////////////////////////
//Tekst parametri za funkciju F2 PLAVA
//////////////////////////////////////////////////////////////////////////////

		  pDC->SetTextColor(RGB(0, 0, 255));
          string.Format (("Ugao: %d;"),alfa2);
          pDC->TextOut (60000+pozTx,22000+pozTy , string);

		  string.Format ("Brzina: ");
		  pDC->TextOut (60000+pozTx,20000+pozTy , string);
		  char cv2[10];		 
		  sprintf(cv2,"%2f",v2);
		  pDC->TextOut (69000+pozTx,20000+pozTy ,cv2);

		  string.Format ("Vreme Leta: ");
		  pDC->TextOut (60000+pozTx,18000+pozTy , string);
          char ct2[10];
		  double resH2 = sin (alfa2*PI/180);
		  double VREME2 = (2*v2*resH1)/(9.81);
		  sprintf(ct2, "%2f",VREME2);
		  pDC->TextOut (69000+pozTx,18000+pozTy ,ct2);

		  string.Format ("Domet:");
		  pDC->TextOut (60000+pozTx,16000+pozTy , string);
		  double VISINA2 = ((v2*v2*resH1*resH2)/(2*9.81));
		  char cd2[10];
		  double resD2;
          resD2 = sin (2*alfa2*PI/180);  // operator... sin (2alfa)
          double DOMET2 = (v2*v2*resD2)/9.81;
		  sprintf(cd2, "%2f",DOMET2);
		  pDC->TextOut (69000+pozTx,16000+pozTy ,cd2);

		  string.Format ("Visina:");
		  pDC->TextOut (60000+pozTx,14000+pozTy , string);
		  char ch2[10];
		  sprintf(ch2,"%2f",VISINA2);
		  pDC->TextOut (69000+pozTx,14000+pozTy ,ch2);
/*
*/




		  
		  
		  //
		  //
		  //

          //pDC->SetTextColor(RGB(0, 0, 255));
          //string.Format (("-Funkcija f2 Ugao %d, Brzina %d m/s "), alfa2, v2);
          //pDC->TextOut (60000,33000 ,str);

          //pDC->SetTextColor(RGB(0, 255, 0));
          //string.Format (("-Funkcija f3 Ugao %d, Brzina %d m/s "), alfa3, v3);
          //pDC->TextOut (60000,31000 , string);
		  
	
		break;
	}    









//////////////////////////////////////////////////////
// OSE
//////////////////////////////////////////////////////	
	pDC->MoveTo(-1000,     0);
	pDC->LineTo( 20000000,     0);
	pDC->MoveTo(   0, -1000);
	pDC->LineTo(   0,  20000000);






//////////////////////////////////////////////////////	
//FUNKCIJE
//////////////////////////////////////////////////////	


//KORAK I MAKSIMUM
//////////////////////////////////////////////////////	

double kor = m_Korak;

double max = m_IDev; //oko milion
//////////////////////////////////////////////////////	



pDC->SelectObject(PenBlack4);

////////////////////////////////////////////////////////////////////////////////////////////////////////////		
//Funkcija f1 CRVENA
////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	switch (m_Mf1)
	{
	case 0:
		
	      
	break;



	case 1:
		for(double i = 1; i < max; i +=kor)
	{ 
        
		double c1 = 1  ;


	    double f =(i*tan((PI*alfa1)/180))-((g*i*i)/(2*v1*v1*cos((PI*alfa1)/180)*cos((PI*alfa1)/180)));
			//
		    // double f =1*i*tan(100*i)*sin(i);


	    pDC->SetPixel(i, f, RGB(255, 0, 0));
	}
		break;
	}



////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//Funkcija f2 PLAVA
////////////////////////////////////////////////////////////////////////////////////////////////////////////
	switch (m_Mf2)
	{
	case 0:
		
	     
	break;



	case 1:
		for(double i2 = 1; i2 < max; i2 +=kor)
	{
		int c2 = 0;
	    double f2 = (i2*tan((PI*alfa2)/180))-((g*i2*i2)/(2*v2*v2*cos((PI*alfa2)/180)*cos((PI*alfa2)/180)));
			//tan(i2)*i2;
	    pDC->SetPixel(i2, f2, RGB(0, 0, 255));

	}
		break;
	}




	
////////////////////////////////////////////////////////////////////////////////////////////////////////////		
//Funkcija f3 ZELENA
////////////////////////////////////////////////////////////////////////////////////////////////////////////	
    switch (m_Mf3)
	{
	case 0:
		
	      
	break;



	case 1:	

		for(double t = 0; t < 20; t +=1)
	{
		for(double i3 = 1; i3 < max; i3 +=kor)
		
		{
		int c3 = 0;
	    double f3 = (i3*tan((PI*alfa3)/180))-((g*i3*i3)/(2*(v3+t)*(v3+t)*cos((PI*alfa3)/180)*cos((PI*alfa3)/180)));;
	    pDC->SetPixel(i3, f3, RGB(0, 200, 0));
		}   
    }
		break;
	}
	
	
	
	/*
	(int)((nHeight / 2)*(1 - (sin ((2 * PI * i)*1000 / 30))));
	CRect rect;
    GetClientRect (&rect);
    int nWidth = rect.Width ();
    int nHeight = rect.Height ();
	  
		
		  CPoint aPoint[KOR];

    for (int i3=0; i3<KOR; i3+=kor) 
	{
        //aPoint[i].x = (i * nWidth)*1000 / 30;
        aPoint[i3].y = (i3*tan((PI*alfa3)/180))-((g*i3*i3)/(2*(v3)*(v3)*cos((PI*alfa3)/180)*cos((PI*alfa3)/180)));
    }
   pDC->Polyline (aPoint, KOR);
	
	*/


//////////////////////////////////////////////////////////////////////////////
//GRAFIK DONJI PRIKAZ    
//////////////////////////////////////////////////////////////////////////////	
	
	CBrush brush (RGB (255, 255, 255));
	pDC->SelectObject(brush);


	switch (m_DGrafik)
	{
	case 0:
		
	      pDC->Rectangle(-1000000,-1,2000000,-10000000);
	break;



	case 1:
		break;
	}
//////////////////////////////////////////////////////	
	




//////////////////////////////////////////////////////	
//POZICIJA OZNAKA
//////////////////////////////////////////////////////		
	
	//Koordinate 
	//////////////////////////////////////////////////////	
	pDC->SetTextColor(RGB(100, 100, 0));

    pDC->TextOut(56000, 2000, 'X');
	pDC->TextOut(500, 35000, 'Y');
   //////////////////////////////////////////////////////	
    
	
	
	//Lenjiri po x i y osi
	//////////////////////////////////////////////////////	

    CPen PenBlack3(PS_SOLID, 2, RGB(0, 0, 0));
	pDC->SelectObject(PenBlack3);
    
	for(int koox = 0; koox < 1000000; koox += 1000)
    {
        pDC->MoveTo(koox, 0);
        pDC->LineTo(koox, -1000);
    }

	for(int kooy = 0; kooy < 1000000; kooy += 1000)
    {
        pDC->MoveTo(0, kooy);
        pDC->LineTo(-1000, kooy);
    }



	pDC->SelectObject(PenBlack3);
    for(int koox2 = 0; koox2 < 1000000; koox2 += 5000)
    {
        pDC->MoveTo(koox2, 0);
        pDC->LineTo(koox2, -2000);
    }
	for(int kooy2 = 0; kooy2 < 1000000; kooy2 += 5000)
    {
        pDC->MoveTo(0, kooy2);
        pDC->LineTo(-2000, kooy2);
    }

	for(int koox3 = 0; koox3 < 1000000; koox3 += 10000)
    {
        pDC->MoveTo(koox3, 0);
        pDC->LineTo(koox3, -3000);
    }
	for(int kooy3 = 0; kooy3 < 1000000; kooy3 += 10000)
    {
        pDC->MoveTo(0, kooy3);
        pDC->LineTo(-3000, kooy3);
    }

    pDC->SetTextColor(RGB(0, 0, 0));
	for (int kt1= 0; kt1<1000000; kt1+=10000) 
	{
        
        CString string;
        string.Format (("%d km"), (kt1/1000 ) );
        pDC->TextOut (kt1, -3000, string);
    }

	pDC->SetTextColor(RGB(0, 0, 0));
	for (int kt2= 1000; kt2<1000000; kt2+=10000) 
	{
        
        CString string;
        string.Format (("%dkm"), (((kt2-1000)/1000) ) );
        pDC->TextOut (-6000, kt2, string);
    }

}














/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaDVView diagnostics

#ifdef _DEBUG
void CKreatorTrajektorijaDVView::AssertValid() const
{
	CView::AssertValid();
}

void CKreatorTrajektorijaDVView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CKreatorTrajektorijaDVDoc* CKreatorTrajektorijaDVView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CKreatorTrajektorijaDVDoc)));
	return (CKreatorTrajektorijaDVDoc*)m_pDocument;
}
#endif //_DEBUG








/////////////////////////////////////////////////////////////////////////////
// CKreatorTrajektorijaDVView message handlers

/////////////////////////////////////////////////
                         //////////
                           
                          ////////

                            ///
 
                             

void CKreatorTrajektorijaDVView::OnUputstvo() 
{
	CUputstvo dlg;
	dlg.DoModal();
	
}
void CKreatorTrajektorijaDVView::OnTabla() 
{
	CTabla dlg;
	dlg.m_ITabla = m_ITabla;

	if(dlg.DoModal()==IDOK)
	{
		m_ITabla = dlg.m_ITabla;
		Invalidate();
	}
	
}
void CKreatorTrajektorijaDVView::OnParametri() 
{
	CParametri dlg;

    dlg.m_Brzina_f1 =m_Brzina_f1;
	dlg.m_Brzina_f2 =m_Brzina_f2;
	dlg.m_Brzina_f3 =m_Brzina_f3;

	dlg.m_Ugao_f1 =m_Ugao_f1;
	dlg.m_Ugao_f2 =m_Ugao_f2;
	dlg.m_Ugao_f3 =m_Ugao_f3;

	dlg.m_Grav= m_Grav;
	dlg.m_PI= m_PI;

	dlg.m_Mf1 = m_Mf1;
	dlg.m_Mf2 = m_Mf2;
	dlg.m_Mf3 = m_Mf3;


    if (dlg.DoModal () == IDOK)
	{
        m_Brzina_f1 = dlg.m_Brzina_f1;
		m_Brzina_f2 = dlg.m_Brzina_f2;
		m_Brzina_f3 = dlg.m_Brzina_f3;

		m_Ugao_f1 = dlg.m_Ugao_f1;
		m_Ugao_f2 = dlg.m_Ugao_f2;
		m_Ugao_f3 = dlg.m_Ugao_f3;

		m_Grav = dlg.m_Grav;
		m_PI = dlg.m_PI;

		m_Mf1=dlg.m_Mf1;
		m_Mf2=dlg.m_Mf2;
		m_Mf3=dlg.m_Mf3;



             
        Invalidate ();
    }    
	
}

void CKreatorTrajektorijaDVView::OnMreza() 
{
	CMreza dlg;
	dlg.m_Mreza = m_Mreza;
	dlg.m_GMreza = m_GMreza;
	
	if (dlg.DoModal () == IDOK)
	{
		m_Mreza= dlg.m_Mreza;
		m_GMreza= dlg.m_GMreza;
		Invalidate ();
	}
	
}

void CKreatorTrajektorijaDVView::OnKorak() 
{
	CKorak dlg;
	dlg.m_Korak = m_Korak;
	dlg.m_IDev = m_IDev;

	if(dlg.DoModal() == IDOK)
	{
	m_Korak = dlg.m_Korak;
	m_IDev = dlg.m_IDev;
	Invalidate();
	}
	
}

void CKreatorTrajektorijaDVView::OnDgrafik() 
{
	CDGrafik dlg;
	dlg.m_DGrafik = m_DGrafik;
	
	if (dlg.DoModal () == IDOK)
	{
		m_DGrafik= dlg.m_DGrafik;
		Invalidate ();
	}
	
}

void CKreatorTrajektorijaDVView::OnAutor() 
{
	CAutorDV dlg;
	dlg.DoModal();
	
}





void CKreatorTrajektorijaDVView::OnZum() 
{
	{
	CZum dlg;
	dlg.m_Zumx = m_Zumx;
	dlg.m_Zumy = m_Zumy;
	
	if (dlg.DoModal () == IDOK)
	{
		m_Zumx= dlg.m_Zumx;
		m_Zumy= dlg.m_Zumy;
		Invalidate ();
	}
	
}
	
}



void CKreatorTrajektorijaDVView::OnAtmosfera() 
{
	
	CAtmosfera dlg;
	dlg.m_tropos = m_tropos;
	dlg.m_troposp = m_troposp;
	dlg.m_mezos = m_mezos;
	dlg.m_stratos = m_stratos;
	dlg.m_PAG = m_PAG;
	dlg.m_TAG = m_TAG;
	dlg.m_PTG = m_PTG;
	dlg.m_PadT = m_PadT;
	dlg.m_To = m_To;


	
	if (dlg.DoModal () == IDOK)
	{
		m_tropos = dlg.m_tropos;
		m_troposp = dlg.m_troposp;
	    m_mezos = dlg.m_mezos;
	    m_stratos = dlg.m_stratos;
	    m_PAG = dlg.m_PAG;
	    m_TAG =dlg.m_TAG;
		m_PTG = dlg.m_PTG;
	    m_PadT = dlg.m_PadT;
	    m_To = dlg.m_To;

		Invalidate ();
	}
}

