// KreatorTrajektorijaMDIDoc.h : interface of the CKreatorTrajektorijaMDIDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_KREATORTRAJEKTORIJAMDIDOC_H__5C4D2975_7374_4025_B987_14ECB192EE94__INCLUDED_)
#define AFX_KREATORTRAJEKTORIJAMDIDOC_H__5C4D2975_7374_4025_B987_14ECB192EE94__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CKreatorTrajektorijaMDIDoc : public CDocument
{
protected: // create from serialization only
	CKreatorTrajektorijaMDIDoc();
	DECLARE_DYNCREATE(CKreatorTrajektorijaMDIDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKreatorTrajektorijaMDIDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CKreatorTrajektorijaMDIDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CKreatorTrajektorijaMDIDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_KREATORTRAJEKTORIJAMDIDOC_H__5C4D2975_7374_4025_B987_14ECB192EE94__INCLUDED_)
