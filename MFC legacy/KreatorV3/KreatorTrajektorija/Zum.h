#if !defined(AFX_ZUM_H__A1198C43_EDE2_43F8_9AE4_916D1A54A815__INCLUDED_)
#define AFX_ZUM_H__A1198C43_EDE2_43F8_9AE4_916D1A54A815__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Zum.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CZum dialog

class CZum : public CDialog
{
// Construction
public:
	CZum(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CZum)
	enum { IDD = IDD_ZUM };
	int		m_Zumx;
	int		m_Zumy;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CZum)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CZum)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ZUM_H__A1198C43_EDE2_43F8_9AE4_916D1A54A815__INCLUDED_)
