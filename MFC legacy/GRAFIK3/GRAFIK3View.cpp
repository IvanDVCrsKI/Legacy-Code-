// GRAFIK3View.cpp : implementation of the CGRAFIK3View class
//

#include "stdafx.h"
#include "GRAFIK3.h"

#include "Math.h"
#include "Performanse_1.h"
#include "GeoML.h"
#include "CRMM.h"






#include "GRAFIK3Doc.h"
#include "GRAFIK3View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK3View

IMPLEMENT_DYNCREATE(CGRAFIK3View, CView)

BEGIN_MESSAGE_MAP(CGRAFIK3View, CView)
	//{{AFX_MSG_MAP(CGRAFIK3View)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK3View construction/destruction

CGRAFIK3View::CGRAFIK3View()
{
	m_Mreza = 1;
	m_DGrafik = 0;
	m_Zumx= 20;
	m_Zumy= 20;
	m_Korak = 10;
	m_IDev = 100000;
	m_GMreza = 5000;

	m_viewx = 1;
	m_viewy = -1;

	m_Orgfx =90;
	m_Orgfy =620;

	dx = 1;
	m_paint = 1;




	m_pp1 = 1;

   nWidth =0;
   nHeight =0;
   Px = 0; //nema promene u pocetnom trenutku P=0
   Py = 0;
   P = 0;
   dxzum =0; 
   dyzum =0; 

   dxs =0;
   dys =0;
   x =0;
   y=0;
  
 

}

CGRAFIK3View::~CGRAFIK3View()
{
}

BOOL CGRAFIK3View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK3View drawing

void CGRAFIK3View::OnDraw(CDC* pDC)                //(CDC* pDC)
{
	//CMemDC pDC(dc);
	
    //CMemDC pDC(pDC, &rcBounds);
	CGRAFIK3Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;
	CRect rect;
	CBrush brsBlack, *brsOld;

	GetClientRect(&rect);
	brsBlack.CreateSolidBrush(RGB(0, 0, 0));

	
//----------------------------------------------------------
// POKRETNE LINIJE NA POZICIJU MISA
//----------------------------------------------------------
 pDC->MoveTo(point.x-4, 0 );//LINIJA ZA PRACENJE X OSE  
 pDC->LineTo(point.x-4, point.y+10000);

 pDC->MoveTo(0,point.y-40);//LINIJA ZA PRACENJE Y OSE
 pDC->LineTo(point.x+10000, point.y-40);
//----------------------------------------------------------


	CRect Recto; //PRAVOUGAONIK EKRANA
    GetClientRect (&Recto);//PRAV. KLIJENTA TJ KORISNIKA I NJEGOVE REZOLUCIJE

	nWidth  =Recto.Width();//int MERA SIRINE
    nHeight = Recto.Height();// int MERA DUZINE

GetCursorPos(&point);// ZADNJA POZICIJA MISA KORISNIKA



//----------------------------------------------------------	     
//POZICIJA TEKSTA NA KORDINATU EKRANA!!! KOS1 FIKSNA POZICIJA
//----------------------------------------------------------
char t1[10];
sprintf(t1, "Pozicija  (%d,%d)",point.x,  point.y);
pDC->TextOut (nWidth/1.2,nHeight/5 ,t1);

char t2[10];
sprintf(t2, "ORG (%dx,%dy)",m_Orgfx,  m_Orgfy);
pDC->TextOut (nWidth/1.2,nHeight/6 ,t2);

char t3[10];
sprintf(t3, "Zum (%dx,%dy)",m_Zumx,  m_Zumy);
pDC->TextOut (nWidth/1.2,nHeight/8,t3);

char t4[10];
sprintf(t4, "(X: %d Y: %d)",xLogical+dxzum,yLogical+dyzum);
pDC->TextOut (nWidth/1.2,nHeight/10 ,t4);



int xLogical1= (point.x-m_Orgfx)/(m_viewx/m_Zumx);
int yLogical1 =(point.y-m_Orgfy)/(m_viewy/m_Zumy);

	 int sxLogical1=xLogical1+dxzum;

    int  syLogical1=yLogical1+dyzum;
//Invalidate();

dxg =sxLogical1;
dyg =syLogical1;


char t5[10];
sprintf(t5, "(dX: %d dY: %d)",dxg,dyg);
pDC->TextOut (nWidth/1.2,nHeight/13 ,t5);
//----------------------------------------------------------


//----------------------------------------------------------
//LINIJE NA CENTRU NEMOBILAN PO PRESEKU REZOLUCIJE EKRANA	
//----------------------------------------------------------
	CPen PenBlue2(PS_SOLID, 1, RGB(0, 0, 255));
	pDC->SelectObject(PenBlue2);

    pDC->MoveTo(nWidth / 2, 0);  //LINIJA ZA PRIKAZ POLOVINE X EKRANA
	pDC->LineTo(nWidth / 2, nHeight);
	pDC->MoveTo(0, nHeight/ 2);//LINIJA ZA PRIKAZ POLOVINE Y EKRANA
	pDC->LineTo(nWidth, nHeight / 2);
//----------------------------------------------------------


//----------------------------------------------------------
//POKRETNI TEKST NA POZICIJU MISA X I Y	
//----------------------------------------------------------	
	pDC->SetTextColor(RGB(100, 100, 0)); 

    pDC->TextOut(point.x-100, point.y-50, 'X');
	pDC->TextOut(point.x-4, point.y+50, 'Y');
   //////////////////////////////////////////////////////
/*
   MSG msg;
  
        if (!AfxGetApp ()->PumpMessage ()) {
            ::PostQuitMessage (0);
         
        }
    LONG lIdle = 0;
    while (AfxGetApp ()->OnIdle (lIdle++));
    return TRUE;

*/


   pDC->SetMapMode(MM_ANISOTROPIC); //orijentacija osa PO ZELJI PROGRAMERA



pDC->SetViewportOrg(m_Orgfx, m_Orgfy);

/*
To control your own unit system, the orientation of the axes or how the 
application converts the units used on your application, use either the 
MM_ISOTROPIC or the MM_ANISOTROPIC map modes.
*/


   //  //90 -620  definise pocetnu referentnu tacku na ekranu  


/*
Since you are drawing on a device context, all you need to do is simply call
the CDC::SetViewportOrg() method. It is overloaded with two versions, which 
allow you to use either the X and the Y coordinates or a defined point. 
 
   The syntaxes of this method are:

SetViewportOrg(int X, int Y);
SetViewportOrg(CPoint Pt);


*/

	pDC->SetWindowExt(m_Zumx,m_Zumy); //SetWindowExtent(100,100)  SetViewportExtent(200,200)  100 jedinica = 200 pixela
/*
CSize SetWindowExt(int cx, int cy);
CSize SetWindowExt(SIZE size);

Therefore, after calling SetMapMode() and specifying the MM_ISOTROPIC (or MM_ANISOTROPIC),
you must call the CDC:SetWindowExt() method. This method specifies how much each new unit
will be multiplied by the old or default unit system.


*/ //1 m = 2 pixela
	


	pDC->SetViewportExt(m_viewx,m_viewy);//1 -1
/*
CSize SetViewportExt(int cx, int cy);
CSize SetViewportExt(SIZE size);



To use the first version of this function, you must provide the units of device conversion 
as cx for the horizontal axis and as cy for the vertical axis.



----------------------------------
Logical coordinates  //jedinice npr metri...
----------------------------------
(also referred to as page coordinates ) are determined
by the mapping mode. For example, the MM_LOENGLISH mapping mode has
logical coordinates in units of 0.01 inches, with the origin in the top left corner
of the client area, and the positive y axis direction running from bottom to top.

----------------------------------
Device coordinates   //pikseli
----------------------------------
(also referred to as client coordinates  in a window) are
measured in pixels in the case of a window, with the origin at the top left corner
of the client area, and with the positive y axis direction from top to bottom.
These are used outside of a device context, for example for defining the
position of the cursor in mouse message handlers.


----------------------------------
Screen coordinates 
----------------------------------
are measured in pixels and have the origin at the top left
corner of the screen, with the positive y axis direction from top to bottom.
These are used when getting or setting the cursor position.


The formulae that are used by Windows to convert from logical coordinates to device
coordinates are:


xDevice = (xLogical-xWindowOrg)*(xViewPortExt/xWindowExt)+xViewportOrg

yDevice = (yLogical-yWindowOrg)*(yViewPortExt/yWindowExt)+yViewportOrg



pDC->SetViewportOrg(m_Orgfx, m_Orgfy); ViewOrg pozicija x,y
pDC->SetWindowExt(m_Zumx,m_Zumy);   winExt je zum
pDC->SetViewportExt(m_viewx,m_viewy); orijentacija osa
pDC->SetWindowOrg




*/	
//PRETVARANJE POZICIJA MISA U LOGICKE KOORDINATE ZA PRIKAZ NA SEKUNDARNOM KOS
   xLogical= (point2.x-m_Orgfx)/(m_viewx/m_Zumx);
   yLogical =(point2.y-m_Orgfy)/(m_viewy/m_Zumy);

	 int sxLogical=xLogical+dxzum;

    int  syLogical=yLogical+dyzum;




CPen PenGreen2(PS_SOLID, 10, RGB(0,255, 0));
pDC->SelectObject(PenGreen2);



     pDC->MoveTo(sxLogical, 0 );
     pDC->LineTo(sxLogical, syLogical);

     pDC->MoveTo(0, syLogical );
     pDC->LineTo(sxLogical, syLogical);

//=---------------------------------------------------------

/*zum je 10 uvecannje je za 500 po x i y osi
	     11 550
         12 600
         ...
  za zum 20 uvecanje je 1000 x y

  za svaki 1 zum uvecanje je za 50 m_orgx po x ili y pravcu

dx = 1000
zum 20

dzum = 1000/20 = 50

xLogic = F(x)
za svaku promenu zuma ili pozicije x y treba dodati ili oduzeti funkciju od zuma

-u pocetnom trenutku vrednost treba da bude 0
- nakon pomene oduzeti ili dodati vrednost F(zum)

  1.pre promene

  xLogic = F(x)+0

  2.nakon promene

  xLogic = F(x)+-F(zum)

  F(zum) = dx


  dx = 50*20; jeste 1000 tacno

  dx = 50*m_zumx

  ideja uvesti varijablu promene

  dx = 50*m_zumx*P
  
	P=1,0

  P = 0 u pocetnom
  P = -1,+1 u zavisnosti od smera kretanja 
RESENO POMERENJE POMOCU TASTATURE

*/


/*
	 PRILIKOM ZUMA 
	 zum je ekvivalentan m_Zum*(trenutna_pozicija)*P

  trenutna_pozicija misa 

  m_Zum   pozicija
   1           251  
   2           2*251 = 502
   3           753
    ...        ...
   10          2510

  xLog =m_Zumx*(point.x)*P
  yLog =m_Zumy*(point.y)*P

  DukupnoX = dxzum+dxs
  DukupnoX = 50*m_zumx*P+m_Zumx*(point.x)*P


	 
	 
	 
 */






   xLogical2= (point3.x-m_Orgfx)/(m_viewx/m_Zumx);
   yLogical2 =(point3.y-m_Orgfy)/(m_viewy/m_Zumy);



   	 int sx2Logical=xLogical2+dxzum;

    int  sy2Logical=yLogical2+dyzum;

dxs =sx2Logical;
dys =sy2Logical;

CPen PenOrange(PS_SOLID, 10, RGB(100,100, 0));
pDC->SelectObject(PenOrange);



     pDC->MoveTo(dxs, 0 );
     pDC->LineTo(dxs, dys);

     pDC->MoveTo(0, dys );
     pDC->LineTo(dxs, dys);
/*
*/
//----------------------------------------------------------
// OSE
	CPen PenBlack40(PS_SOLID, 50, RGB(0, 0, 0));
	pDC->SelectObject(PenBlack40);
	pDC->MoveTo(-1000,     0);
	pDC->LineTo( 20000000,     0);
	pDC->MoveTo(   0, -1000);
	pDC->LineTo(   0,  20000000);


//----------------------------------------------------------



//----------------------------------------------------------	
//POZICIJA OZNAKA MREZE LENJIRA
//----------------------------------------------------------	
	

    
switch (m_paint)
 {


		case 0 :

		break;


	case 1 :
			
		CPen PenBlack3(PS_SOLID, 3, RGB(0, 0, 0));
	pDC->SelectObject(PenBlack3);
    
	for(int koox = 0; koox < 100000; koox += 1000)
    {
        pDC->MoveTo(koox, 0);
        pDC->LineTo(koox, -1000);
    }

	for(int kooy = 0; kooy < 100000; kooy += 1000)
    {
        pDC->MoveTo(0, kooy);
        pDC->LineTo(-1000, kooy);
    }



	pDC->SelectObject(PenBlack3);
    for(int koox2 = 0; koox2 < 100000; koox2 += 5000)
    {
        pDC->MoveTo(koox2, 0);
        pDC->LineTo(koox2, -2000);
    }
	for(int kooy2 = 0; kooy2 < 100000; kooy2 += 5000)
    {
        pDC->MoveTo(0, kooy2);
        pDC->LineTo(-2000, kooy2);
    }

	for(int koox3 = 0; koox3 < 100000; koox3 += 10000)
    {
        pDC->MoveTo(koox3, 0);
        pDC->LineTo(koox3, -3000);
    }
	for(int kooy3 = 0; kooy3 < 100000; kooy3 += 10000)
    {
        pDC->MoveTo(0, kooy3);
        pDC->LineTo(-3000, kooy3);
    }

    pDC->SetTextColor(RGB(0, 0, 0));
	for (int kt1= 0; kt1<100000; kt1+=10000) 
	{
        
        CString string;
        string.Format (("%d "), (kt1 ) );
        pDC->TextOut (kt1, -3000, string);
    }

	pDC->SetTextColor(RGB(0, 0, 0));
	for (int kt2= 1000; kt2<100000; kt2+=10000) 
	{
        
        CString string;
        string.Format (("%d"), (((kt2-1000)) ) );
        pDC->TextOut (-6000, kt2, string);
    }
	//Invalidate();
         break;
	
	

			
	
	


		
}
	
	//Lenjiri po x i y osi
	//////////////////////////////////////////////////////	

    



//----------------------------------------------------------
//MREZA
	
	CPen PenBlack4(PS_SOLID, 1, RGB(0, 0, 0));
	pDC->SelectObject(PenBlack4);
	
	switch (m_Mreza =0)//PRIKAZ MREZE DA NE 1 0
	
	{
	case 0:
    
	

	break;

	case 1:
		for(int kooxx = 0; kooxx < 1000000; kooxx += m_GMreza)
    {
        pDC->MoveTo(kooxx, 0);
        pDC->LineTo(kooxx, 50000);
    }
	

	for(int kooyy = 0; kooyy < 1000000; kooyy += m_GMreza)
    {
        pDC->MoveTo(0, kooyy);
        pDC->LineTo(150000, kooyy);
    }
		break;
	}
//----------------------------------------------------------

	

//KONSTANTE ZA PRORACUNE U VIEW DOMENU
//////////////////////////////////////////////////////	
double PI = 3.1415926;
double g = 9.81;
//////////////////////////////////////////////////////	
//----------------------------------------------------------




//----------------------------------------------------------
//KORAK I MAKSIMUM
//----------------------------------------------------------	

double kor = m_Korak;//1

double max = m_IDev; //oko milion
//----------------------------------------------------------	



//----------------------------------------------------------	
//FUNKCIJE
//----------------------------------------------------------
/*  sinusna funkcija 

	  double x = i+dx;
		 double y = (1000)*sin((PI*(x))/180);
		 pDC->SetPixel(x, y, RGB(255, 0, 0));*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////		
//Funkcija CRVENA
//DEFINISATI STA SE MENJA U FUNKCIJI OD CEGA U PARAMETAR PROMENE UBACITI i  ZA LOOP  
////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//for(double i = 1; i < max; i +=kor)


        
PERF par;
CCRM fluid;
GEOML geo;
    
x =fluid.CCRMgetKapa(); 
y = par.PERFgetPotisak();


		 pDC->SetPixel(x, y, RGB(255, 0, 0));





        

}


/////////////////////////////////////////////////////////////////////////////
// CGRAFIK3View diagnostics

#ifdef _DEBUG
void CGRAFIK3View::AssertValid() const
{
	CView::AssertValid();
}

void CGRAFIK3View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CGRAFIK3Doc* CGRAFIK3View::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CGRAFIK3Doc)));
	return (CGRAFIK3Doc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CGRAFIK3View message handlers



   


void CGRAFIK3View::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	switch (nChar)
 {
	case VK_RIGHT :
			m_Orgfx-=50;
			m_pp1 = 0;
			xLogical=+50;
			
			Px=1;
		    dxzum -=50*m_Zumx*Px;
			Px=0;
		;
            //int dyzum =50*m_Zumy*Py; 
   
			
	
			Invalidate();
         break;

	case VK_LEFT :
		m_Orgfx+=50;
			m_pp1 = 0;
			xLogical=-50;
			Px=1;
			dxzum +=50*m_Zumx*Px;
			Px=0;
		
		
	
			Invalidate();
		break;

	case VK_UP :
		m_Orgfy+=50;
			m_pp1 = 0;
			Py=1;
			dyzum -=50*m_Zumy*Py;
			Py=0;


	
	
			Invalidate();
		break;

    case VK_DOWN:
		m_Orgfy-=50;
			m_pp1 = 0;
			Py=1;
			dyzum +=50*m_Zumy*Py;
			Py=0;



		
		Invalidate();
		break;
    
	case VK_HOME:
		m_Orgfx = 90;
		m_Orgfy = 620;
		m_Zumx= 60;
        m_Zumy = 60;
			m_pp1 = 0;
		Invalidate();
		break;

	case VK_F2:
		m_Zumx +=10;
		m_Zumy +=10;
			m_pp1 = 0;
			dxzum =0;
			dyzum =0;
	
		Invalidate();
		break;

	case VK_F1:
		m_Zumx -=10;
		m_Zumy -=10;
		m_pp1 = 0;
		dxzum =0;
		dyzum =0;
	
		if (m_Zumx < 1)
		{
			m_Zumx =1;
			m_Zumy =1;
		}
			
		Invalidate();
		break;

	case VK_F3:
		m_Zumx -=1;
		m_Zumy -=1;
		
		if (m_Zumx < 1)
		{
			m_Zumx =1;
			m_Zumy =1;
		}
			m_pp1 = 0;
		Invalidate();
		break;

	case VK_F4://f4
		m_Zumx +=1;
		m_Zumy +=1;
		m_pp1 = 0;
	
		Invalidate();
		break;



	case 0x51://q
	
		dx +=100;
		m_pp1 = 0;
	
		Invalidate();
		break;

	case 0x57://w
		
		dx -=100;
		m_pp1 = 0;
	
		Invalidate();
		break;
	

}
       
	
	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}




void CGRAFIK3View::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
		switch (nChar)
 {
	case VK_RIGHT :
			
			m_pp1 = 1;
			Px=0;
		    //dxzum -=50*m_Zumx*Px; 
            //int dyzum =50*m_Zumy*Py; 
			

			
	
			Invalidate();
         break;

	case VK_LEFT :
	m_pp1 = 1;
	Px=0;

	
		
	
			Invalidate();
		break;

	case VK_UP :
	m_pp1 = 1;
	Py=0;
	
	
	
			Invalidate();
		break;

    case VK_DOWN:
		m_pp1 = 1;
		Py=0;
	
		
		Invalidate();
		break;
    
	case VK_HOME:
	m_pp1 = 1;
		Invalidate();
		break;

	case VK_F2:
	m_pp1 = 1;
	
		Invalidate();
		break;

	case VK_F1:
	m_pp1 = 1;
	P=1;
	dxs -=m_Zumx*(point3.x)*P;
    dys -=m_Zumy*(point3.y)*P;
		
	
		break;

	case VK_F3:
     m_pp1 = 1;
		Invalidate();
		break;

	case VK_F4://f4
	m_pp1 = 1;
	
		Invalidate();
		break;



	case 0x51://q
	
	m_pp1 = 1;
	
		Invalidate();
		break;

	case 0x57://w
		
	m_pp1 = 1;
	
		Invalidate();
		break;
	

	


	case 0x41://a
	
			m_viewx -=1;
		

	
		Invalidate();
		break;

	case 0x53://s
		
		m_viewx +=1;
		

	
		Invalidate();
		break;

	case 0x5a://z
	
		m_viewy -=1;

	
		Invalidate();
		break;

	case 0x58://x
	
         m_viewy +=1;
	
		Invalidate();
		break;


	case 0x1b://escape
	
       exit(0);  
	
		Invalidate();
		break;
/*
	case 0x56://v
		
		dugao2-=1;

	
		Invalidate();
		break;
		*/
	


}
       
	CView::OnKeyUp(nChar, nRepCnt, nFlags);
}

void CGRAFIK3View::OnMouseMove(UINT nFlags, CPoint point) 
{
	 
      //previous = point;
 
	::SetCursor(::LoadCursor(NULL, IDC_CROSS));
m_paint =0;
	Invalidate();
		m_paint =1;
	

CView::OnMouseMove(nFlags, point);
}

void CGRAFIK3View::OnLButtonDown(UINT nFlags, CPoint point) 
{
      

	
	point2.x= point.x;
	point2.y= point.y;
	dxzum =0;
    dyzum =0;
    


	CView::OnLButtonDown(nFlags, point);

	Invalidate();
}

void CGRAFIK3View::OnLButtonDblClk(UINT nFlags, CPoint point) 
{

    m_Orgfx= point.x;
	m_Orgfy= point.y;

	
	CView::OnLButtonDblClk(nFlags, point);
}

void CGRAFIK3View::OnRButtonDown(UINT nFlags, CPoint point) 
{
	
point3.x =point.x;
point3.y =point.y;

dxzum =0;
dyzum =0;


	CView::OnRButtonDown(nFlags, point);
	Invalidate();
}






double CGRAFIK3ViewgetX()
{
	int x = 1;
	return x;
}
/*
BOOL CGRAFIK3View::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default
	//Invalidate();

	return true;
}
*/


