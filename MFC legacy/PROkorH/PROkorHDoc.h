// PROkorHDoc.h : interface of the CPROkorHDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROKORHDOC_H__E4467171_8AA4_4FF3_9513_28E0EAE7C30D__INCLUDED_)
#define AFX_PROKORHDOC_H__E4467171_8AA4_4FF3_9513_28E0EAE7C30D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CPROkorHDoc : public CDocument
{
protected: // create from serialization only
	CPROkorHDoc();
	DECLARE_DYNCREATE(CPROkorHDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPROkorHDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CPROkorHDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CPROkorHDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROKORHDOC_H__E4467171_8AA4_4FF3_9513_28E0EAE7C30D__INCLUDED_)
