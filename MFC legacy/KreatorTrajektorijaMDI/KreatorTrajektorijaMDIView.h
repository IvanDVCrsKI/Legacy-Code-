// KreatorTrajektorijaMDIView.h : interface of the CKreatorTrajektorijaMDIView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_KREATORTRAJEKTORIJAMDIVIEW_H__BDFFB496_D19B_4CB3_BF89_620F87CCFC39__INCLUDED_)
#define AFX_KREATORTRAJEKTORIJAMDIVIEW_H__BDFFB496_D19B_4CB3_BF89_620F87CCFC39__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CKreatorTrajektorijaMDIView : public CView
{
protected: // create from serialization only
	CKreatorTrajektorijaMDIView();
	DECLARE_DYNCREATE(CKreatorTrajektorijaMDIView)

// Attributes
public:
	CKreatorTrajektorijaMDIDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKreatorTrajektorijaMDIView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CKreatorTrajektorijaMDIView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CKreatorTrajektorijaMDIView)
	afx_msg void OnUputstvo();
	afx_msg void OnOpcijePrikazInfotabla();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in KreatorTrajektorijaMDIView.cpp
inline CKreatorTrajektorijaMDIDoc* CKreatorTrajektorijaMDIView::GetDocument()
   { return (CKreatorTrajektorijaMDIDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_KREATORTRAJEKTORIJAMDIVIEW_H__BDFFB496_D19B_4CB3_BF89_620F87CCFC39__INCLUDED_)
