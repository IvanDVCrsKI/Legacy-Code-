// TESTFL.h : main header file for the TESTFL application
//

#if !defined(AFX_TESTFL_H__CC5F497B_D22F_48A3_9AAF_2A03F9D7D913__INCLUDED_)
#define AFX_TESTFL_H__CC5F497B_D22F_48A3_9AAF_2A03F9D7D913__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CTESTFLApp:
// See TESTFL.cpp for the implementation of this class
//

class CTESTFLApp : public CWinApp
{
public:
	CTESTFLApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTESTFLApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CTESTFLApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTFL_H__CC5F497B_D22F_48A3_9AAF_2A03F9D7D913__INCLUDED_)
