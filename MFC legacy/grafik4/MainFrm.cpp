// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "GRAFIK3.h"
#include "SplashWnd.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_MOUSEMOVE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	
}

CMainFrame::~CMainFrame()
{
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	//::MessageBox(NULL, "OLE", "", MB_OK | MB_ICONQUESTION); //salje 2 poruke ?


	

	if( !CMDIFrameWnd::PreCreateWindow(cs) )
		return FALSE;

	
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;//TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWnd::AssertValid();

	//za haos u sistemu ukloniti kose crte  :) 
	//::ShellExecute(this->m_hWnd,"open","C:\\Program Files\\VideoLAN\\VLC\\vlc.exe","E:\\muzika\\x\\101-snake-eater.mp3","",SW_SHOW );
}

void CMainFrame::Dump(CDumpContext& dc) const
{	
	CMDIFrameWnd::Dump(dc);

}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers


void CMainFrame::OnMouseMove(UINT nFlags, CPoint point) 
{
	
	CMDIFrameWnd::OnMouseMove(nFlags, point);
}

BOOL CMainFrame::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{

	return CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);
}
