; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CCKalkulator
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Pirol.h"
LastPage=0

ClassCount=7
Class1=CPirolApp
Class2=CPirolDoc
Class3=CPirolView
Class4=CMainFrame

ResourceCount=4
Resource1=IDR_PIROLTYPE
Resource2=IDR_MAINFRAME
Class5=CChildFrame
Class6=CAboutDlg
Resource3=IDD_ABOUTBOX
Class7=CCKalkulator
Resource4=IDD_DIALOG1

[CLS:CPirolApp]
Type=0
HeaderFile=Pirol.h
ImplementationFile=Pirol.cpp
Filter=N

[CLS:CPirolDoc]
Type=0
HeaderFile=PirolDoc.h
ImplementationFile=PirolDoc.cpp
Filter=N

[CLS:CPirolView]
Type=0
HeaderFile=PirolView.h
ImplementationFile=PirolView.cpp
Filter=C
LastObject=CPirolView
BaseClass=CScrollView
VirtualFilter=VWC


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T


[CLS:CChildFrame]
Type=0
HeaderFile=ChildFrm.h
ImplementationFile=ChildFrm.cpp
Filter=M
LastObject=CChildFrame


[CLS:CAboutDlg]
Type=0
HeaderFile=Pirol.cpp
ImplementationFile=Pirol.cpp
Filter=D
LastObject=CAboutDlg

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_MRU_FILE1
Command4=ID_APP_EXIT
Command5=ID_VIEW_TOOLBAR
Command6=ID_APP_ABOUT
CommandCount=6

[TB:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_CUT
Command5=ID_EDIT_COPY
Command6=ID_EDIT_PASTE
Command7=ID_FILE_PRINT
Command8=ID_APP_ABOUT
CommandCount=8

[MNU:IDR_PIROLTYPE]
Type=1
Class=CPirolView
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_CLOSE
Command4=ID_FILE_SAVE
Command5=ID_FILE_SAVE_AS
Command6=ID_FILE_MRU_FILE1
Command7=ID_APP_EXIT
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_VIEW_TOOLBAR
Command13=ID_WINDOW_NEW
Command14=ID_WINDOW_CASCADE
Command15=ID_WINDOW_TILE_HORZ
Command16=ID_WINDOW_ARRANGE
Command17=ID_APP_ABOUT
Command18=IDM_KALKULATOR
CommandCount=18

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_UNDO
Command5=ID_EDIT_CUT
Command6=ID_EDIT_COPY
Command7=ID_EDIT_PASTE
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_NEXT_PANE
Command13=ID_PREV_PANE
CommandCount=13

[DLG:IDD_DIALOG1]
Type=1
Class=CCKalkulator
ControlCount=12
Control1=IDOK,button,1342242816
Control2=IDCANCEL,button,1342242816
Control3=IDC_BUTTON_ADD,button,1342242817
Control4=IDC_ADD1,edit,1350631552
Control5=IDC_ZNAK,edit,1350631552
Control6=IDC_ADD2,edit,1350631552
Control7=IDC_SUM,edit,1350631552
Control8=IDC_HISTORY,listbox,1352728835
Control9=IDC_STATIC,static,1342312460
Control10=IDC_STATIC,static,1342308352
Control11=IDC_STATIC,static,1342308352
Control12=IDC_CMD,button,1342242816

[CLS:CCKalkulator]
Type=0
HeaderFile=CKalkulator.h
ImplementationFile=CKalkulator.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDC_SUM

