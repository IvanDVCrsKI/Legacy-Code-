#include <windows.h>
#include "resource.h"
LPCTSTR ClsName = "BasicApp";
LPCTSTR WndName = "URAAAAAAAAAAAAAAAAAAAAAAAAAAA";

LRESULT CALLBACK WndProcedure(HWND hWnd, UINT uMsg,
			      WPARAM wParam, LPARAM lParam);

INT WINAPI WinMain(HINSTANCE hInstance, 
				   HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine, 
			       int nCmdShow)
{
	MSG        Msg;
	HWND       hWnd;
	WNDCLASSEX WndClsEx;
// Create the application window
	WndClsEx.cbSize        = sizeof(WNDCLASSEX);                     //1)
	WndClsEx.style         = CS_HREDRAW | CS_VREDRAW;                //2)
	WndClsEx.lpfnWndProc   = WndProcedure;                           //3)
	WndClsEx.cbClsExtra    = 0;                                      //4)
	WndClsEx.cbWndExtra    = 0;                                      //5)
	WndClsEx.hIcon         = LoadIcon(NULL, IDI_APPLICATION);        //6)
	WndClsEx.hCursor       = LoadCursor(hInstance,IDC_ARROW);       //7)
	WndClsEx.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);  //8)
	WndClsEx.lpszMenuName  = MAKEINTRESOURCE(IDR_MENU1);                                   //9)
	WndClsEx.lpszClassName = ClsName;                                //10)
	WndClsEx.hInstance     = hInstance;                              //11)
	WndClsEx.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);        //12)-6)
// Register the application
	RegisterClassEx(&WndClsEx);
// Create the window object 
	hWnd = CreateWindow(              // hWnd = CreateWindow(. . .);
ClsName,                        //1)name of class
WndName,                        //2)name of window "Ura"
WS_OVERLAPPEDWINDOW| WS_VISIBLE,            //3)style
CW_USEDEFAULT,                  //4)distance of window from left side of screen
CW_USEDEFAULT,                  //5)distance of window from top of screen
CW_USEDEFAULT,                  //6)width of window
CW_USEDEFAULT,                  //7)height of window
NULL,                           //8)handle to parent of this window
NULL,                           //9)handle to menu
hInstance,                      //10)handle to instance of application
NULL);                          //11)window creation data
// Find out if the window was created
	if( !hWnd ) // If the window was not created,
		return 0; // stop the application

	// Display the window to the user
	ShowWindow(hWnd, SW_SHOWNORMAL);
	UpdateWindow(hWnd);

	// Decode and treat the messages
	// as long as the application is running
	while( GetMessage(&Msg, NULL, 0, 0) )   // ili while(GetMessage(&Msg, NULL, 0, 0) > 0)
	{
             TranslateMessage(&Msg);
             DispatchMessage(&Msg);
	}

	return Msg.wParam;
}

LRESULT CALLBACK WndProcedure(HWND hWnd, UINT Msg,
			   WPARAM wParam, LPARAM lParam)
{
	//static variables used to keep track of the ball�s position
	//static  Declares that the member is static in a class.
 
     static int dX = 1, dY = 1; //stores direction BRZINAAAAAAAAA
     
     static int x = 3, y = 3, oldX = 2, oldY =2;//stores position
     const int i = 1;
	 //device context and brush used for drawing i ostale komponente

	HDC hDC;//deklaracija hendla za hDC
    PAINTSTRUCT Ps;//deklaracija za typedef struct tagPAINTSTRUCT 
	HBRUSH brush;


    switch(Msg)
    {
  
    case WM_CREATE:
		MessageBox(NULL, "Timer function has been added", "Info",MB_ICONINFORMATION | MB_OK);//MSG box INFO
		SetTimer(hWnd, 1, 20, NULL);// handle hWnd ,TAJMER 1, 20 ms (0.02 s), TIMERPROC lpTimerFunc je NULLbreak;

		
    case WM_ACTIVATE:
		 break;

    case WM_TIMER: 
		
               hDC = GetDC(hWnd);//get the dc for drawing
               
               brush = (HBRUSH)SelectObject(hDC,GetStockObject(BLACK_BRUSH));//use 
               
               RECT SSSR;//fill a RECT object with the appropriate values 
			   //------ typedef struct tagRECT uzima 4 parametra 
			   //LONG    left;
               //LONG    top;
               //LONG    right;
               //LONG    bottom;
               SSSR.left = oldX;
               SSSR.top = oldY;
               SSSR.right = oldX ;
               SSSR.bottom = oldY;
               
               
               FillRect(hDC, &SSSR, brush);//cover the old ellipse
               
               brush = (HBRUSH)SelectObject(hDC,GetStockObject(BLACK_BRUSH));//get ready to draw the new ellipse
               

               LineTo(hDC, 10+x,10+y);//draw it
               Arc(hDC, 100, 100, 1+x, 1+y, x, y, 1+x, 1+y);
               oldX = x;//update the values
               oldY = y;
               
               x += dX;//prep the new coordinates for next time
               y += dY;
			   
               
               /*RECT rect;//get the window size and store it in rect
               GetClientRect(hWnd, &rect);
               
               if(x + 30 > rect.right || x < 0)//if the circle is going off the edge thenreverse its direction
               {
                    dX = -dX;
               }
               if(y + 30 > rect.bottom || y < 0)
               {
                    dY = -dY;
               }
			   */
               SelectObject(hDC, brush);//put the old brush back
               ReleaseDC(hWnd, hDC);//release the dc
               break;

		break;

    case WM_SHOWWINDOW:
		 break;

    case WM_SIZE:
		 break;

    case WM_MOVE:
	     break;

	case WM_PAINT:
		break;
	
	case WM_DESTROY:
      	KillTimer(hWnd, 1);//handle HWnd, Unisti TAJMER 1
        PostQuitMessage(WM_QUIT);//PostQuitMessage(int nExitCode) = WM_QUIT
        break;
	

	
	case WM_COMMAND:// komanda da bi se koristio meni
		{
	switch(LOWORD(wParam))
		{
	      case IDM_FILE_EXIT://ako se klikne na file >>> EXIT onda uradi to
		  PostQuitMessage(WM_QUIT);
		  break;
		  
		  case IDM_OBJEKAT_P://komanda u meniju za OBJEKAT_P ne definisan
	      MessageBox(hWnd, "No function added for IDM_OBJEKAT_P", "Error", MB_ICONERROR | MB_OK);
          break;

		  //TJ hDC = BeginPaint TEK CE SADA DA SE AKTIVIRA ANIMACIJA
		  case IDM_OBJEKAT_PP://komanda u meniju za slanje poruke za kreiranje pravougaonika I animacije
	      hDC = BeginPaint(hWnd, &Ps);
          Rectangle(hDC, 200, 200, 300, 300);
          BeginPaint(hWnd, &Ps);// HWND hWnd, LPPAINTSTRUCT lpPaint
          break;

		  }
		}
		

    default:
        // Process the left-over messages
        return DefWindowProc(hWnd, Msg, wParam, lParam);
    }
    // DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
    return 0;
}
