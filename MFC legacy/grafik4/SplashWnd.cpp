// SplashWnd.cpp : implementation file
//
// �1998-2001 Codejock Software, All Rights Reserved.
// Based on the Visual C++ splash screen component.
//
// support@codejock.com
// http://www.codejock.com
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SplashWnd.h"

#include "Resource.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
//   Splash Screen class

BOOL        CSplashWnd::m_bShowSplashWnd;
CSplashWnd* CSplashWnd::m_pSplashWnd;

CSplashWnd::CSplashWnd()
{
}

CSplashWnd::~CSplashWnd()
{

	m_pSplashWnd = NULL;
}

BEGIN_MESSAGE_MAP(CSplashWnd, CWnd)
	//{{AFX_MSG_MAP(CSplashWnd)
	ON_WM_PAINT()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CSplashWnd::EnableSplashScreen(BOOL bEnable /*= TRUE*/)
{
	m_bShowSplashWnd = bEnable;
}

BOOL CSplashWnd::ShowSplashScreen(UINT uTimeOut,CWnd* pParentWnd)
{
	m_pSplashWnd = new CSplashWnd;

	if (!m_pSplashWnd->m_bitmap.LoadBitmap(BANNER)) 
	{
		return FALSE;
	}
	/**/
	BITMAP bm;
	m_pSplashWnd->m_bitmap.GetBitmap(&bm);

	CString strWndClass = AfxRegisterWndClass(CS_NOCLOSE ,LoadCursor(NULL,IDC_ARROW));
/**/

	m_pSplashWnd->
		CreateEx(
		0,
		strWndClass, 
		NULL, 
	    SW_MAX|WS_POPUP|WS_VISIBLE, //Window Styles WS_VISIBLE WS_POPUP -uklanja osnovnu strukturu
		0, //Additional Memory Request
		0, //Extra Memory
		bm.bmWidth, //sirina prozora jednaka je sirini slicice
		bm.bmHeight, //visina prozora jednaka je visina slicice pParentWnd->GetSafeHwnd()
		pParentWnd->GetSafeHwnd(),  //ako se stavi null oba prozora se aktiviraju u isto vreme
		NULL);

	m_pSplashWnd->ShowWindow ( SW_SHOW );
	m_pSplashWnd->UpdateWindow();
	m_pSplashWnd->CenterWindow();// dialog support prikazuje prozor na centru ekrana


	return TRUE;
}
/**/

BOOL CSplashWnd::PreTranslateAppMessage(MSG* pMsg)
{
	if (m_pSplashWnd == NULL)
		return FALSE;

	return FALSE;	// message not handled
}

void CSplashWnd::HideSplashScreen()
{
	
	m_pSplashWnd->DestroyWindow();

}

void CSplashWnd::OnPaint()
{
	CPaintDC dc(this);

	CDC dcImage;
	if (dcImage.CreateCompatibleDC(&dc))
	{
		BITMAP bm;
		m_bitmap.GetBitmap(&bm);

		// Paint the image.
		CBitmap* pOldBitmap = dcImage.SelectObject(&m_bitmap);
		dc.BitBlt(0, 0, bm.bmWidth, bm.bmHeight, &dcImage, 0, 0, SRCCOPY);
		dcImage.SelectObject(pOldBitmap);
	}
}

void CSplashWnd::OnTimer(UINT nIDEvent)
{
		DestroyWindow();
}
