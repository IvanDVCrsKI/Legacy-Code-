// KORView.cpp : implementation of the CKORView class
//

#include "stdafx.h"
#include "KOR.h"

#include "Math.h"
#include "CAutorDLG.h"

#include "KORDoc.h"
#include "KORView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CKORView

IMPLEMENT_DYNCREATE(CKORView, CView)

BEGIN_MESSAGE_MAP(CKORView, CView)
	//{{AFX_MSG_MAP(CKORView)
	ON_WM_KEYDOWN()
	ON_COMMAND(IDM_AUTOR_DLG, OnAutorDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CKORView construction/destruction

CKORView::CKORView()
{
	m_Zumx = 8070;
	m_Zumy = 8070;
	I = 1;
	ualfa1 = 45;
	ualfa2 = 60;
	Dkm = 1000;
	Lkm = 100;
	DM1 = 700;
	DM2 = 2000;
	// TODO: add construction code here

}

CKORView::~CKORView()
{
}

BOOL CKORView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CKORView drawing

void CKORView::OnDraw(CDC* pDC)
{
		CClientDC;
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetViewportOrg(90, 620); //90 -620
	pDC->SetWindowExt(m_Zumx,m_Zumy); //SetWindowExtent(100,100)  SetViewportExtent(200,200)  100 jedinica = 200 pixela
	                                                                                             //1 m = 2 pixela
	
	pDC->SetViewportExt(1000,-1000);


//--------------------------------------------------------
CPen PenBlack1(PS_SOLID,1,RGB(0,0,0));
pDC->SelectObject(PenBlack1);

	pDC->MoveTo(-1000, 0);
	pDC->LineTo( 200000,0);

	pDC->MoveTo(   0, -1000);
	pDC->LineTo(   0,  200000);

//osnovna skala I
	

//--------------------------------------------------------
//MREZA
//--------------------------------------------------------
pDC->SelectObject(PenBlack1);
for(int kooxa2 = 0; kooxa2 < 5100; kooxa2 += 500+I)
    {
        pDC->MoveTo(kooxa2, 0);
        pDC->LineTo(kooxa2, 5000);
	

    }

for(int kooya2 = 0; kooya2 < 5100; kooya2 += 500+I)
    {
        pDC->MoveTo(0, kooya2);
        pDC->LineTo(5000, kooya2);
	
    }
//--------------------------------------------------------
//MREZA MARGINE
//--------------------------------------------------------
CPen PenBlack20(PS_SOLID,20,RGB(0,0,0));
pDC->SelectObject(PenBlack20);


for(int kooxa3 = 0; kooxa3 < 5001; kooxa3 += 5000)
    {
        pDC->MoveTo(kooxa3, 0);
        pDC->LineTo(kooxa3, 5000);
	

    }

for(int kooya3 = 0; kooya3 < 5000; kooya3 += 5000)
    {
        pDC->MoveTo(0, kooya3);
        pDC->LineTo(5000, kooya3);
	
    }
//--------------------------------------------------------
//TEKST NA LENJIRIMA
//--------------------------------------------------------

pDC->SetTextColor(RGB(0, 0, 0));
pDC->SetBkMode(TRANSPARENT);

//ZA X OSU
	for (int kt1= 0; kt1<=5000; kt1+=500) //kt1<=5000-I  kt1=1000
	{
		        
        CString string;
        string.Format (("%d"), kt1 );
        pDC->TextOut (kt1, -250, string);//kt1+I
    }
//ZA Y OSU
	for (int kt2= 0; kt2<=5000; kt2+=500) 
	{
        
        CString string;
        string.Format (("%d"), kt2 );
        pDC->TextOut (-300, kt2, string);
    }
//--------------------------------------------------------
//PODEOCI NA LENJIRU
//--------------------------------------------------------
	pDC->SelectObject(PenBlack1);
    for(int koox2 = 0; koox2 < 5000; koox2 += 1000+I)
    {
        pDC->MoveTo(koox2, 0);
        pDC->LineTo(koox2, -250);
	

    }
	for(int kooy2 = 0; kooy2 < 5000; kooy2 += 1000+I)
    {
        pDC->MoveTo(0, kooy2);
        pDC->LineTo(-250, kooy2);
	
    }

	for(int koox3 = 0; koox3 < 5500; koox3 += (1000+I)/4)
    {
        pDC->MoveTo(koox3, 0);
        pDC->LineTo(koox3, -100);
	
    }
	for(int kooy3 = 0; kooy3 < 5500; kooy3 += (1000+I)/4)
    {
        pDC->MoveTo(0, kooy3);
        pDC->LineTo(-100, kooy3);
	
    }

//--------------------------------------------------------
//GRAFICKI PRIKAZ MLAZNIKA RAKETNOG MOTORA
//--------------------------------------------------------
double PI = 3.14159265359;
double g = 9.8041;



CPen PenBlue20(PS_SOLID,20,RGB(0,0,255));
pDC->SelectObject(PenBlue20);


pDC->MoveTo(2000,1500);
pDC->LineTo(2000+Lkm,1500);


pDC->MoveTo(2000,1500);

double alfa1;


alfa1 = 270-ualfa1;

double sin_alfa1;
double cos_alfa1;

sin_alfa1 =sin((PI*alfa1)/180);
cos_alfa1 =cos((PI*alfa1)/180);

double xm1;
double xm2;
double ym1;
double ym2;

double duzM1;
duzM1 = DM1;

xm1 = cos_alfa1*duzM1;
ym1 = sin_alfa1*duzM1;


pDC->LineTo(2000+xm1,1500+ym1);


pDC->MoveTo(2000+Lkm,1500);


double alfa2;

alfa2 = 270+ualfa2;


double sin_alfa2;
double cos_alfa2;

sin_alfa2 =sin((PI*alfa2)/180);
cos_alfa2 =cos((PI*alfa2)/180);

double duzM2;
duzM2 = DM2;

xm2 = cos_alfa2*duzM2;
ym2 = sin_alfa2*duzM2;



pDC->LineTo(2000+Lkm+xm2,1500+ym2);


//OGLEDALO
//--------------------------------------------------------

pDC->MoveTo(2000,1500+Dkm);
pDC->LineTo(2000+Lkm,1500+Dkm);

double oalfa1 =90+ualfa1;

double sin_oalfa1 =sin((PI*oalfa1)/180);
double cos_oalfa1 =cos((PI*oalfa1)/180);


double xmo1 = cos_oalfa1*duzM1; 
double ymo1 = sin_oalfa1*duzM1;




pDC->MoveTo(2000,1500+Dkm);
pDC->LineTo(2000+xmo1,1500+ymo1+Dkm);
//-------------------------------------------


double oalfa2 = 0-alfa2;

double sin_oalfa2 =sin((PI*oalfa2)/180);
double cos_oalfa2 =cos((PI*oalfa2)/180);


double xmo2 = cos_oalfa2*duzM2; 
double ymo2 = sin_oalfa2*duzM2;




pDC->MoveTo(2000+Lkm,1500+Dkm);
pDC->LineTo(2000+Lkm+xmo2,1500+ymo2+Dkm);






//--------------------------------------------------------
//GRAFICKI PRIKAZ VELICINE MLAZNIKA RAKETNOG MOTORA
//--------------------------------------------------------


//--------------------------------------------
//pomocne linije za RM CRVENE tanke


double xml1= 2000+xm1;
double xml2= 2000+Lkm+xm2;
double yml1= 1500+ym1;
double yml2= 1500+ym2;


/*
izvodjenje

pDC->MoveTo(2000+xm1,1500+ym1);
pDC->LineTo(2000+xm1,1500+ym1-200);

pDC->MoveTo(2500+xm2,1500+ym2);
pDC->LineTo(2500+xm2,1500+ym2-200);
*/

CPen PenRed2(PS_SOLID,2,RGB(255,0,0));
pDC->SelectObject(PenRed2);

pDC->MoveTo(xml1,yml1);
pDC->LineTo(xml1,500);

pDC->MoveTo(xml2,yml2);
pDC->LineTo(xml2,500);

pDC->MoveTo(xml2,500);
pDC->LineTo(xml1,500);

//-----------------------------
//vertikalna mera



pDC->MoveTo(xml1,yml1);
pDC->LineTo(500,yml1);

pDC->MoveTo(xml2,yml2);
pDC->LineTo(4500,yml2);
//pDC->MoveTo(

pDC->MoveTo(2000+Lkm+xmo2,1500+ymo2+Dkm);
pDC->LineTo(4500,1500+ymo2+Dkm);


pDC->MoveTo(2000+xmo1,1500+ymo1+Dkm);
pDC->LineTo(500,1500+ymo1+Dkm);

pDC->MoveTo(500,1500+ymo1+Dkm);//kota spoj levo
pDC->LineTo(500,yml1);

pDC->MoveTo(4500,1500+ymo2+Dkm);//kota spoj desno
pDC->LineTo(4500,yml2);

//--------------------------------------------
//pomocne linije za RM plave tanke

CPen PenBlue2(PS_SOLID,2,RGB(0,0,255));
pDC->SelectObject(PenBlue2);

pDC->MoveTo(xml1,yml1);
pDC->LineTo(2000+xmo1,1500+ymo1+Dkm);

pDC->MoveTo(xml2,yml2);
pDC->LineTo(2000+Lkm+xmo2,1500+ymo2+Dkm);


//--------------------------------------------------------
//PRORACUN ZA KONVERGENTNO DIVERGENTNI MLAZNIK
//--------------------------------------------------------


//temperatura i pritisak u komori RM
double To = 20000;
double po = 100*100000;

//izentropsko strujanje T = const
double Tkr; //temperatura u kriticnom pp
double Mkr = 2; //mahov broj u kriticnom pp 

double kapa = 1.4;//kapa za dati gas

//Tkr = Tk/(1+(((kapa-1)*(Mkr*Mkr))/2);


double mkv; // kvadrat mahovog broja
mkv = Mkr*Mkr;

double ko1;//standard za kapu
ko1 = 1+((kapa-1)/2);


Tkr = To/(ko1*mkv);//T u kriticnom pp


double TsT;//odnos temperatura
TsT = Tkr/To;

double pkr; //pritisak u kriticnom pp

double ko2; //kapa sa kapa - 1

ko2 = kapa/(kapa-1);

//pkr = pow(2,3);

pkr = po*(pow(TsT,ko2));

double Vkr; //brzina na kriticnom pp
double ct; //brzina zvuka na kriticnom pp 

double R = 287; //gasna konstanta
double kct; // konstanta pod korenom
kct = R*kapa*Tkr;
ct = sqrt(kct);

Vkr = Mkr*ct;// brzina u k pp

double qo; //gustina q



//qkr = pkr*(R*Tkr);

//double m_t;

//m_t = qo*

double Akr_sA;
double s_Mkr; //1 sa mkr
s_Mkr = 1/Mkr;

double k_kapa; //koeficijent pod korenom za odnos povrsina

k_kapa = (2*ko1)/(kapa+1);

double k_kapa2;
k_kapa2 = (kapa+1)/(2*(kapa-1));

Akr_sA = s_Mkr*pow(k_kapa,k_kapa2);

double pamb = 1*100000;// pritisak na izlazu
double Miz;//mahov broj na izlazu
double po_spamb;

po_spamb = po/pamb;
double k_kapa3;

k_kapa3= (kapa-1)/kapa;
double k_kapa4;
k_kapa4 = 2/(kapa-1);

double podkor1;

podkor1 = (pow(po_spamb,k_kapa3)-1)*k_kapa4;

Miz = sqrt(podkor1);  

double Ao_sA;
double s_Miz;

s_Miz = 1/Miz;
Ao_sA = s_Miz*pow(k_kapa,k_kapa2);

double A_t;
double Aiz = 0.001;//izlazni pp iz mlaznika
A_t = Aiz/Ao_sA;

double Akr = A_t*Akr_sA;







//--------------------------------------------------------
//TEKST DIMENZIJA
//--------------------------------------------------------

pDC->SetTextColor(RGB(255,0,0));

double tx2 = 4500;
double tx1 = 500;
double ty2 = (1500+ymo2+Dkm+yml2)/2;
double ty1 = (1500+ymo1+Dkm+yml1)/2;

double tx3 = (xml2+xml1)/2;
double ty3 = 500;


pDC->TextOut (tx2+50,ty2,"D1");

pDC->TextOut (tx1-150,ty1 ,"D2");

pDC->TextOut (tx3,ty3 ,"L");




//--------------------------------------------------------
//GRAFICKI PRIKAZ INFORMACIJA U VIDU TEKSTA
//--------------------------------------------------------


//--------------------------------------------------------
//proracuni za L,D1,D2


double dL;
//D2 je alfa1

double sin_ualfa1 =sin((PI*ualfa1)/180);
double sin_ualfa2 =sin((PI*ualfa2)/180);

dL = sin_ualfa1*duzM1+sin_ualfa2*duzM2+500;


pDC->SetTextColor(RGB(0,0,0));


char t1[10];
sprintf(t1,"Ugao alfa:    %2f",Miz );
pDC->TextOut (5200,5000 ,t1);

char t2[10];
sprintf(t2,"Ugao beta:    %2f",ualfa2 );
pDC->TextOut (5200,4800 ,t2);

char t3[10];
sprintf(t3,"L:    %2f",dL );
pDC->TextOut (5200,4600 ,t3);



double cos_ualfa2 =cos((PI*ualfa2)/180);
double dD1= 2*(cos_ualfa2*duzM2)+Dkm;
char t5[10];
sprintf(t5,"dD1:    %2f",dD1 );
pDC->TextOut (5200,4200 ,t5);


double cos_ualfa1 =cos((PI*ualfa1)/180);
double dD2= 2*(cos_ualfa1*duzM1)+Dkm;
char t7[10];
sprintf(t7,"dD2:    %2f",dD2);
pDC->TextOut (5200,4000 ,t7);

char t8[10];
sprintf(t8,"DM1:    %2f",DM1);
pDC->TextOut (5200,3800 ,t8);

char t9[10];
sprintf(t9,"DM2:    %2f",DM2);
pDC->TextOut (5200,3600 ,t9);

char t10[10];
sprintf(t10,"Kriticni PP:    %2f",Dkm);
pDC->TextOut (5200,3400 ,t10);

char t11[10];
sprintf(t11,"Duzina KPP:    %2f",Lkm);
pDC->TextOut (5200,3200 ,t11);

char t12[10];
sprintf(t12,"KPP:    %2f m^2",Akr);
pDC->TextOut (5200,3000 ,t12);

char t13[10];
sprintf(t13,"Izalzni PP:    %2f m^2",Aiz);
pDC->TextOut (5200,2800 ,t13);

char t14[10];
sprintf(t14,"Mahov na izlazu:    %2f",Miz);
pDC->TextOut (5200,2600 ,t14);

char t15[10];
sprintf(t15,"Brzina na KPP:    %2f",Vkr);
pDC->TextOut (5200,2400 ,t15);



/*
double dD2;
dD2 = sqrt((ymo1+ymo2)*(ymo1+ymo2));
char t2[10];
sprintf(t2,"D2:    %2f",dD2 );
pDC->TextOut (5200,4800 ,t2);
*/


ReleaseDC(pDC);
}

/////////////////////////////////////////////////////////////////////////////
// CKORView diagnostics

#ifdef _DEBUG
void CKORView::AssertValid() const
{
	CView::AssertValid();
}

void CKORView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CKORDoc* CKORView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CKORDoc)));
	return (CKORDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CKORView message handlers

void CKORView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
		switch (nChar)
		{
	case VK_F1:
		I +=100;
	
		Invalidate();
		break;

	case VK_F2:
		I -=100;

		Invalidate();
		break;

	case 0x51://q
		ualfa1+=1;

	
		Invalidate();
		break;

	case 0x57://w
		ualfa1-=1;

		Invalidate();
		break;


	case 0x41://a
		ualfa2-=1;

		Invalidate();
		break;

	case 0x53://s
		ualfa2+=1;

		Invalidate();
		break;
		}
	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CKORView::OnAutorDlg() 
{
       CCAutorDLG dlg;
	   	if (dlg.DoModal () == IDOK)
	{
				Invalidate ();
	}
}
