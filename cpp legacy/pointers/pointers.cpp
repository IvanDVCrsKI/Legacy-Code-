// pointers.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

int main()
{
	int x = 500;

	int  * pointer; // ili neki drugi naziv npr int * ptr... se deklarise sa zvezdom i omogucava da se vide adrese vrednosti na drugi nacin 

    pointer = &x;


	cout << "adresa x je : " << &x << endl;
	cout << "vednost x je : " << x << endl;
	cout << "adresa pointera je : " << &pointer <<endl; //pointer ima svoju adresu u RAM memoriji
	cout << "nakon preuzimanja adrese od x: " << pointer << endl;// sta se nalazi na toj adresi
	return 0;
}





