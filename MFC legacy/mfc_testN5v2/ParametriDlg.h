#if !defined(AFX_PARAMETRIDLG_H__686DA63F_B949_479E_AB7E_8757CB5DC4E0__INCLUDED_)
#define AFX_PARAMETRIDLG_H__686DA63F_B949_479E_AB7E_8757CB5DC4E0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ParametriDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CParametriDlg dialog

class CParametriDlg : public CDialog
{
// Construction
public:
	CParametriDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CParametriDlg)
	enum { IDD = IDD_KOORDINATE_DLG };
	int		m_Brzina;
	int		m_Ugao;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CParametriDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CParametriDlg)
	afx_msg void OnPomocnidlg();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PARAMETRIDLG_H__686DA63F_B949_479E_AB7E_8757CB5DC4E0__INCLUDED_)
