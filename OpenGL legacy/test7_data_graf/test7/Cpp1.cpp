
#include <glut.h>
#include <stdlib.h> //Needed for "exit" function

#include <fstream.h>
#include "Math.h"
#include <windows.h>


void tastatura(unsigned char key,int x, int y) 
{    
	switch (key)
	{
		case 27: //Escape key !!
			exit(0);
			
			
	}
}
int _ugao;
int _ugao2;

void handleResize(int w, int h) 
{
	//Tell OpenGL how to convert from coordinates to pixel values
	glViewport(0, 0, w, h);
	
	glMatrixMode(GL_PROJECTION); //Switch to setting the camera perspective
	
	//Set the camera perspective
	glLoadIdentity(); //Reset the camera
	gluPerspective(0.0,                  //The camera angle ili ugao oka je uvek konstantan
				   (double)w / (double)h, //The width-to-height ratio
				   1.0,                   //The near z clipping coordinate - z koordinata koja predstavlja granicu u pozitivnom smeru sve sto je manje od ove vrednosti nrmoj da crtas
				   200.0);                //The far z clipping coordinate
}

float red =0;
float green =0;
float blue =0;

void processSpecialKeys(int key, int x, int y)
{
	switch(key) 
	{
		case GLUT_KEY_F1 :
				red = 1.0;
				green = 0.0;
				blue = 0.0; break;
		case GLUT_KEY_F2 :
				red = 0.0;
				green = 1.0;
				blue = 0.0; break;
		case GLUT_KEY_F3 :
				red = 0.0;
				green = 0.0;
				blue = 1.0; break;
	}
}

void drawScene()
 {
	//Clear information from last draw
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);//ocisti sve predhodno
	glClearColor(red,green,blue,1.0); //( 0.5, 0.5, 1, 1.0 );//set window buffer collor
	
    glColor3f(0.0f, 0.0f,0.0f); //draw  in blue

	glMatrixMode(GL_PROJECTION); //Switch to the drawing perspective
	glLoadIdentity(); //Reset the drawing perspective
	
	

glMatrixMode (GL_PROJECTION);//(GL_PROJECTION)
glLoadIdentity ();
glOrtho(-1.0, 1.0, -1.0, 1.0, 1.0, -1.0);
//projection volume


glMatrixMode(GL_MODELVIEW);
glLoadIdentity();


double i =0;




//glRotatef(0, 0.0f, 1.0f, 0.0f);

glBegin(GL_POINTS);
/*
  const char* filename = "DL2_data.txt";
  std::ifstream inFile(filename);

  // Make sure the file stream is good
 

  double x = 0;
  double y= 0;
  while(!inFile.eof()) {
    inFile >> x >>y;
    cout  << x<<"   "<<y<<endl;
  }
  */
  //glVertex3f( x, y,0);
  //cout << endl;






	




	

int j =100;
 
  
  
glEnd(); 

	glBegin(GL_LINES); // OVDE POCINJU OBJEKTI

	
	glVertex3f(0.0,0.0, -5);
	glVertex3f(50, 0, -5);
glEnd(); 


	glBegin(GL_LINES); // OVDE POCINJU OBJEKTI
	
	
	glVertex3f(0.0,0.0, -5);
	glVertex3f(0, 50, -5);
glEnd(); 


	glBegin(GL_LINES); // OVDE POCINJU OBJEKTI

	
	glVertex3f(0.0,0.0, -5);
	glVertex3f(0, 0, -50);
glEnd(); 


   glBegin(GL_QUADS);
    




glPushMatrix(); //Save the transformations performed thus far

	glBegin(GL_LINES); // OVDE POCINJU OBJEKTI
	
	
	glVertex3f(-0.0,- 0.0, -5);
	glVertex3f(-1.5, -1.5, -5);
	
	glEnd(); 


	glBegin(GL_LINES); // OVDE POCINJU OBJEKTI
	
	
	glVertex3f(-0.0,- 0.0, -5);
	glVertex3f(-1, -1, -5);
	

glEnd();
glFlush();

	
	glutSwapBuffers(); //Send the 3D scene to the screen
	
}


void initRendering() 
{

	glEnable(GL_DEPTH_TEST);	//Makes 3D drawing work when something is in front of something else

}



void update(int value)
 {
	_ugao -= 4;
	_ugao2 -= 6;
	

	if (_ugao > 360) 
	{
			_ugao -= 360;
	}


	if (_ugao2 > 360) 
	{
			_ugao2 -= 360;
	}

	
	glutPostRedisplay(); //Tell GLUT that the display has changed
	
	//Tell GLUT to call update again in 25 milliseconds
	glutTimerFunc(100, update, 0);
}





int main(int argc, char** argv)
 {

	//Initialize GLUT
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(600, 600); //Set the window size
	
	//Create the window
	glutCreateWindow("TEST");//naziv prozora

	initRendering(); //Initialize rendering
 
	glutKeyboardFunc(tastatura);
	glutDisplayFunc(drawScene);//napravi ili nacrtaj
	glutReshapeFunc(handleResize);//funkcija koja omogucava da se obezbedi resize za prozor
	glutSpecialFunc(processSpecialKeys);
	glutTimerFunc(25, update, 0); //Add a timer


	glutMainLoop(); //Start the main loop.  glutMainLoop doesn't return.
	return 0; //This line is never reached
}