// MFCtestDoc.h : interface of the CMFCtestDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MFCTESTDOC_H__87167265_48EB_4EFB_8990_578FAE24A89A__INCLUDED_)
#define AFX_MFCTESTDOC_H__87167265_48EB_4EFB_8990_578FAE24A89A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMFCtestDoc : public COleDocument
{
protected: // create from serialization only
	CMFCtestDoc();
	DECLARE_DYNCREATE(CMFCtestDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMFCtestDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMFCtestDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMFCtestDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFCTESTDOC_H__87167265_48EB_4EFB_8990_578FAE24A89A__INCLUDED_)
